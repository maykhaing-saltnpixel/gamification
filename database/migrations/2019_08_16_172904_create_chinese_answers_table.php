<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChineseAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chinese_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->integer('question_id');
            $table->text('answer');
            $table->unsignedTinyInteger('correct_flg')->default('1')->comment('0:correct , 1:false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chinese_answer');
    }
}
