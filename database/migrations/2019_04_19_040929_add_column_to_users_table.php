<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('registrant_position')->after('points');
            $table->string('first_registrant_flg')->after('points')->comment('0:1st registrant , 1:not 1st');
            $table->string('gender')->after('email')->comment('1:Male , 2:Female');
            $table->string('profile')->after('points')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_registrant_flg');
            $table->dropColumn('registrant_position');
            $table->dropColumn('gender');
            $table->dropColumn('profile');
        });
    }
}
