<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->string('tuto_1_type')->nullable();
            $table->text('tuto_1_value')->nullable();
            $table->string('tuto_2_type')->nullable();
            $table->text('tuto_2_value')->nullable();
            $table->string('tuto_3_type')->nullable();
            $table->text('tuto_3_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorials');
    }
}
