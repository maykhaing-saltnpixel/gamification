<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChineseQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chinese_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id');
            $table->text('question');
            $table->unsignedTinyInteger('question_type')->comment('1:radio , 2:checkbox');
            $table->unsignedTinyInteger('delete_flg')->default('0')->comment('0:active , 1:delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chinese_question');
    }
}
