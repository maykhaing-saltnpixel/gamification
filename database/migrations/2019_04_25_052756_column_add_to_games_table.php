<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ColumnAddToGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->integer('tuto_flg')->default(0)->comment('0:not , 1:exist')->after('publish_flg');
            $table->integer('question_flg')->default(0)->comment('0:not , 1:exist')->after('publish_flg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('tuto_flg');
            $table->dropColumn('question_flg');
        });
    }
}
