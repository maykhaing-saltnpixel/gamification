<?php

// resources/lang/cn/messages.php

return [
	'login_title' => '登录',
	'login' => '登录',
    'register_title' => '注册',
    'register' => '注册',
    'first_name' => '名字',
    'last_name' => '姓氏',
    'country' => '国家',
    'partnerid' => '合作伙伴 ID',
    'email' => '电邮地址',
    'gender' => '性别',
    'company_name' => '公司名称',
    'password' => '密码',
    'confirm_password' => '确认密码',
    'c_greate_c' => '中国',
	'hk_greate_c' => '香港',
	'taiwan' => '台湾',
	'india' => '印度',
	'indo' => '印尼',
	'malaysia' => '马来西亚',
	'philippines' => '菲律宾',
	'singapore' => '新加坡',
	'thai' => '泰国',
	'vietnam' => '越南' ,
	'korea' => '韩国',
	'australia' => '澳大利亚',
	'new_zea' => '新西兰',

	//country
	'designation' => '职务',
	'administration' => '行政管理',
	'customer_service' => '客服',
	'engineering' => '工程师',
	'finance' => '财务',
	'it' => '信息技术',
	'logistics' => '物流',
	'marketing' => '市场营销',
	'retail' => '零售',
	'sales' => '销售',
	'technical' => '技术',
	'other_design' => '其它',

	// gender
	'gender' => '性别',
	'male' => '男性',
	'female' => '女性',

	'already' => '已经注册？',
	// 'click_to_login' => '请点击此处登录',
	'click_to_login' => '点击登录',
	'copy_right' => '© 惠普公司版权所有 2019。',
	'term_condi' => '条款和条件',
	'iagreetothe' => '我同意接受',

	// login
	'email_address' => '电邮地址',
	'your_email_address' => '电邮地址',
	'enter_valid_pwd' => '密码',
	'forget_pwd' => '忘记密码？',
	'newhere' => '尚未注册？',
	'click_to_register' => '请点击此处注册',
	'enter_fname' => '请输入名字',
	'enter_lname' => '请输入姓氏',
	'select_country' => '请选择国家',
	'select_designation' => '请选择职务',
	'enter_partnerid' => '请输入惠普伙伴ID',
	'enter_cemail' => '请输入公司电邮地址',
	'select_gender' => '请选择性别',
	'enter_companyname' => '请输入公司名称',
	'enter_password' => '请输入密码',
	'enter_cpassword' => '请再次输入与之前完全相 同的密码',
	'welcomeguest' => '欢迎',
	'logout' => '退出',

	//home
	'hp_logo' => '惠普活力星球竞赛',//rest
	'hp_menu1' => '培训与测验',
	'hp_menu2' => '惠普耗材产品组合',
	'hp_menu3' => '知识表',
	'hp_menu4' => '参与者信息',
	'hp_logo_img_text' => '每年有 10 万个海洋生物因塑料缠绕而死，而这只是人类目前发现的数量。',//rest
	'protect_ocean' => '保护海洋',//rest
	'4weeks' => '为期 4 周的培训。4 个方面的学习。保护 1 个星球。',
	'difference' => '您能否贡献一臂之力？',
	'quiz_schedule_text1' => '我们的生活习惯大大加速了气候变化。加入我们，了解惠普通过惠普耗材为缓解气候变化所做的努力，并成为惠普活力星球守护者。',
	'quiz_schedule_text2' => '在为期四周的培训中，您将学习惠普耗材如何帮助我们的客户减少碳阻迹，同时，我们将会从四个不同的方面对您进行测试。我们将在月末统计您每周的得分，并为得分最高的参与者颁发 HP EliteBook 745 G5 大奖。',
	'quiz_schedule' => '测验时间表',//rest
	'week1' => '第一周',
	'week2' => '第2周',
	'week3' => '第3周',
	'week4' => '第4周',
	'ocean_clean_up' => '清理海洋',
	'breathe_easy' => '惠普让您畅享呼吸',
	'science_inks' => '墨水科普',
	'putting_counterfeit' => '与仿冒产品说再见',
	'week1_text' => '塑料垃圾不断进入海洋，扼杀海洋生物。了解惠普是如何利用海洋塑料制成墨盒和硒鼓，帮助减少海洋污染的。',
	'week3_text' => '与仿制或再生墨盒不同的是，惠普原装墨盒历经数百小时测试和多年精心研发，拥有卓越非凡的质量和可靠性。',
	'week4_text' => '仿冒产品只会让您花原装品的价格，得到仿冒产品的质量。您能看出不同之处吗？',
	'startnow' => '立即开始',//rest
	'hp_portfolio_text' => '我们正在努力提高每个业务环节的循环能力。其中一种方法是使用闭环塑料制造所有惠普原装墨盒和硒鼓。',
	'learnmore' => '了解更多信息',//rest
	'prizes' => '奖品',
	'prize_collect' => '参与竞赛，获得最高分，即可赢取 HP EliteBook 745 G5 等诱人大奖！',
	'prize_qualify' => '领奖条件：',
	'prizes_qualify_list1' => '仅支持使用官方公司的电子邮箱注册',
	'prizes_qualify_list2' => '根据得分评选获奖者',
	'prizes_qualify_list3' => '每家公司的获奖人数最多为 3 人',
	'grandprize' => '特等奖',//rest
	'hpelitebook' => 'HP EliteBook 745 G5',//rest
	'hpelitebook_under_text' => '全球选出 1 位',//rest
	'countryprize' => '国家奖',//rest
	'homecountry' => '国家',
	'1stprize' => '一等奖',
	'2ndprize' => '二等奖',
	'3rdprize' => '三等奖',
	'winner' => '名获胜者',
	'winners' => '名获胜者',
	'prizes_note1' => '注',
	'prizes_note2' => '：表中所列金额将以电子代金券的形式发放。',
	'quiz_rules' => '测验规则',
	'step1' => '第 1 步',//rest
	'step1_text' => '阅读比赛注意事项',//rest
	'step2' => '第 2 步',//rest
	'step2_text' => '参与每周测验',//rest
	'step3' => '第 3 步',//rest
	'step3_text' => '以最短的时间完成测验即可赢大奖',//rest
	'pointsystem' => '积分系统',
	'participation' => '参与',
	'receive1' => '注册即可获得 ',
	'receive2' => '20',
	'receive3' => ' 个知识点（仅限前',
	'receive4' => ' 500 名',
	'receive5' => ')。',
	'knowyourstuff' => '正确率大比拼',
	'100points' => '每答对一题得 100 分 (举例来说，答对 3 题即可得 300 分)',
	'fastestfingersfirst' => '速度大比拼',
	'complete' => '回答问题所用的时间越短，获得的分数越高（奖励的分数即倒计时剩余的时间，例如，00:20 = 20 分）',
	'footer_home_1' => '（南太平洋）：澳大利亚和新西兰',
	'footer_home_2' => '（东南亚和韩国）：印度尼西亚、马来西亚、菲律宾、新加坡、泰国、越南和韩国',
	'footer_home_3' => '大中华区：中国大陆、中国香港和中国台湾',
	'SPAC' => 'SPAC',
	'SEA-K' => 'SEA-K',
	'greaterchina' => '大中华区',
	'eVouchers' => '电子代金券',
	'week1_tuto_title' => '您知道吗？',
	'week2_tuto_title' => '选择惠普产品来保护您的健康和环境',
	'week3_tuto_title' => '',
	'week4_tuto_title' => '使用假冒墨盒的后果比您想象的要严重',
	'tuto_intro_week_1' => '阅读培训材料并完成第一轮测验。',
	'tuto_intro_week_2' => '阅读培训材料并完成第二轮测验。',
	'tuto_intro_week_3' => '阅读培训材料并完成第三轮测验。',
	'tuto_intro_week_4' => '阅读培训材料并完成第二轮测验。',
	'week2_footnote' => '脚注：',
	'week2_footnote_txt1' => '1.基于城市居民。 有关详细信息，请参阅',
	'week2_footnote_txt2' => '2.惠普委托开展的 2018 年 WKI 蓝色天使室内空气质量合规性研究。该研究对 4 种新型兼容硒鼓品牌进行了测试，这些硒鼓可替代 HP LaserJet Pro MFP M425dn 使用的',
	'week2_footnote_txt3' => '280A 硒鼓。根据蓝色天使标签 RAL-UZ 205 针对办公设备的认证标准，本测试按照《测定硬拷贝设备排放物的测试方法》开展。有关详细信息，请访问',
	'week2_footnote_txt4' => 'http://h20195.www2.hp.com/v2/GetDocument.aspx?docname=4AA7-1981ENW。',
	'week2_footnote_txt5' => '',

	'tutorial_progress' => '培训',
	'q1_progress' => '问题1',
	'q2_progress' => '问题2',
	'q3_progress' => '问题3',

	'week_finish' => '本周结束了。',
];

?>