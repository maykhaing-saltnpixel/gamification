@extends('layouts.app')

@section('title', 'Profile Edit')

@section('content')

@include('layouts.header')
<?php
	$count = 1;
	foreach ($ranking_list as $list) {
		if ($list->id != auth()->user()->id) {
			$count = $count + 1;
		} else {
			$count = $count;
			break;
		}
	}
?>
<div class="spacer-30"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt">Participant Info</span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span>Participant Info</span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>

<div class="spacer-50"></div>

<div class="row" style="width: 100%;margin-left: 0px;">
	<div class="col-md-3" style="padding: 0px;"></div>
	<div class="col-md-6" style="padding: 0px;">
		<h3 style="font-size: 30px !important;font-family: 'Gotham Book' !important;">Participant Info</h3>
		@include('layouts.messages')
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-sm-3 text-center" style="padding: 0px;background: #1ab4f0;">
				<div class="spacer-20"></div>
				@if (auth()->user()->profile != '' && file_exists(public_path() . '/storage/' .$user_data->profile) && file_exists(public_path() . '/storage/' .$user_data->profile))
					<img src="{{url('/storage') . '/' .$user_data->profile}}" class="rounded-circle mx-auto d-block" style="width: 100px;height: 100px;">
				@else
					<img src="{{url('/img/default-img.png')}}" class="rounded-circle mx-auto d-block" style="width: 100px;height: 100px;">
				@endif
				<span class="participant_name" style="text-transform: uppercase;font-family: 'Gotham Light' !important;font-size: 18px !important;">{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
				<span class="participant_nation profile_nation_partner1">{{auth()->user()->country}}</span>
				<span class="participant_partnet profile_nation_partner2">PARTNER ID : {{auth()->user()->partner_id}}</span>

				<div class="spacer-50">
				</div>	
				
				<div class="">
					<img class="points_nation" src="{{url('/img/participant_separate-line.png')}}">
					<span class="participant_point1">{{auth()->user()->points}}</span>
					<span class="participant_point2">POINTS</span>
				</div>

				<div class="">
					<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
					<span class="participant_rankingnation1">{{$count}}</span>
					<span class="participant_rankingnation2">RANKING IN {{auth()->user()->country}}</span>
					<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
				</div>
			</div>
			<div class="col-sm-9" style="padding: 0px;background-color: #e8f2f7;padding: 30px;">
				<h4 style="font-family: 'Gotham Medium' !important;font-size: 18px !important;color: #000608 !important;">UPDATE YOUR PROFILE PICTURE</h4>
				<div class="spacer-20"></div>
				<form method="post" action="{{url('/picture_update')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="row">
						<div class="col-5 form-group">
							<input type="text" class="form-control" name="profile_img" id="profile_img" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;" placeholder="SELECT THE FILE">
							<input type="file" class="d-none" name="profile" id="profile" onchange="this.form.submit()">
						</div>
						<div class="col-5 form-group">
							<button type="button" class="btn btn-default" id="browse_profile" style="border-color: #1ab6f1;color: #1ab6f1;background-color: #fff;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;padding-top: 10px; font-family: 'Gotham Bold' !important;font-size: 12px !important;">BROWSE PICTURE</button>
						</div>
						<div class="col-2 form-group"></div>
					</div>
				</form>
				<hr>
				<form method="post" action="{{url('/profile_update')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<h4 style="font-family: 'Gotham Medium' !important;font-size: 18px !important;color: #000608 !important">PERSONAL INFO</h4>
					<div class="spacer-20"></div>
					<div class="row">
						<div class="col-5 form-group" style="padding-right: 0px;">
							<input type="text" name="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="FIRST NAME" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;">
						</div>
						<div class="col-5 form-group" style="padding-right: 0px;">
							<input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="LAST NAME" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;">
						</div>
					</div>
					<div class="row">
						<div class="col-10 form-group" style="padding-right: 0px;">
							<select class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" id="country" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;text-transform: uppercase;font-family: ' Gotham Light' !important;font-size: 11px !important;">
								<option value="">Select country</option>
								<option value="China (Greater China)" {{ $user_data->country == 'China (Greater China)' ? 'selected':'' }}>China (Greater China)</option>
								<option value="Hong Kong (Greater China)" {{ $user_data->country == 'Hong Kong (Greater China)' ? 'selected':'' }}>Hong Kong (Greater China)</option>
								<option value="Taiwan (Greater China)" {{ $user_data->country == 'Taiwan (Greater China)' ? 'selected':'' }}>Taiwan (Greater China)</option>
								<option value="India" {{ $user_data->country == 'India' ? 'selected':'' }}>India</option> 
								<option value="Indonesia (SEA-K)" {{ $user_data->country == 'Indonesia (SEA-K)' ? 'selected':'' }}>Indonesia (SEA-K)</option> 
								<option value="Malaysia (SEA-K)" {{ $user_data->country == 'Malaysia (SEA-K)' ? 'selected':'' }}>Malaysia (SEA-K)</option> 
								<option value="Philippines (SEA-K)" {{ $user_data->country == 'Philippines (SEA-K)' ? 'selected':'' }}>Philippines (SEA-K)</option> 
								<option value="Singapore (SEA-K)" {{ $user_data->country == 'Singapore (SEA-K)' ? 'selected':'' }}>Singapore (SEA-K)</option> 
								<option value="Thailand (SEA-K)" {{ $user_data->country == 'Thailand (SEA-K)' ? 'selected':'' }}>Thailand (SEA-K)</option> 
								<option value="Vietnam (SEA-K)" {{ $user_data->country == 'Vietnam (SEA-K)' ? 'selected':'' }}>Vietnam (SEA-K)</option>
								<option value="Korea (SEA-K)" {{ $user_data->country == 'Korea (SEA-K)' ? 'selected':'' }}>Korea (SEA-K)</option> 
								<option value="Australia (SPAC)" {{ $user_data->country == 'Australia (SPAC)' ? 'selected':'' }}>Australia (SPAC)</option> 
								<option value="New Zealand (SPAC)" {{ $user_data->country == 'New Zealand (SPAC)' ? 'selected':'' }}>New Zealand (SPAC)</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-10 form-group" style="padding-right: 0px;">
							<input type="text" name="company_name" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" placeholder="COMPANY NAME" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;">
						</div>
					</div>

					<div class="row">
						<div class="col-10 form-group" style="padding-right: 0px;">
							<select class="form-control {{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" id="designation" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;text-transform: uppercase;font-family: ' Gotham Light' !important;font-size: 11px !important;">
								<option value="">SELECT DESIGNATION</option>
								<option value="Administration">Administration</option>
								<option value="Customer Service">Customer Service</option>
								<option value="Engineering">Engineering</option>
								<option value="Finance">Finance</option> 
								<option value="IT/Technology">IT/Technology</option> 
								<option value="Logistics">Logistics</option> 
								<option value="Marketing">Marketing</option> 
								<option value="Retail Service">Retail Service</option> 
								<option value="Sales">Sales</option> 
								<option value="Technical">Technical</option>
								<option value="Others">Others</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-10 form-group d-none" id="other_designation" style="padding-right: 0px;">
							<input type="text" class="form-control {{ $errors->has('other_designation') ? ' is-invalid' : '' }}" name="other_designation" id="other_designation_txt" value="" placeholder="Designation (Please Specify)" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;">
						</div>
					</div>

					<div class="row">
						<div class="col-10 form-group" style="padding-right: 0px;">
							<select class="form-control {{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;text-transform: uppercase;font-family: ' Gotham Light' !important;font-size: 11px !important;">
								<option value="">SELECT GENDER</option>
								<option value="1">Male</option>
								<option value="2">Female</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-10 form-group">
							<button class="btn btn-default" style="border-color: #1ab6f1;color: #1ab6f1;background-color: #fff;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;padding-top: 10px; font-family: 'Gotham Bold' !important;font-size: 12px !important;">UPDATE PROFILE</button>
						</div>
					</div>
				</form>
				<hr>
				<h4 style="font-family: 'Gotham Medium' !important;font-size: 18px !important;color: #000608 !important">UPDATE PASSWORD</h4>
				<div class="spacer-20"></div>
				<form method="post" action="{{url('/password_update')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="row">
						<div class="col-5 form-group">
							<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;" placeholder="NEW PASSWORD">
							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-5 form-group">
							<input type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" id="confirm_password" style="border-radius: 0px;border-color: #aaaead;background-color: transparent;font-family: ' Gotham Light' !important;font-size: 11px !important;" placeholder="CONFIRM PASSWORD">
							@if ($errors->has('confirm_password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('confirm_password') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-10 form-group">
							<button class="btn btn-default" style="border-color: #1ab6f1;color: #1ab6f1;background-color: #fff;padding-left: 10px;padding-right: 10px;padding-bottom: 10px;padding-top: 10px; font-family: 'Gotham Bold' !important;font-size: 12px !important;">UPDATE PASSWORD</button>
						</div>
					</div>
				</form>
			</div>				
		</div>
		<div class="row" style="width: 100%;margin-left: 0px;border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 15px;">
			<div class="col-sm-6 col-md-6">
				<span style="font-size: 12px;font-family: 'Gotham Light' !important;">{{ __('messages.copy_right') }}</span>
			</div>
			<div class="col-sm-6 col-md-6 text-right">
				<a href="{{url('/term_of_use')}}" style="text-decoration: none;color: #000;">
					<span style="font-size: 12px;text-decoration: underline;font-family: 'Gotham Light' !important;">{{ __('messages.term_condi') }}</span>
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3" style="padding: 0px;"></div>
</div>

<div class="spacer-100"></div>

<!-- @include('layouts.footer-home') -->
<script type="text/javascript">
	$("#designation").change(function() {
		if ($(this).val() == 'Others') {
			$("#other_designation").removeClass('d-none');
			$("#other_designation_txt").attr('required','true');
		} else {
			$("#other_designation").addClass('d-none');
			$("#other_designation_txt").removeAttr('required');
		}
	});

	$("#profile_img").click(function() {
		$("#profile").click();
	});

	$("#profile").change(function(e) {
		var fileName = e.target.files[0].name;
		$("#profile_img").val(fileName);
	});

	$("#browse_profile").click(function() {
		$("#profile").click();
	});
</script>
@endsection