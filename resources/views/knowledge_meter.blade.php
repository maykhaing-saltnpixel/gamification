@extends('layouts.app')

@section('title', 'Knowledge Metre')

@section('content')

@include('layouts.header')
<link rel="stylesheet" href="{{url('/css/pagination_custom.css?v=' . time())}}">
<?php
	$user_rank = 1;
	if(isset($ranking_list) && count($ranking_list) > 0){
		foreach ($ranking_list as $list) {
			if ($list->id != auth()->user()->id) {
				$user_rank = $user_rank + 1;
			} else {
				$user_rank = $user_rank;
				break;
			}
		}
	}
	if($current_user_rank->country == 'Australia (SPAC)' || $current_user_rank->country =='New Zealand (SPAC)') {
		$user_country = 'SPAC (South Pacific)';
	} elseif($current_user_rank->country == 'Indonesia (SEA-K)' || $current_user_rank->country =='Malaysia (SEA-K)' || $current_user_rank->country =='Philippines (SEA-K)' || $current_user_rank->country =='Singapore (SEA-K)' || $current_user_rank->country =='Thailand (SEA-K)' || $current_user_rank->country =='Vietnam (SEA-K)' || $current_user_rank->country =='Korea (SEA-K)') {
		$user_country = 'SEAK (S.E.A & Korea)';
	} elseif($current_user_rank->country == 'China (Greater China)' || $current_user_rank->country =='Hong Kong (Greater China)' || $current_user_rank->country == 'Taiwan (Greater China)') {
		$user_country = 'China';
	} elseif($current_user_rank->country == 'India') {
		$user_country = 'India';
	}
?>
<div class="spacer-30"></div>
<div class="container-fluid">
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-md-2 col-sm-2"></div>
		<div class="col-md-8 col-sm-8 d-none d-sm-block">
			<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
			<div class="bottom-left">
				<a href="{{url('/')}}" style="text-decoration: none;">
					<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
				</a>
			</div>
			<div class="bottom-left2">
				<span class="bread_tu_qui_txt">{{__('messages.hp_menu3')}}</span>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 d-block d-sm-none">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #1ab6f1;"></i>
			</a>
			>
			<span>{{__('messages.hp_menu3')}}</span>
		</div>
		<div class="col-md-2 col-sm-2"></div>
	</div>
	<div class="spacer-30"></div>
	<div class="row">
		<div class="col-sm-2"></div>
		<!-- Dasktop -->
		<div class="col-sm-8 d-none d-sm-block">
			<h3 style="font-family: 'Gotham Medium';font-size: 30px;margin-left: 26px;">{{__('messages.hp_menu3')}}</h3>
			<div class="row" style="background-color: #e8f2f7;padding: 20px !important;margin-right: 0px;margin-left: 0px;">
				<div class="col-sm-12 col-md-6">
					@if($rank_key == '')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">Overall - <span>Grand prize rankings</span></h5>
					@elseif($rank_key == 'SPAC (South Pacific)')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">SPAC Rankings</h5>
					@elseif($rank_key == 'SEAK (S.E.A & Korea)')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">SEAK Rankings</h5>
					@elseif($rank_key == 'China')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">Greater China Rankings</h5>
					@elseif($rank_key == 'India')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">India Rankings</h5>
					@endif
				</div>
				<div class="col-sm-12 col-md-6 text-right d-sm-none d-md-block">
					<form action="{{url('/rank_search')}}" method="post" id="rank_search_form" enctype="multipart\form-data">
						{{ csrf_field() }}
						<select name="country" style="padding: 10px;background-color: #c9dae2;color: #495057;border: none;font-family: 'Gotham Light';" onchange="this.form.submit()">
							<option value="Filter by location" {{ $rank_key == "" ? "selected":"" }}>Filter by location</option>
							<option value="" {{ $rank_key == "overall" ? "selected":"" }}>Overall Location</option>
							<option value="SPAC (South Pacific)" {{ $rank_key == "SPAC (South Pacific)" ? "selected":"" }}>SPAC (South Pacific)</option>
							<option value="SEAK (S.E.A & Korea)" {{ $rank_key == "SEAK (S.E.A & Korea)" ? "selected":"" }}>SEAK (S.E.A & Korea)</option>
							<option value="China" {{ $rank_key == "China" ? "selected":"" }}>China</option>
							<option value="India" {{ $rank_key == "India" ? "selected":"" }}>India</option>
						</select>
					</form>
				</div>
				<div class="col-sm-12 col-md-6 d-none d-sm-block d-md-none" >
					<form action="{{url('/rank_search')}}" method="post" id="rank_search_form" enctype="multipart\form-data">
						{{ csrf_field() }}
						<select name="country" style="padding: 10px;background-color: #c9dae2;color: #495057;border: none;font-family: 'Gotham Light';" onchange="this.form.submit()">
							<option value="Filter by location" {{ $rank_key == "" ? "selected":"" }}>Filter by location</option>
							<option value="" {{ $rank_key == "overall" ? "selected":"" }}>Overall Location</option>
							<option value="SPAC (South Pacific)" {{ $rank_key == "SPAC (South Pacific)" ? "selected":"" }}>SPAC (South Pacific)</option>
							<option value="SEAK (S.E.A & Korea)" {{ $rank_key == "SEAK (S.E.A & Korea)" ? "selected":"" }}>SEAK (S.E.A & Korea)</option>
							<option value="China" {{ $rank_key == "China" ? "selected":"" }}>China</option>
							<option value="India" {{ $rank_key == "India" ? "selected":"" }}>India</option>
						</select>
					</form>
				</div>
			</div>
			<div class="d-none d-sm-block" style="background-color: #627184;padding: 10px;">
				<div class="row" style="font-family: 'Gotham Medium';font-size: 16px;color: #fff;width: 100%;margin-left: 0px;">
					<div class="col-1 knowledge_t_head_1" style="height: 40px !important;padding: 0px;">
						<span style="vertical-align: -webkit-baseline-middle;">NO.</span>
					</div>
					<div class="col-4" style="height: 40px;padding: 0px 30px;">
						<span style="vertical-align: -webkit-baseline-middle;">NAME</span>
					</div>
					<div class="col-4" style="height: 40px;">
						<span style="vertical-align: -webkit-baseline-middle;">COUNTRY</span>
					</div>
					<div class="col-3">
						<span style="vertical-align: -webkit-baseline-middle;">POINTS</span>
					</div>
				</div>
			</div>

			<div class="col-12" style="background-color: #e8f2f7;padding-left: 40px;padding-right: 40px;padding-bottom: 5px;">
				@php $count = 1; @endphp

				@if($rank_key != '' || $user_country == $rank_key)
					@if(isset($ranking_list))
						@if(count($ranking_list) > 5)
							@php $loop = 5; @endphp
						@else
							@php $loop = count($ranking_list); @endphp
						@endif
						@for($i = 0; $i < $loop; $i++)
							@if($ranking_list[$i]['id'] == $current_user_rank->id)
								<div class="row" style="color: #fff;margin-left: -40px;margin-right: -40px;">
									<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$user_rank}}.</p>
									</div>
									<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
										@if(file_exists(public_path() . '/storage/' .auth()->user()->profile) && file_exists(public_path() . '/storage/' .auth()->user()->profile) && auth()->user()->profile != '')
											<img src="{{url('/storage') . '/' .auth()->user()->profile}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
										@else
											<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
										@endif
										<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</span>
									</div>
									<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 0px;margin-top: -1px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin-top: 20px;">{{$current_user_rank->country}}</p>
									</div>
									<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
									</div>
									<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
									</div>
								</div>
								@php break; @endphp
								@php $count = $count+1; @endphp
							@else
								<div class="row" style="color: #000;border-bottom: 1px solid #afc1cb;padding-bottom: 5px;">
									<div class="col-1 text-left" style="height: 60px;padding-left: 0px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$count}}.</p>
									</div>
									<div class="col-4" style="height: 60px;padding-left: 0px;">
										@if($ranking_list[$i]['profile'] == '')
											<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 6px;width: 55px;height: 55px;">
										@else
											<img src="{{url('/storage') . '/' .$ranking_list[$i]['profile']}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 6px;width: 55px;height: 55px;">
										@endif
										<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$ranking_list[$i]['first_name']}} {{$ranking_list[$i]['last_name']}}</span>
									</div>
									<div class="col-4" style="padding-left: 0px;margin: auto !important;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin: auto !important;">{{$ranking_list[$i]['country']}}</p>
									</div>
									<div class="col-3" style="height: 60px;padding-left: 36px;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$ranking_list[$i]['points']}}</p>
									</div>
								</div>
								@php $count = $count+1; @endphp
							@endif
						@endfor
					@endif
				@else
					@if(count($ranking_list) > 5)
						@php $loop = 5; @endphp
					@else
						@php $loop = count($ranking_list); @endphp
					@endif
					@for($k = 0; $k < $loop; $k++)
						@if($ranking_list[$k]['id'] == $current_user_rank->id)
							<div class="row" style="color: #fff;margin-left: -40px;margin-right: -40px;">
								<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
									<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$user_rank}}.</p>
								</div>
								<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
									@if(file_exists(public_path() . '/storage/' .auth()->user()->profile) && file_exists(public_path() . '/storage/' .auth()->user()->profile) && auth()->user()->profile != '')
										<img src="{{url('/storage') . '/' .auth()->user()->profile}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
									@else
										<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
									@endif
									
									<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</span>
								</div>
								<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 0px;margin-top: -1px;">
									<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin-top: 20px;">{{$current_user_rank->country}}</p>
								</div>
								<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;">
									<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
								</div>
								<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;">
									<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
								</div>
							</div>
							@php break; @endphp
							@php $count = $count+1; @endphp
						@else
							<div class="row" style="color: #000;border-bottom: 1px solid #afc1cb;padding-bottom: 5px;">
								<div class="col-1 text-left" style="height: 60px;padding-left: 0px;">
									<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$count}}.</p>
								</div>
								<div class="col-4" style="height: 60px;padding-left: 0px;">
									@if($ranking_list[$k]['profile'] == '')
										<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 6px;width: 55px;height: 55px;">
									@else
										<img src="{{url('/storage') . '/' .$ranking_list[$k]['profile']}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 6px;width: 55px;height: 55px;">
									@endif
									<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$ranking_list[$k]['first_name']}} {{$ranking_list[$k]['last_name']}}</span>
								</div>
								<div class="col-4" style="padding-left: 0px;margin: auto !important;">
									<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin: auto !important;">{{$ranking_list[$k]['country']}}</p>
								</div>
								<div class="col-3" style="height: 60px;padding-left: 36px;">
									<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$ranking_list[$k]['points']}}</p>
								</div>
							</div>
							@php $count = $count+1; @endphp
						@endif
					@endfor
				@endif
			</div>
			@if($user_rank == 6 && ($rank_key == '' || $user_country == $rank_key))
				<div class="col-12" style="">
					<div class="row" style="color: #fff;">
						<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
							<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$user_rank}}.</p>
						</div>
						<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
							@if(file_exists(public_path() . '/storage/' .auth()->user()->profile) && file_exists(public_path() . '/storage/' .auth()->user()->profile) && auth()->user()->profile != '')
								<img src="{{url('/storage') . '/' .auth()->user()->profile}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
							@else
								<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
							@endif
							<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</span>
						</div>
						<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 0px;margin-top: -1px;">
							<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin-top: 20px;">{{$current_user_rank->country}}</p>
						</div>
						<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;">
							<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
						</div>
						<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;">
							<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
						</div>
					</div>
				</div>
			@elseif($user_rank > 6 && ($rank_key == '' || $user_country == $rank_key))
				<div class="col-12 text-center" style="height: 100px;background-color: #e8f2f7;padding-left: 40px;padding-right: 40px;padding-bottom: 5px;">
					<i class="fa fa-ellipsis-v" style="font-size: 45px;margin-top: 25px;"></i>
				</div>
				<div class="col-12" style="">
					<div class="row" style="color: #fff;">
						<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
							<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">{{$user_rank}}.</p>
						</div>
						<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 24px;margin-top: -1px;">
							@if(file_exists(public_path() . '/storage/' .auth()->user()->profile) && file_exists(public_path() . '/storage/' .auth()->user()->profile) && auth()->user()->profile != '')
								<img src="{{url('/storage') . '/' .auth()->user()->profile}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
							@else
								<img src="{{url('/img/default-img.png')}}" class="img-responsive person-img" style="border: 1px solid #667587;border-radius: 50%;margin-top: 4px;vertical-align: -webkit-baseline-middle;width: 55px;height: 55px;">
							@endif
							<span style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</span>
						</div>
						<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding-left: 0px;margin-top: -1px;">
							<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin-top: 20px;">{{$current_user_rank->country}}</p>
						</div>
						<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;">
							<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
						</div>
						<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;">
							<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
						</div>
					</div>
				</div>
			@endif
			<div class="col-12">
				<div class="row d-none d-sm-block">
					<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="row">
							<div class="col-sm-8 col-md-8 footer_div">
								<span>{{ __('messages.copy_right') }}</span>
							</div>
							<div class="col-sm-4 col-md-4 footer_div text-right">
								<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
								</a>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{ __('messages.copy_right') }}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Mobile -->
		<div class="col-sm-8 d-block d-sm-none">
			<h3 style="font-family: 'Gotham Medium';font-size: 30px;margin-left: 26px;">{{__('messages.hp_menu3')}}</h3>
			<div class="row" style="background-color: #e8f2f7;padding: 20px !important;margin-right: 0px;margin-left: 0px;">
				<div class="col-12">
					@if($rank_key == '')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">Overall - <span>Grand prize rankings</span></h5>
					@elseif($rank_key == 'SPAC (South Pacific)')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">SPAC Rankings</h5>
					@elseif($rank_key == 'SEAK (S.E.A & Korea)')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">SEAK Rankings</h5>
					@elseif($rank_key == 'China')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">Greater China Rankings</h5>
					@elseif($rank_key == 'India')
						<h5 style="font-family: 'Gotham Medium';font-size: 16px;line-height: 22px;text-transform: uppercase;color: #000000;margin-top: 10px;">India Rankings</h5>
					@endif
				</div>
				<div class="col-12">
					<form action="{{url('/rank_search')}}" method="post" id="rank_search_form" enctype="multipart\form-data">
						{{ csrf_field() }}
						<select name="country" style="background-color: #c9dae2;color: #495057;border: none;font-family: 'Gotham Light';  -webkit-appearance: none;-moz-appearance: none;appearance: none;padding: 10px 15px 10px 15px;" onchange="this.form.submit()">
							<option value="Filter by location" {{ $rank_key == "" ? "selected":"" }}>Filter by location</option>
							<option value="" {{ $rank_key == "overall" ? "selected":"" }}>Overall Location</option>
							<option value="SPAC (South Pacific)" {{ $rank_key == "SPAC" ? "selected":"" }}>SPAC</option>
							<option value="SEAK (S.E.A & Korea)" {{ $rank_key == "SEAK" ? "selected":"" }}>SEA-K</option>
							<option value="China" {{ $rank_key == "China" ? "selected":"" }}>Greater China</option>
							<option value="India" {{ $rank_key == "India" ? "selected":"" }}>India</option>
						</select>
					</form>
				</div>
			</div>
			<div class="" style="background-color: #627184;padding: 10px;">
				<div class="row" style="font-family: 'Gotham Medium';font-size: 16px;color: #fff;width: 100%;margin-left: 0px;">
					<div class="col-1 " style="padding: 0px;">
						<span style="vertical-align: -webkit-baseline-middle;">NO.</span>
					</div>
					<div class="col-4" style="height: 40px;">
						<span style="vertical-align: -webkit-baseline-middle;">NAME</span>
					</div>
					<div class="col-4" style="height: 40px;padding: 0px;">
						<span style="vertical-align: -webkit-baseline-middle;">COUNTRY</span>
					</div>
					<div class="col-3" style="height: 40px;padding: 0px;">
						<span style="vertical-align: -webkit-baseline-middle;">POINTS</span>
					</div>
				</div>
			</div>

			<div class="" style="background-color: #e8f2f7;padding-bottom: 5px;">
				<div class="" style="background-color: #e8f2f7;padding-bottom: 5px;">
					@if($rank_key != '' || $user_country == $rank_key)
						@php $count = 1; @endphp
						@if(isset($ranking_list))
							@if(count($ranking_list) > 5)
								@php $loop2 = 5; @endphp
							@else
								@php $loop2 = count($ranking_list); @endphp
							@endif
							@for($i = 0; $i < $loop2; $i++)
								@if($ranking_list[$i]['id'] == $current_user_rank->id)
									<div class="row" style="color: #fff;width: 100%;margin-left: 0px;background-color: #1ab6f1;">
										<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding: 0px;">
											<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;padding-left: 10px;">{{$user_rank}}.</p>
										</div>
										<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding: 0px;">
											<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</p>
										</div>
										<div class="col-4" style="background-color: #1ab6f1;padding: 0px;margin: auto !important;">
											<p style="font-family: 'Gotham Book';font-size: 13px !important;margin: auto !important;">{{$current_user_rank->country}}</p>
										</div>
										<div class="col-3 d-sm-none d-md-block" style="height: 60px;background-color: #1ab6f1;padding: 0px;">
											<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
										</div>
										<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;">
											<p style="font-family: 'Gotham Medium';font-size: 15px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
										</div>
									</div>
									@php break; @endphp
									@php $count = $count+1; @endphp
								@else
									<div class="row" style="color: #000;border-bottom: 1px solid #afc1cb;padding-bottom: 5px;width: 100%;margin-left: 0px;">
										<div class="col-1" style="height: 60px;padding: 0px;">
											<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;padding-left: 10px;">{{$count}}.</p>
										</div>
										<div class="col-4" style="margin: auto !important;">
											<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin: auto !important;">&nbsp;&nbsp;&nbsp;{{$ranking_list[$i]['first_name']}} {{$ranking_list[$i]['last_name']}}</p>
										</div>
										<div class="col-4" style="padding: 0px;margin: auto !important;">
											<p style="font-family: 'Gotham Book';font-size: 13px !important;vertical-align: -webkit-baseline-middle;margin: auto !important;">{{$ranking_list[$i]['country']}}</p>
										</div>
										<div class="col-3" style="padding-left: 36px;margin: auto !important;">
											<p style="font-family: 'Gotham Medium';font-size: 15px !important;margin: auto !important;">{{$ranking_list[$i]['points']}}</p>
										</div>
									</div>
									@php $count = $count+1; @endphp
								@endif
							@endfor
						@endif
					@else
						@php $count = 1; @endphp
						@for($i = 0; $i < $loop; $i++)
							@if($ranking_list[$i]['id'] == $current_user_rank->id)
								<div class="row" style="color: #fff;width: 100%;margin-left: 0px;background-color: #1ab6f1;">
									<div class="col-1" style="height: 60px;background-color: #1ab6f1;padding: 0px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;padding-left: 10px;">{{$user_rank}}.</p>
									</div>
									<div class="col-4" style="margin: auto !important;background-color: #1ab6f1;">
										<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin: auto !important;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</p>
									</div>
									<div class="col-4" style="margin: auto !important;background-color: #1ab6f1;padding: 0px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;margin: auto !important;">{{$current_user_rank->country}}</p>
									</div>
									<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1;margin: auto !important;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin: auto !important;">{{$current_user_rank->points}}</p>
									</div>
									<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;padding: 0px;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
									</div>
								</div>
								@php break; @endphp
								@php $count = $count+1; @endphp
							@else
								<div class="row" style="color: #000;border-bottom: 1px solid #afc1cb;padding-bottom: 5px;width: 100%;margin-left: 0px;">
									<div class="col-1" style="height: 60px;padding: 0px;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;padding-left: 10px;margin-top: 20px;">{{$count}}.</p>
									</div>
									<div class="col-4" style="margin: auto !important;">
										<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin: auto !important;">&nbsp;&nbsp;&nbsp;{{$ranking_list[$i]['first_name']}} {{$ranking_list[$i]['last_name']}}</p>
									</div>
									<div class="col-4" style="padding: 0px;margin: auto !important;">
										<p style="font-family: 'Gotham Book';font-size: 14px !important;vertical-align: -webkit-baseline-middle;margin: auto !important;">{{$ranking_list[$i]['country']}}</p>
									</div>
									<div class="col-3" style="margin: auto !important;">
										<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin: auto !important;">{{$ranking_list[$i]['points']}}</p>
									</div>
								</div>
								@php $count = $count+1; @endphp
							@endif
						@endfor
					@endif
				</div>
				@if($user_rank == 6 && ($rank_key != '' || $user_country == $rank_key))
					<div class="col-12" style="">
						<div class="row" style="color: #fff;width: 100%;margin-left: 0px;padding: 0px;background-color: #1ab6f1;">
							<div class="col-1" style="height: 60px;background-color: #1ab6f1;">
								<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;padding-left: 10px;">{{$user_rank}}.</p>
							</div>
							<div class="col-4" style="height: 60px;background-color: #1ab6f1;padding: 0px;">
								<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</p>
							</div>
							<div class="col-4" style="background-color: #1ab6f1;padding-left: 20px;padding: 0px;margin: auto !important;">
								<p style="font-family: 'Gotham Book';font-size: 13px !important;margin: auto !important;">{{$current_user_rank->country}}</p>
							</div>
							<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;padding: 0px;">
								<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
							</div>
							<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;padding: 0px;">
								<p style="font-family: 'Gotham Medium';font-size: 15px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
							</div>
						</div>
					</div>
				@elseif($user_rank > 6 && ($rank_key != '' || $user_country == $rank_key))
					<div class="col-12 text-center" style="height: 100px;background-color: #e8f2f7;padding-left: 40px;padding-right: 40px;padding-bottom: 5px;">
						<!-- <div class="row text-center" style="color: #000;border-bottom: 1px solid #afc1cb;height: 60px;"> -->
							<i class="fa fa-ellipsis-v" style="font-size: 45px;margin-top: 25px;"></i>
						<!-- </div> -->
					</div>
					<div class="col-12" style="">
						<div class="row" style="color: #fff;width: 100%;margin-left: 0px;background-color: #1ab6f1;">
							<div class="col-1" style="height: 60px;background-color: #1ab6f1;margin-top: -1px;padding: 0px;">
								<p style="font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;padding-left: 10px;">{{$user_rank}}.</p>
							</div>
							<div class="col-4" style="height: 60px;background-color: #1ab6f1;margin-top: -1px;padding: 0px;">
								<p style="vertical-align: -webkit-baseline-middle;font-family: 'Gotham Book';font-size: 14px !important;margin-top: 20px;">&nbsp;&nbsp;&nbsp;{{$current_user_rank->first_name}} {{$current_user_rank->last_name}}</p>
							</div>
							<div class="col-4" style="background-color: #1ab6f1;padding-left: 20px;margin-top: -1px;padding: 0px;margin: auto !important;">
								<p style="font-family: 'Gotham Book';font-size: 13px !important;margin: auto !important;">{{$current_user_rank->country}}</p>
							</div>
							<div class="col-3 d-sm-none d-md-block" style="background: #1ab6f1 url('{{url('/img/your-rank.png')}}');background-repeat: no-repeat;background-position: right center;background-size: contain;height: 60px;padding-left: 24px;margin-top: -1px;padding: 0px;">
								<p style="font-family: 'Gotham Medium';font-size: 16px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
							</div>
							<div class="col-3 d-none d-sm-block d-md-none" style="height: 60px;padding-left: 24px;background-color: #1ab6f1;padding: 0px;">
								<p style="font-family: 'Gotham Medium';font-size: 15px !important;margin-top: 20px;">{{$current_user_rank->points}}</p>
							</div>
						</div>
					</div>
				@endif

				<div class="spacer-10"></div>
			</div>

			<div class="row d-none d-sm-block">
				<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="row">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{ __('messages.copy_right') }}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div text-right">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
							</a>
						</div>
					</div>
					
				</div>
			</div>

			<div class="col-12">
				<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{ __('messages.copy_right') }}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
						</a>
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-sm-2"></div>
	</div>
</div>
<div class="spacer-100"></div>

<script type="text/javascript">
	$(document).ready(function(){
	     $('body,html').animate({scrollTop: 550}, 800); 
	});
</script>

@endsection