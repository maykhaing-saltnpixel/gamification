@extends('layouts.app')

@section('title', 'Update Password')

@section('content')

@include('layouts.header')
<div class="spacer-30"></div>
<div class="">
	<!-- <div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-md-2 col-sm-2"></div>
		<div class="col-md-8 col-sm-8 d-none d-sm-block">
			<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
			<div class="bottom-left">
				<a href="{{url('/')}}" style="text-decoration: none;">
					<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
				</a>
			</div>
			<div class="bottom-left2">
				<span class="bread_tu_qui_txt"><a href="{{url('/forget_password')}}" style="text-decoration: none;color: #000;font-family: 'Gotham Book';font-size: 14px;">Update password</a></span>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 d-block d-sm-none">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #1ab6f1;"></i>
			</a>
			>
			<span><a href="{{url('/forget_password')}}" style="font-family: 'Gotham Book';font-size: 14px;text-decoration: none;color: #000;">Update password</a></span>
		</div>
		<div class="col-md-2 col-sm-2"></div>
	</div> -->
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-md-2 col-sm-2"></div>
		<div class="col-md-8 col-sm-8 d-none d-sm-block">
			<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
			<div class="bottom-left">
				<a href="{{url('/')}}" style="text-decoration: none;">
					<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
				</a>
			</div>
			<div class="bottom-left2">
				<span class="bread_tu_qui_txt">Update password</span>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 d-block d-sm-none">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #1ab6f1;"></i>
			</a>
			>
			<span>Update password</span>
		</div>
		<div class="col-md-2 col-sm-2"></div>
	</div>
	<div class="spacer-50"></div>
	<div class="forgetpassword_section d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<h3 style="font-family: 'Gotham Medium';font-size: 30px;">Update Password</h3>
				@include('layouts.messages')
				<div class="spacer-5"></div>
				<div class="row">
					<div class="col-sm-6 forgetform">	
						<div class="row forgetborder">	
							<form action="{{url('/update_password')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
								{{ csrf_field() }}
								<input type="text" class="d-none" name="sec_code" value="{{$sec_code}}">
							    <div class="form-group">
									<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="PASSWORD" value="{{old('password')}}" style="font-family: 'Gotham Light';font-size: 11px;" required>
									@if ($errors->has('password'))
							            <span class="invalid-feedback" role="alert">
							                <strong>{{ $errors->first('password') }}</strong>
							            </span>
							        @else
							        	<div class="invalid-feedback">
								          Please enter a valid password.
								        </div>
							        @endif
							    </div>

							    <div class="form-group">
									<input type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" placeholder="CONFIRM PASSWORD" value="{{old('confirm_password')}}" style="font-family: 'Gotham Light';font-size: 11px;" required>
									@if ($errors->has('confirm_password'))
							            <span class="invalid-feedback" role="alert">
							                <strong>{{ $errors->first('confirm_password') }}</strong>
							            </span>
							        @else
							        	<div class="invalid-feedback">
								        	Please enter a password match with password field.
								        </div>
							        @endif
							    </div>
								
								<div class="spacer-10"></div>

								<img src="{{url('/img/update-password.png')}}" id="forget_btn" style="cursor: pointer;width: 235px;"> <br>

								<button type="submit" class="d-none" id="submit_btn">submit</button>

								<div class="spacer-20"></div>

								<a href="{{url('/login')}}" style="font-family: 'Gotham Light';font-size: 14px;">Login</a> / <a href="{{url('/register')}}" style="font-family: 'Gotham Light';font-size: 14px;">Register</a>
							</form>
						</div>
					</div>
					<div class="col-sm-6 forget">
						<div class="row forget_img_border">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
		<div class="row hpcopyright">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<div class="row d-none d-sm-block">
					<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="row">
							<div class="col-sm-8 col-md-8 footer_div">
								<span>{{ __('messages.copy_right') }}</span>
							</div>
							<div class="col-sm-4 col-md-4 footer_div text-right">
								<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
								</a>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="row d-block d-sm-none" style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{ __('messages.copy_right') }}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
	</div>

	<div class="forgetpassword_mobilesection d-block d-sm-none">
		<div class="container">
			<h3>Login</h3>
			@include('layouts.messages')
			<div class="spacer-5"></div>
			<div class="col-md-12 forgetform">
				<div class="row forgetborder justify-content-center">
						<form action="{{url('/login_check')}}" class="needs-validation" method="post" enctype="multipart/form-data" id="forget_form" novalidate>
							{{ csrf_field() }}
						    <div class="form-group">
								<label style="text-transform: uppercase;font-weight: 700;">Please Enter Your Email Address.<br>You will receive a link to create a new password via email.</label>
								<div class="spacer-20"></div>
								<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="ENTER EMAIL ADDRESS" value="" required>
								@if ($errors->has('email'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('email') }}</strong>
						            </span>
						        @else
						        	<div class="invalid-feedback">
							          Please enter a valid email.
							        </div>
						        @endif
						    </div>
							
							<div class="spacer-20"></div>

							<button type="submit" id="submit_btn" class="d-none" style="cursor: pointer;">submit</button>

							<img src="{{url('/img/forget-password-button.png')}}" id="forget_btn" style="width: 235px;cursor: pointer;">

						</form>
					<br>

					<div class="spacer-20"></div>

					<a href="{{url('/login')}}"" style="color: #000;text-decoration: underline;text-decoration-color: grey;">Login</a> / <a href="{{url('/register')}}" style="color: #000;text-decoration: underline;text-decoration-color: grey;">Register</a>

					<div class="spacer-50"></div>
						
				</div>
			</div>
			<div class="row hpcopyright">
				<div class="col-md-12">
					<div class="row d-none d-sm-block">
						<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
							<div class="row">
								<div class="col-sm-8 col-md-8 footer_div">
									<span>{{ __('messages.copy_right') }}</span>
								</div>
								<div class="col-sm-4 col-md-4 footer_div text-right">
									<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
									</a>
								</div>
							</div>
							
						</div>
					</div>
					
					<div class="row d-block d-sm-none" style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{ __('messages.copy_right') }}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>

	
	<div class="spacer-100"></div>
</div>
<script type="text/javascript">
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();

	$("#forget_btn").click(function() {
		$("#submit_btn").click();
	});
</script>
@endsection