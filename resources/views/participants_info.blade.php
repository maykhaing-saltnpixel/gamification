@extends('layouts.app')

@section('title', 'Participants_info')

@section('content')

@include('layouts.header')
<?php
	$count = 1;
	foreach ($ranking_list as $list) {
		if ($list->id != auth()->user()->id) {
			$count = $count + 1;
		} else {
			$count = $count;
			break;
		}
	}

	$user_arr = array();
	foreach($user_participant as $user){
		array_push($user_arr, $user->game_id);
	}
?>
<div class="spacer-30"></div>
<div class="participant_info_china">
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-md-2 col-sm-2"></div>
		<div class="col-md-8 col-sm-8 d-none d-sm-block">
			<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
			<div class="bottom-left">
				<a href="{{url('/')}}" style="text-decoration: none;">
					<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
				</a>
			</div>
			<div class="bottom-left2">
				<span class="bread_tu_qui_txt">Participants Info</span>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 d-block d-sm-none">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #1ab6f1;"></i>
			</a>
			>
			<span>Participants Info</span>
		</div>
		<div class="col-md-2 col-sm-2"></div>
	</div>
	<div class="spacer-20"></div>

	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<h3 style="font-size: 30px !important;font-family: 'Gotham Book' !important;">Participant Info</h3>
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-sm-3 text-center" style="padding: 0px;background: #1ab4f0;">
					<div class="spacer-20"></div>
					@if(auth()->user()->profile != '' && file_exists(public_path() . '/storage/' .auth()->user()->profile) && file_exists(public_path() . '/storage/' .auth()->user()->profile))
						<img src="{{url('/storage') . '/' .auth()->user()->profile}}" class="rounded-circle mx-auto d-block" style="width: 100px;height: 100px;">
					@else
						<img src="{{url('/img/default-img.png')}}" class="rounded-circle mx-auto d-block" style="width: 100px;height:100px;">
					@endif
					<span class="participant_name" style="text-transform: uppercase;font-family: 'Gotham Light' !important;">{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
					<span class="participant_nation profile_nation_partner1">{{auth()->user()->country}}</span>
					<span class="participant_partnet profile_nation_partner2">PARTNER ID : {{auth()->user()->partner_id}}</span>

					<div class="text-center">
						<a href="{{url('/profile_edit')}}">
							<button class="btn participant_btn1">EDIT PROFILE</button>
						</a>
					</div>	
					
					<div class="">
						<img class="points_nation" src="{{url('/img/participant_separate-line.png')}}">
						<span class="participant_point1">{{auth()->user()->points}}</span>
						<span class="participant_point2">POINTS</span>
					</div>

					<div class="">
						<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}"  class="mx-auto d-block">
						<span class="participant_rankingnation1">{{$count}}</span>
						<span class="participant_rankingnation2">{{auth()->user()->country}}</span>
						<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
					</div>
				</div>
				<div class="col-sm-9" style="padding: 0px;">
					<div class="participant_section1">
						<img src="{{url('/img/participant.jpg')}}" class="participant_img4 w-100">
					</div>	
					<div class="participant_section2">
						<img src="{{url('/img/achivement_bg.png')}}" align="right">
						<span class="participant_achivement">Achievement</span>
					</div>
						
					<div class="achivement col-sm-12">
						
						<div class="row">
							@if(auth()->user()->first_registrant_flg == 0)
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/Achievement_Badge_1.png')}}" class="mx-auto d-block">
									<span>First 500 Registant</span>
								</div>
							@endif
							
							@php $count = 4; @endphp
							@php $remark_i = ''; @endphp
							@php $i = 0; @endphp
							
							@foreach($game_list as $game)
								@if(auth()->user()->first_registrant_flg == 0)
									@if($i == 2)
										@php break; @endphp
									@endif
								@else
									@if($i == 3)
										@php break; @endphp
									@endif
								@endif
								@if (in_array($game->id, $cur_ans)) 
									<div class="col-sm-4 text-center">
										<img src="{{url('/img/Achievement_Badge_'.($i+2).'.png')}}" class="mx-auto d-block">
										<span>Completed week {{$i+1}}</span>
									</div>
								@else
									<div class="col-sm-4 text-center">
										<img src="{{url('/img/Achievement_Badge_Inactive'.($i+2).'.png')}}" class="mx-auto d-block">
										<span>Completed week {{$i+1}}</span>
									</div>
								@endif
								
								@php $count = $count - ($i+1); @endphp
								@php $remark_i = $i+1; @endphp
								@php $i += 1; @endphp
							@endforeach
										
						</div>
						<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-4 text-center">
								<button type="button" class="btnviewmore"  data-toggle="collapse" data-target="#demo">VIEW MORE</button>
							</div>
							<div class="col-sm-4"></div>
						</div>
							
						<div class="spacer-20"></div>
						<div id="demo" class="row collapse">
						@for($m = $i ; $m < count($game_list) ; $m++)
							@if(in_array($game_list[$m]['id'], $cur_ans))
								<div class="col-sm-4 text-center">
									@if(auth()->user()->first_registrant_flg == 0)
										<img src="{{url('/img/Achievement_Badge_'.($m+$i).'.png')}}" class="mx-auto d-block">
									@else
										<img src="{{url('/img/Achievement_Badge_'.($m+$i-1).'.png')}}" class="mx-auto d-block">
									@endif
									<span>Completed week {{$m+1}}</span>
								</div>
							@else
								<div class="col-sm-4 text-center">
									@if(auth()->user()->first_registrant_flg == 0)
										<img src="{{url('/img/Achievement_Badge_Inactive'.($m+$i).'.png')}}" class="mx-auto d-block">
									@else
										<img src="{{url('/img/Achievement_Badge_Inactive'.($m+$i-1).'.png')}}" class="mx-auto d-block">
									@endif
									<span>Completed week {{$m+1}}</span>
								</div>
							@endif
						@endfor	

						@for($k = 0 ; $k < 4 - count($game_list); $k++)
							<div class="col-sm-4 text-center">
								<img src="{{url('/img/Achievement_Badge_Inactive4.png')}}" class="mx-auto d-block">
								<span>Completed week {{count($game_list)+1}}</span>
							</div>
						@endfor		
						</div>

						<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-4 text-center">
								<button type="button" class="btnviewless" id="examplebtn" data-toggle="collapse" data-target="#demo" style="display: none;">VIEW LESS</button>

							</div>
							<div class="col-sm-4"></div>
						</div>
						
					</div>

					<?php 
						$country = Auth::user()->country;
					?>
					<div class="prizes_for_nation d-none d-sm-block">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-sm-12" style="padding: 0px;">
								@if($country == 'China (Greater China)' || $country == 'Hong Kong (Greater China)' || $country == 'Taiwan (Greater China)')
									<img class="w-100" src="{{url('/img/Participants_Info-China.jpg')}}">
								@elseif($country == 'India')
									<img class="w-100" src="{{url('/img/Participants_Info-India.jpg')}}">
								@elseif($country == 'Australia (SPAC)' || $country == 'New Zealand (SPAC)')
									<img class="w-100" src="{{url('/img/Participants_Info-SPAC.jpg')}}">
								@elseif($country == 'Indonesia (SEA-K)' || $country == 'Malaysia (SEA-K)' || $country == 'Philippines (SEA-K)' || $country == 'Singapore (SEA-K)' || $country == 'Thailand (SEA-K)' || $country == 'Vietnam (SEA-K)' || $country == 'Korea (SEA-K)')
									<img class="w-100" src="{{url('/img/Participants_Info-SEA-K.jpg')}}">
								@endif
							</div>
						</div>
					</div>

					<div class="prizes_for_nation d-block d-sm-none">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-sm-12" style="padding: 0px;">
								<img class="w-100" src="{{url('/img/Participants_Info-China.jpg')}}">
							</div>
						</div>
					</div>
				</div>				
			</div>			
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<div class=" d-none d-sm-block">
				<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="row">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{ __('messages.copy_right') }}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div text-right">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;"><span>{{ __('messages.term_condi') }}</span>
							</a>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class=" d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="col-sm-8 col-md-8 footer_div">
					<span>{{ __('messages.copy_right') }}</span>
				</div>
				<div class="col-sm-4 col-md-4 footer_div">
					<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	<div class="spacer-100"></div>
</div>

<script type="text/javascript">

	window.onload=function(){
	  document.getElementById("examplebtn").style.display='none';
	}

	$(".btnviewmore").click(function(){
		// $("#demo").css('display','');
		$(".btnviewless").css('display','');
		$(".btnviewless").show();
		$(".btnviewmore").hide();
	});
	$(".btnviewless").click(function(){
		// $("#demo").css('display','none');
		$(".btnviewless").css('display','none');
		$(".btnviewless").hide();
		$(".btnviewmore").show();
	});
	

</script>
@endsection