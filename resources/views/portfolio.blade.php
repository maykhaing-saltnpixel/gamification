@extends('layouts.app')

@section('title', 'HP Supplies Portfolio')

@section('content')

@include('layouts.header')
@php
	$date = date('Y/m/d');
	$dates = '2019/05/29';
@endphp
	<div class="spacer-30"></div>
	<!-- quiz-schedule -->
	<div class="spacer-30"></div>
	<div class="">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6"  style="padding: 0px;">
				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
						<p style="font-family: 'Gotham Light';font-size: 20px !important;font-weight: normal;">WHY BUY ORIGINAL HP INK AND TONER CARTRIDGES?</p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;font-weight: normal;color: #666666;">Behind each HP Cartridge, there are hundreds of hours of testing and years of engineering and science, to provide customers with printing experiences that amaze.</p>
					</div>
				</div>

				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-3 col-sm-3 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/1.jpg')}}" style="width: 50%;height: auto;">
						<p style="font-family: 'Gotham Medium';font-size: 14px !important;margin-bottom: 0px;margin-top: 10px;">DESIGNED TO WORK THE FIRST TIME, EVERY TIME<sup>1</sup></p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;font-weight: normal;color: #666666;">Rely on Original HP Cartridges to perform consistently.</p>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/2.jpg')}}" style="width: 50%;height: auto;">
						<p style="font-family: 'Gotham Medium';font-size: 14px !important;margin-bottom: 0px;margin-top: 10px;">QUALITY PRINTS YOU CAN TAKE PRIDE IN</p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;font-weight: normal;color: #666666;">Experience outstanding print quality when you use Original HP Cartridges.</p>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/3.jpg')}}" style="width: 50%;height: auto;">
						<p style="font-family: 'Gotham Medium';font-size: 14px !important;margin-bottom: 0px;margin-top: 10px;">HE ENVIRONMENTAL CHOICE<sup>2</sup></p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;font-weight: normal;color: #666666;">Cartridges designed with the planet in mind for easy recycling and less waste.</p>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/4.jpg')}}" style="width: 50%;height: auto;">
						<p style="font-family: 'Gotham Medium';font-size: 14px !important;margin-bottom: 0px;margin-top: 10px;">BETTER RESULTS. BETTER TOGETHER</p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;font-weight: normal;color: #666666;">Only Original HP Cartridges are precisely tuned with HP printers for reliable quality.</p>
					</div>
				</div>

				<div class="spacer-50"></div>

				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
						<p style="font-family: 'Gotham Light';font-size:24px !important; ">IT’S ALL ABOUT SCIENCE</p>
					</div>
				</div>

				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/video.jpg')}}" style="width: 100%;height: auto;">
					</div>
				</div>

				<div class="spacer-30"></div>

				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
						<p style="font-family: 'Gotham Light';font-size:16px !important;color: #666666; ">Learn how HP is supporting education and economic opportunity for people<br> in Haiti through our closed loop ink cartridge program.</p>
					</div>
				</div>

				<div class="spacer-30"></div>

				<div class="row text-center" style="width: 100%;margin-left: 0px;">
					<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/product_1.png')}}" style="width: 80%;height: auto;">
						<p style="font-family: 'Gotham Light';font-size:24px !important;color: #000;margin-top: 20px;">INKS</p>
						<p style="font-family: 'Gotham Light';font-size:16px !important;color: #0099cc;text-transform: uppercase;">
							Singapore   |   Malaysia   |   Thailand
						</p>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
						<img src="{{url('/img/portfolio/product_2.png')}}" style="width: 80%;height: auto;">
						<p style="font-family: 'Gotham Light';font-size:24px !important;color: #000;margin-top: 20px;">TONERS</p>
						<p style="font-family: 'Gotham Light';font-size:16px !important;color: #0099cc;text-transform: uppercase;">
							Singapore   |   Malaysia   |   Thailand
						</p>
					</div>
				</div>

				<div class="spacer-30"></div>
				
				<div class="row text-left" style="width: 100%;margin-left: 0px;">
					<p style="font-family: 'Gotham Light';font-size: 9px !important;">1. Ink based on an 2016 BLI study commissioned by HP for Black & Color cartridges 564X, 61XL, 678, 703, 802, 901 sold in Asia Pacific;<br>keypointintelligence.com/media/1094/hpinkaprefill.pdf (pdf-download PDF 1.25 MB). Toner based on 2015 SpencerLab study commissioned by HP for cartridges sold in Asia (AU, CN, IN, KR, TH, TW) 80A, 85A, 88A cartridges; spencerlab.com/reports/HPReliability-PanAsia2015NB.pdf (pdf-download PDF 2.33 MB).</p>

					<p style="font-family: 'Gotham Light';font-size: 9px !important;">2. HP Planet Partners Program availability varies. For details, see www.hp.com/recycle. On recycled materials in cartridges, see www.hp.com/go/recycledcontent.</p>
				</div>
				
				<div style="height: 50px;"></div>
			</div>
			<div class="col-3" style="padding: 0px;"></div>
		</div>
	</div>
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<div class=" d-none d-sm-block">
				<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="row">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{__('messages.copy_right')}}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div text-right">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;"><span>{{__('messages.term_condi')}}</span>
							</a>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class=" d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="col-sm-8 col-md-8 footer_div">
					<span>{{__('messages.copy_right')}}</span>
				</div>
				<div class="col-sm-4 col-md-4 footer_div">
					<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;"><span>{{__('messages.term_condi')}}</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="spacer-100"></div>
	</div>
@endsection