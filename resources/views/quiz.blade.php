@extends('layouts.app')

@section('title', $game->game_title)

@section('content')
@include('layouts.header')

<link rel="stylesheet" href="{{url('/css/progress.css?v=' . time())}}">
<div class="spacer-30"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt">Tutorials & Quizzes</span>
			<!-- <span class="game_title_quiz">WEEK 1 - {{$game->game_title}}</span> -->
		</div>
		<div class="bottom-left3">
			<span class="bread_tu_qui_txt">WEEK 1 - 
				@if (App::isLocale('cn'))
					{{$game->cn_game_title}}
				@else
					{{$game->game_title}}
				@endif
			</span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span>Tutorials & Quizzes</span>
		>
		<span style="">WEEK 1 - 
			@if (App::isLocale('cn'))
				{{$game->cn_game_title}}
			@else
				{{$game->game_title}}
			@endif
		</span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>
<div class="spacer-30"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-sm-3 col-xs-12"></div>
	<div class="col-sm-6 col-xs-12 quiz_space_hide" style="background-color: #c0d2dc !important;">
		<div class="row question_banner">
			<div class="col-sm-9">
				<nav class="form-steps_questions">
					<div class="form-steps__item form-steps__item--completed">
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">1</span>
							<span class="form-steps__item-text">{{ __('messages.tutorial_progress') }}</span>
						</div>
					</div>
					@if($progress == 1)
						<div class="form-steps__item form-steps__item--active">
					@else
						<div class="form-steps__item form-steps__item--completed">
					@endif
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">2</span>
							<span class="form-steps__item-line"></span>
							<span class="form-steps__item-text">{{ __('messages.q1_progress') }}</span>
						</div>
					</div>
					@if($progress == 2)
						<div class="form-steps__item form-steps__item--active">
					@elseif($progress == 1)
						<div class="form-steps__item">
					@else
						<div class="form-steps__item form-steps__item--completed">
					@endif
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">3</span>
							<span class="form-steps__item-line"></span>
							<span class="form-steps__item-text">{{ __('messages.q2_progress') }}</span>
						</div>
					</div>
					@if($progress == 3)
						<div class="form-steps__item form-steps__item--active">
					@elseif($progress == 1 || $progress == 2)
						<div class="form-steps__item">
					@else
						<div class="form-steps__item form-steps__item--completed">
					@endif
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">4</span>
							<span class="form-steps__item-line"></span>
							<span class="form-steps__item-text">{{ __('messages.q3_progress') }}</span>
						</div>
					</div>
				</nav>
			</div>
			<div class="col-sm-3 text-center d-none d-sm-block" style="padding-left: 0px;">
				<p id="some_div" class="timer"></p>
			</div>
		</div>
		<div class="row d-block d-sm-none" style="background-color: #00adef;color: #fff;">
			<div class="col-sm-12" style="padding: 0px;">
				<div class="row">
					<div class="col-6" style="padding: 10px;text-align: right;">
						<span style="font-size: 20px;font-family: 'Gotham Light';">Time Bonus</span>
					</div>
					<div class="col-6" style="padding: 10px;text-align: left;">
						<span style="font-size: 20px;font-family: 'Gotham Light';" id="some_div2"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="background-color: #e8f2f7;padding: 10px;font-family: 'Gotham Book' !important;">
			<div class="col-12 question_div" style="border: 1px solid #fff;padding: 15px;width: 100%;">
				<p style="font-size: 18px;">
					@if (App::isLocale('cn'))
						{!! nl2br(e($question->cn_question)) !!}
						<input type="hidden" name="language" id="language" value="cn">
					@else
						{!! nl2br(e($question->question)) !!}
						<input type="hidden" name="language" id="language" value="en">
					@endif
				</p>
				
				@php $ans_correct = ''; @endphp
				<form action="{{url('/start_quiz/save')}}" method="post" id="quiz_form">
					{{ csrf_field() }}
					<input type="hidden" name="bonus_time" id="bonus_time" value="">
					<input type="hidden" name="question_id" id="question_id" value="{{$question->id}}">
					<input type="hidden" name="game_id" id="game_id" value="{{$game->id}}">
					<input type="hidden" name="question_type" id="question_type" value="{{$question->question_type}}">
					@foreach($answer as $ans)
						@if($ans->question_id == $question->id)
							@if($ans->correct_flg == 0)
								@if (App::isLocale('cn'))
									@php $ans_correct = $ans->cn_answer; @endphp
								@else
									@php $ans_correct = $ans->answer; @endphp
								@endif
							@endif
							@if($question->question_type == 1)
								<div class="row">
									<div class="col-1 text-center" style="padding-right: 0px;">
										<input type="radio" class="ans_radio" name="user_ans" id="user_ans_{{$ans->id}}" value="{{$ans->id}}">
									</div>
									<div class="col-11 text-left" style="">
										<label for="user_ans_{{$ans->id}}">
											@if (App::isLocale('cn'))
												{{$ans->cn_answer}}
											@else
												{{$ans->answer}}
											@endif
										</label>
									</div>
								</div>
								<input type="hidden" name="user_ans_correct_{{$ans->id}}" id="user_ans_correct_{{$ans->id}}" value="{{$ans->correct_flg}}">
							@else
								<div class="row">
									<div class="col-1 text-center" style="padding-right: 0px;">
										<input type="checkbox" class="ans_radio" name="user_ans[]" id="user_ans_{{$ans->id}}" value="{{$ans->id}}">
									</div>
									<div class="col-11 text-left" style="">
										<label for="user_ans_{{$ans->id}}">{{$ans->answer}}</label>
									</div>
								</div>
							@endif
						@endif
					@endforeach
					<input type="hidden" name="user_ans_correct" id="user_ans_correct" value="{{$ans_correct}}">
					<div class="spacer-20"></div>
					<img src="{{url('/img/submit_button.png')}}" id="submit_q_btn" alt="" style="cursor: pointer;">
				</form>
			</div>
		</div>
		<input type="hidden" name="user_answer" id="user_answer" value="">

		<div class="row d-none d-sm-block">
			<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="row">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{ __('messages.copy_right') }}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div text-right">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
						</a>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
			<div class="col-sm-8 col-md-8 footer_div">
				<span>{{ __('messages.copy_right') }}</span>
			</div>
			<div class="col-sm-4 col-md-4 footer_div">
				<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12"></div>
</div>
<div class="spacer-100"></div>

<script type="text/javascript">
	$(document).ready(function(){
	     $('body,html').animate({scrollTop: 550}, 800); 
	});
	var question_id = $("#question_id").val();
	var game_id = $("#game_id").val();
	var question_type = $("#question_type").val();
	$("#submit_q_btn").click(function() {
		doSomething();
		if (question_type == 1) {
			$('.ans_radio').each(function () {
	            if ($(this).prop('checked')) {
	            	var ans_id = $(this).val();
	            	$("#user_answer").val(ans_id);
	                var correct_flg = $("#user_ans_correct_"+ans_id).val();
	                if (correct_flg == 0) {
	                	$("#quiz_form").submit();
	                } else {
	                	var ans = $("#user_ans_correct").val();
	                	var bonus_t = $("#bonus_time").val();
	                	$(".question_div").html('');
	                	if ($("#language").val() == 'cn') {
	                		var correct_text = '正确的答案是';
	                		var full_stop = '。';
	                	} else {
	                		var correct_text = 'The correct answer is';
	                		var full_stop = '.';
	                	}
	                	var html = '<p class="next-question_text" style="font-size: 18px;">'+correct_text+' <b>'+ans+full_stop+'</b></p>'+
	                				'<div class="spacer-20"></div>'+
									'<img src="{{url("/img/next-question_button.png")}}" onclick="nextQ()" alt="" class="next_question_btn" style="width: 25%;cursor: pointer;">'+
									'<form action="{{url("/start_quiz/save")}}" method="post" id="quiz_form">'+
										'{{ csrf_field() }}'+
										'<input type="hidden" name="question_id" id="question_id" value="{{$question->id}}">'+
										'<input type="hidden" name="game_id" id="game_id" value="{{$game->id}}">'+
										'<input type="hidden" name="question_type" id="question_type" value="{{$question->question_type}}">'+
										'<input type="hidden" class="ans_radio" name="user_ans" id="user_ans_checked" value="">'+
										'<input type="hidden" name="user_ans_correct_'+ans_id+'" id="user_ans_correct_'+ans_id+'" value="1">'+
										'<input type="hidden" name="bonus_time" id="bonus_time" value="'+bonus_t+'">'+
									'</form>';
	                	$(".question_div").html(html);
	                }
	            }
	        });
		} else {
			$("#quiz_form").submit();
		}
	});

	$("#next_q_btn").click(function() {
		
	});

	function nextQ() {
		var ans_id = $("#user_answer").val();
		$("#user_ans_checked").val(ans_id);
		$("#quiz_form").submit();
	}

	var timeLeft = 20;
	var timeLeft2 = 20;
	var elem = document.getElementById('some_div');
	var timerId = setInterval(countdown, 1000);
	var elem2 = document.getElementById('some_div2');
	var timerId2 = setInterval(countdown2, 1000);

	function countdown() {
	    if (timeLeft == -1) {
	        clearTimeout(timerId);
	        doSomething();
	    } else {
	        elem.innerHTML = '0:'+timeLeft;
	        timeLeft--;
	    }
	}

	function countdown2() {
	    if (timeLeft2 == -1) {
	        clearTimeout(timerId2);
	        doSomething2();
	    } else {
	        elem2.innerHTML = '0:'+timeLeft2;
	        timeLeft2--;
	    }
	}

	function doSomething() {
	    var bonus = document.getElementById('some_div').innerHTML;
	    $("#bonus_time").val(bonus);
	    clearTimeout(timerId);
	}
	function doSomething2() {
	    var bonus = document.getElementById('some_div2').innerHTML;
	    $("#bonus_time").val(bonus);
	    clearTimeout(timerId);
	}
</script>
@endsection