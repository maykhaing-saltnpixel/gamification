@extends('layouts.app')

@section('title', 'TUTORIALS & QUIZZES')

@section('content')

@include('layouts.header')
@php
	$date = Date('Y/m/d');
	$dates = '2019/08/30';
	if(substr(URL::to('/'), 7, 2) == 'cn') {
		$url = str_replace('cn.', '', URL::to('/'));
	} else {
		$url = URL::to('/');
	}
@endphp
<div class="spacer-30"></div>

<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt">{{ __('messages.hp_menu1') }}</span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span>{{ __('messages.hp_menu1') }}</span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>
<div class="spacer-20"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-3 col-sm-3"></div>
	<div class="col-md-6 col-sm-6 text-center">
		<div class="">
			<p class="text-center" style="font-size: 30px !important;font-family: 'Gotham Book' !important;text-transform: uppercase;">{{ __('messages.hp_menu1') }}</p>
		</div>
	</div>
	<div class="col-md-3 col-sm-3"></div>
</div>

<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-3 col-sm-3"></div>
	<div class="col-md-6 col-sm-6">
		<div class="row">
			@php $week_count = 1; @endphp
			@foreach($user_answer_list as $u_a_list)
				<div class="col-sm-12 col-md-12 col-lg-6">
					@php
						$publish_date = date('Y/m/d', strtotime($u_a_list->from_date));
						$end_date = date('Y/m/d', strtotime($u_a_list->to_date));					
					@endphp
					@if($date >= $publish_date && $date <= $end_date)
						<img src="{{$url}}/storage/{{$u_a_list->game_image}}" alt="{{$u_a_list->game_title}}" align="left" style="width: 100%;">
					@else
						<img src="{{$url}}/storage/{{$u_a_list->inactive_game_image}}" alt="{{$u_a_list->inactive_game_image}}" align="left" style="width: 100%;">
					@endif
					<div class="col-12">
						<h5 style="font-family: 'Gotham Book' !important;font-size: 24px;">WEEK {{$week_count}}:</h5>
						<h5 class="game_title" style="font-family: 'Gotham XLight' !important;font-size: 24px;text-transform: uppercase;">
							@if (App::isLocale('cn'))
								{{$u_a_list->cn_game_title}}
							@else
								{{$u_a_list->game_title}}
							@endif
						</h5>
						<h5 style="font-family: 'Gotham Medium' !important;font-size: 18px;color: #3793d4;">
							{{date('d M', strtotime($u_a_list->from_date))}} – {{date('d M', strtotime($u_a_list->to_date))}}
						</h5>
						@if(Auth::check())
							@if (in_array($u_a_list->id, $cur_ans)) 
								<h5 style="font-family: 'Gotham Book' !important;font-size: 16px;">You have completed week {{$week_count}}.</h5>
							@else
								@if($date > $end_date)
									<h5 style="font-family: 'Gotham Book' !important;font-size: 16px;">This week was finished.</h5>
								@elseif($date >= $publish_date && $date <= $end_date)
									@if(App::isLocale('cn'))
										<a href="{{url('/game/start/'.$u_a_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese_new.png')}}" style="width: 180px;">
										</a>
									@else
										<a href="{{url('/game/start/'.$u_a_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" style="width: 180px;">
										</a>
									@endif
								@elseif($date < $publish_date)
									@if(App::isLocale('cn'))
										<img src="{{url('/img/chinese_commingsoon.png')}}" style="width: 180px;">
									@else
										<img src="{{url('/img/commingsoon.png')}}" style="width: 180px;">
									@endif
								@endif
							@endif
						@else
							@if($date > $end_date)
								<h5 style="font-family: 'Gotham Book' !important;font-size: 16px;">This week was finished.</h5>
							@elseif($date >= $publish_date && $date <= $end_date)
								@if(App::isLocale('cn'))
									<a href="{{url('/game/start/'.$u_a_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese_new.png')}}" style="width: 180px;">
									</a>
								@else
									<a href="{{url('/game/start/'.$u_a_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" style="width: 180px;">
									</a>
								@endif
							@elseif($date < $publish_date)
								@if(App::isLocale('cn'))
									<img src="{{url('/img/chinese_commingsoon.png')}}" style="width: 180px;">
								@else
									<img src="{{url('/img/commingsoon.png')}}" style="width: 180px;">
								@endif
							@endif
						@endif
						
					</div>
				</div>
				@php $week_count = $week_count+1; @endphp
			@endforeach
		</div>
		<div class="spacer-30"></div>
		<div class="row d-none d-sm-block">
			<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="row">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{ __('messages.copy_right') }}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div text-right">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
						</a>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
			<div class="col-sm-8 col-md-8 footer_div">
				<span>{{ __('messages.copy_right') }}</span>
			</div>
			<div class="col-sm-4 col-md-4 footer_div">
				<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3"></div>
</div>
<div class="spacer-100"></div>

<!-- @include('layouts.footer-home') -->

@endsection