@extends('layouts.app')

@section('title', $game->game_title)

@section('content')

@include('layouts.header')
@php
	$date = Date('Y/m/d');
	$dates = '2019/05/13';
	if(substr(URL::to('/'), 7, 2) == 'cn') {
		$url = str_replace('cn.', '', URL::to('/'));
	} else {
		$url = URL::to('/');
	}

@endphp

<link rel="stylesheet" href="{{url('/css/progress.css?v=' . time())}}">
<div class="spacer-30"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt">{{ __('messages.hp_menu1') }}</span>
		</div>
		<div class="bottom-left3">
			<span class="bread_tu_qui_txt">WEEK 1 - 
				@if (App::isLocale('cn'))
					{{$game->cn_game_title}}
				@else
					{{$game->game_title}}
				@endif
			</span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		<i class="fa fa-chevron-right" style="font-size: 10px;"></i>
		<span>{{ __('messages.hp_menu1') }}</span>
		<i class="fa fa-chevron-right" style="font-size: 10px;"></i>
		WEEK 1 - 
		@if (App::isLocale('cn'))
			{{$game->cn_game_title}}
		@else
			{{$game->game_title}}
		@endif
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>
<div class="spacer-20"></div>

@if(count($already_answered) > 0)
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-sm-3 col-xs-12"></div>
		<div class="col-sm-6 col-xs-12" style="background-color: #c0d2dc">
			<div class="row" style="background-color: #e8f2f7;padding: 10px;">
				<div class="col-12" style="border: 1px solid #fff;padding: 15px;width: 100%;">
					<p style="font-family: 'Gotham Book' !important;font-size: 18px;">You've already participated this week.</p>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-sm-3 col-xs-12"></div>
		<div class="col-sm-6 col-xs-12">
			<div class="row tuto_banner">
				<div class="col-9">
					
					
					<nav class="form-steps">
						<div class="form-steps__item form-steps__item--active">
							<div class="form-steps__item-content">
								<span class="form-steps__item-icon">1</span>
								<span class="form-steps__item-text">{{ __('messages.tutorial_progress') }}</span>
							</div>

						</div>
						<div class="form-steps__item">
							<div class="form-steps__item-content">
								<span class="form-steps__item-icon">2</span>
								<span class="form-steps__item-line"></span>
								<span class="form-steps__item-text">{{ __('messages.q1_progress') }}</span>
							</div>
						</div>
						<div class="form-steps__item">
							<div class="form-steps__item-content">
								<span class="form-steps__item-icon">3</span>
								<span class="form-steps__item-line"></span>
								<span class="form-steps__item-text">{{ __('messages.q2_progress') }}</span>
							</div>
						</div>
						<div class="form-steps__item">
							<div class="form-steps__item-content">
								<span class="form-steps__item-icon">4</span>
								<span class="form-steps__item-line"></span>
								<span class="form-steps__item-text">{{ __('messages.q3_progress') }}</span>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div class="row d-block d-sm-none" style="background-color: #00adef;color: #fff;">
				<div class="col-sm-12" style="padding: 0px;">
					<div style="padding: 10px;text-align: center;">
						<span style="font-size: 25px;">Tutorial</span>
					</div>
				</div>
			</div>
			<div class="row" style="background-color: #e8f2f7;padding: 10px;">
				<div class="col-12" style="border: 1px solid #fff;padding: 15px;width: 100%;">
					@if($date < '2019/08/29')
						<p class="tuto-learning-text">{{ __('messages.tuto_intro_week_1') }}</p>
					@elseif($date == '2019/08/29')
						<p class="tuto-learning-text">{{ __('messages.tuto_intro_week_1') }}</p>
					@elseif($date == '2019/08/30')
						<p class="tuto-learning-text">{{ __('messages.tuto_intro_week_2') }}</p>
					@elseif($date == '2019/08/31')
						<p class="tuto-learning-text">{{ __('messages.tuto_intro_week_3') }}</p>
					@elseif($date >= '2019/09/01')
						<p class="tuto-learning-text">{{ __('messages.tuto_intro_week_4') }}</p>
					@endif
					
					@if(App::isLocale('cn'))
						<p class="text-center" style="font-size: 30px !important;font-weight: 700;">
							@if($date < '2019/08/29')
								{{ __('messages.week1_tuto_title') }}
							@elseif($date == '2019/08/29')
								{{ __('messages.week1_tuto_title') }}
							@elseif($date == '2019/08/30')
								{{ __('messages.week2_tuto_title') }}
							@elseif($date == '2019/08/31')
								{{ __('messages.week3_tuto_title') }}
							@elseif($date >= '2019/09/01')
								{{ __('messages.week4_tuto_title') }}
							@endif
						</p>
					@else
						<p class="text-center" style="font-family: 'Gotham Bold' !important;font-size: 26px !important;">
							@if($date < '2019/08/29')
								{{ __('messages.week1_tuto_title') }}
							@elseif($date == '2019/08/29')
								{{ __('messages.week1_tuto_title') }}
							@elseif($date == '2019/08/30')
								{{ __('messages.week2_tuto_title') }}
							@elseif($date == '2019/08/31')
								{{ __('messages.week3_tuto_title') }}
							@elseif($date >= '2019/09/01')
								{{ __('messages.week4_tuto_title') }}
							@endif
						</p>
					@endif
					@php
						$count = 1;
						$chinese_count = 1;
						$tuto_count = count($tutorial);
					@endphp
					@if(App::isLocale('en'))
						@foreach($tutorial as $tut)
							@if($tut->tuto_type == 1)
								<p class="text-center" style="font-family: 'Gotham Bold' !important;font-size: 20px !important;color: #0097d5 !important;">
									{{$tut->tuto_value}}
								</p>
								@php $count = (int)$count+1; @endphp
							@elseif($tut->tuto_type == 2)
								@foreach($tuto_image as $tut_image)
									@if($tut_image->tuto_id == $tut->id)
										<div class="row" style="width: 100%;margin-right: 0px !important;margin-left: 0px !important;">
											<img src="{{$url}}/storage/{{$tut_image->photo}}" style="width: 100%;height: 100%;margin: 0 auto;" alt="">
										</div>
									@endif
								@endforeach
								@php $count = (int)$count+1; @endphp
							@elseif($tut->tuto_type == 3)
							@endif

							@if($count <= $tuto_count)
								<img src="{{url('/img/hr_green.png')}}" alt="" style="width: 100%;height: auto;margin: 0 auto;padding-bottom: 20px;">
							@endif
						@endforeach
					@elseif(App::isLocale('cn'))
						@foreach($chinese_tutorial as $chinese_tut)
							@if($chinese_tut->tuto_type == 1)
								<p class="text-center" style="font-family: 'Gotham Bold' !important;font-size: 20px !important;color: #0097d5 !important;">
									{{$chinese_tut->tuto_value}}
								</p>
								@php $count = (int)$count+1; @endphp
							@elseif($chinese_tut->tuto_type == 2)
								@foreach($chinese_tuto_image as $chinese_tut_image)
									@if($chinese_tut_image->tuto_id == $chinese_tut->id)
										<div class="row" style="width: 100%;margin-right: 0px !important;margin-left: 0px !important;">
											<img src="{{$url}}/storage/{{$chinese_tut_image->photo}}" style="width: 100%;height: 100%;margin: 0 auto;" alt="">
										</div>
									@endif
								@endforeach
								@php $chinese_count = (int)$chinese_count+1; @endphp
							@elseif($chinese_tut->tuto_type == 3)
							@endif

							@if($chinese_count <= $tuto_count)
								<img src="{{url('/img/hr_green.png')}}" alt="" style="width: 100%;height: auto;margin: 0 auto;padding-bottom: 20px;">
							@endif
						@endforeach
					@endif

					@if($date >= '2019/05/20' && $date <= '2019/05/26')
						<p style="font-weight: bolder;font-size: 9px !important;">{{ __('messages.week2_footnote') }}</p>
						<p class="week2_foot_note">{{ __('messages.week2_footnote_txt1') }} http://h20195.www2.hp.com/v2/getpdf.aspx/4AA7-1249ENW.pdf</p>
						<p class="week2_foot_note">{{ __('messages.week2_footnote_txt2') }}</p>
						<p class="week2_foot_note">{{ __('messages.week2_footnote_txt3') }}</p>
						<p class="week2_foot_note">{{ __('messages.week2_footnote_txt4') }}</p>
						<p class="week2_foot_note">{{ __('messages.week2_footnote_txt5') }}</p><br>
					@endif

					@if(App::isLocale('en'))
						<a href="{{url('/start_quiz/'.$game->id)}}">
							<img class="tuto-start-btn" src="{{url('/img/start-quiz-now_button.png')}}" alt="" style="width: 25%;">
						</a>
					@else
						<a href="{{url('/start_quiz/'.$game->id)}}">
							<img class="tuto-start-btn" src="{{url('/img/start-now_button_chinese_new.png')}}" alt="" style="width: 25%;">
						</a>
					@endif
				</div>
			</div>
			<div class="row d-none d-sm-block">
				<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="row">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{__('messages.copy_right')}}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div text-right">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
							</a>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="col-sm-8 col-md-8 footer_div">
					<span>{{__('messages.copy_right')}}</span>
				</div>
				<div class="col-sm-4 col-md-4 footer_div">
					<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-3 col-xs-12"></div>
	</div>
@endif
<div class="spacer-100"></div>

<script type="text/javascript">
	$(document).ready(function(){
	     $('body,html').animate({scrollTop: 550}, 800); 
	});
</script>
@endsection