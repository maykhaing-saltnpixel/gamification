@extends('layouts.app')

@section('title', 'Login')

@section('content')

@include('layouts.header')

<div class="spacer-50"></div> 

<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt login_bread_tu_qui_txt"><a style="text-decoration: none;color: #000;font-size: 15px;">{{__('messages.login')}}</a> / <a href="{{url('/register')}}" style="text-decoration: none;color: #000;font-size: 15px;">{{ __('messages.register') }}</a></span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span>{{__('messages.login')}} / <a href="{{url('/register')}}">{{ __('messages.register') }}</a></span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>

<div class="spacer-20"></div>

<div class="login_section d-none d-sm-block">
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<h3>{{ __('messages.login_title') }}</h3>
			@include('layouts.messages')
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-6 loginform">	
					<div class="row loginborder">
						<form action="{{url('/login_check')}}" class="needs-validation" id="login_form" method="post" enctype="multipart/form-data" novalidate>
						{{ csrf_field() }}
						    <div class="form-group">
								<label>{{__('messages.email_address')}}</label>
								<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{__('messages.your_email_address')}}" value="{{old('email')}}" autocomplete="off" required>
								@if ($errors->has('email'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('email') }}</strong>
						            </span>
						        @else
						        	<div class="invalid-feedback">
							          Please enter a valid email.
							        </div>
						        @endif
						    </div>
						    <div class="form-group">
								<label>{{__('messages.password')}}</label>
								<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{__('messages.enter_valid_pwd')}}" id="password" required>
								@if ($errors->has('password'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('password') }}</strong>
						            </span>
						        @else
						        	<div class="invalid-feedback">
							          Please enter a password.
							        </div>
						        @endif
								
						    </div>
							
							<div class="spacer-20"></div>

							
							@if (App::isLocale('cn'))
							<img src="{{url('/img/login_button_chinese.png')}}" id="login_btn" style="cursor: pointer;"> <br>
							@else
							<img src="{{url('/img/login-button.png')}}" id="login_btn" style="cursor: pointer;"> <br>
							@endif

							<div class="spacer-20"></div>

							<a href="{{url('/forget_password')}}">{{__('messages.forget_pwd')}}</a>

							<!-- <button type="submit" id="submit_btn" class="btn btn-primary">Submit</button> -->
						</form>
					</div>			
				</div>
				<div class="col-sm-6 login">
					<div class="row login_img_border">
						<div class="login_over_img1">{{__('messages.newhere')}}</div>
						<div class="login_over_img2">
							<a href="{{url('/register')}}">{{__('messages.click_to_register')}}.</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
	<div class="row hpcopyright"  style="padding: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<div class="row d-none d-sm-block">
				<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="row">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{__('messages.copy_right')}}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div text-right">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
							</a>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
				<div class="col-sm-8 col-md-8 footer_div">
					<span>{{__('messages.copy_right')}}</span>
				</div>
				<div class="col-sm-4 col-md-4 footer_div">
					<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
</div>

<div class="login_mobilesection d-block d-sm-none">
	<div class="container">
		<h3>{{__('messages.login_title')}}</h3>
		@include('layouts.messages')
		<div class="spacer-5"></div>
		<div class="col-md-12 loginform">
			<div class="row loginborder justify-content-center">
				<form action="{{url('/login_check')}}" class="needs-validation" id="login_form_xs" method="post" enctype="multipart/form-data" novalidate>
				{{ csrf_field() }}
				    <div class="form-group">
						<label>{{__('messages.email_address')}}</label>
						<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{__('messages.your_email_address')}}" value="{{old('email')}}" required>
						@if ($errors->has('email'))
				            <span class="invalid-feedback" role="alert">
				                <strong>{{ $errors->first('email') }}</strong>
				            </span>
				        @else
				        	<div class="invalid-feedback">
					          Please enter a valid email.
					        </div>
				        @endif
				    </div>
				    <div class="form-group">
						<label>{{__('messages.password')}}</label>
						<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{__('messages.enter_valid_pwd')}}" id="password" required>
						@if ($errors->has('password'))
				            <span class="invalid-feedback" role="alert">
				                <strong>{{ $errors->first('password') }}</strong>
				            </span>
				        @else
				        	<div class="invalid-feedback">
					          Please enter a password.
					        </div>
				        @endif
						
				    </div>
					
					<div class="spacer-20"></div>

					@if (App::isLocale('cn'))
					<img src="{{url('/img/login_button_chinese.png')}}" id="login_btn_xs" style="cursor: pointer;"> <br>
					@else
					<img src="{{url('/img/login-button.png')}}" id="login_btn_xs" style="cursor: pointer;"> <br>
					@endif

					<div class="spacer-20"></div>

					<a href="{{url('/forget_password')}}">{{__('messages.forget_pwd')}}</a>

					<div class="spacer-20"></div>

					<span style="font-weight: 700;">{{__('messages.newhere')}}</span>
					<span><a href="{{url('/register')}}"  style="text-decoration: none !important;color: blue;">{{__('messages.click_to_register')}}</a></span>
					
				</form>
			</div>
		</div>
		
		<div class="row hpcopyright">
			<div class="col-md-12">
				<div class="row d-none d-sm-block">
					<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="row">
							<div class="col-sm-8 col-md-8 footer_div">
								<span>{{__('messages.copy_right')}}</span>
							</div>
							<div class="col-sm-4 col-md-4 footer_div text-right">
								<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-sm-none" style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{__('messages.copy_right')}}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="spacer-20"></div>
<script type="text/javascript">
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();

	$("#login_btn").click(function() {
		$("#login_form").submit();
	});

	$("#login_btn_xs").click(function() {
		$("#login_form_xs").submit();
	});
</script>
@endsection