<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<p>
		Click the link below to reset your password.
	</p>

	<p>
		Otherwise ignore this email.
	</p>

	<p>
		@php
			$firstCharacter = substr($link, 0, 1);
			if($firstCharacter == '=') {
				$str = ltrim($link, '='); 
			} else {
				$str = $link;
			}
		@endphp
		<a href="{{$str}}">Reset Password</a>
		<!-- {{$str}} -->
	</p>
</body>
</html>