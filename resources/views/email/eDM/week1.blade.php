<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title>Week1 Gamification eDM</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
    <style>
      .fallback-font {
      font-family:Arial,sans-serif !important;
      }
    </style>
    <![endif]-->
    
        <!--[if mso 15]><style>
      .fallback-font {
      font-family:Arial,sans-serif !important;
      }
    </style><![endif]-->
    
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link rel="stylesheet" type="text/css" href="http://www.www8-hp.com/sg/en/system/styles/hpi/hpi-fontface-core.css?v=4.0.2"> -->
    <!--<![endif]-->
    <link rel="stylesheet" type="text/css" href="http://www.www8-hp.com/sg/en/system/styles/hpi/hpi-fontface-core.css?v=4.0.2">
    
    <!-- Web Font / @font-face : END -->
	<style type="text/css">
		html,body{
			margin:0 auto !important;
			padding:0 !important;
			height:100% !important;
			width:100% !important;
			font-family:'HPSimplifiedLight', arial;
		}
	body,td,th {
		font-family: HPSimplifiedLight, arial;
		}
    </style>
</head>

<body>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top" bgcolor="#bee8f6">
		  <img src="{{$message->embed($kv_link)}}" width="700" height="530" class="fallback-font" style="display: inline-block; white-space: pre; font-family:HPSimplifiedLight, arial; background-color: #bee8f6; color:#000000; outline: none; border: 0px; font-size:25px; line-height:28px;" alt="
		  Become an HP Planet Protector.
		  Week 1 quiz starts now! 
		  13-19 MAY
		  ">
	  </td>
    </tr>
	  
	<tr>
      <td align="left" valign="top" bgcolor="#FFFFFF">
			<table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
				<tbody>
				  <tr>
					<td width="40" align="left" valign="top"><img src="{{$message->embed($space)}}" width="40" height="30" style="display: block;" alt=""></td>
					<td align="left" valign="top" class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:30px; color:#000000;">
						<table width="620" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
								  <td><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
								</tr>
								<tr>
								  <td class="fallback-font" style="font-family:HPSimplified, arial; font-size:30px; color:#000000; text-align: center; text-transform: uppercase; line-height: 34px;">Week 1: <br>
								  <span style="font-family:HPSimplifiedLight, arial; font-size:30px; color:#000000; text-align: center; text-transform: uppercase; line-height: 34px;">The ocean Clean-up</span></td>
								</tr>
								<tr>
								  <td><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
								</tr>
								<tr>
								  <td class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:18px; color:#000000; text-align: center; line-height: 22px;">According to the World Economic Forum, about 800 billion kilograms of <br/>plastic enters the world's oceans every year. <br><br>

								 As part of our sustainability initiative, we hull plastics from the ocean and <br/>upcycle them to manufacture new Original HP Ink and Toner cartridges. By using ocean-bound plastics, we are working towards improving our resource efficiency. </td>
								</tr>
							</tbody>
						</table>
					</td>
					<td width="40" align="left" valign="top">&nbsp;</td>
				  </tr>

				  <tr>
					<td width="30" align="left" valign="top"><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
					<td align="left" valign="top" class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:18px; color:#000000;">&nbsp;</td>
					<td align="left" valign="top"><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
				  </tr>
				</tbody>
      		</table>
		</td>
     </tr>
	
	<tr>
      <td align="left" valign="top">
		  <img src="{{$message->embed($section1_top)}}" width="700" height="115" class="fallback-font" style="display: inline-block; white-space: pre; font-family:HPSimplifiedLight, arial; background-color: #bee8f6; color:#ffffff; outline: none; border: 0px; font-size:18px; line-height:20px;" alt="
		  ">
	  </td>
    </tr>
	
	<tr>
      <td align="left" valign="top" bgcolor="#FFFFFF">
			<table width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="#bee8f6">
				<tbody>
				  <tr>
					<td width="40" align="left" valign="top"><img src="{{$message->embed($space)}}" width="40" height="30" style="display: block;" alt=""></td>
					<td align="left" valign="top" class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:30px; color:#000000;">
						<table width="620" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
								  <td class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:26px; color:#000000; text-align: center; text-transform: uppercase; line-height: 32px;">Start the Week 1 quiz now!</td>
								</tr>
								<tr>
								  <td><img src="{{$message->embed($space)}}" width="30" height="20" style="display: block;" alt=""></td>
								</tr>
								<tr>
								  <td class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:18px; color:#000000; text-align: center;">
								  	<table width="620" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td width="192" align="left" valign="top">&nbsp;</td>
												<td width="235" align="left" valign="top">
													<img src="{{$message->embed($start_now_button)}}" width="235" height="50" style="display: block;" alt="start now button">
												</td>
												<td width="193" align="left" valign="top">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								  </td>
								</tr>
							</tbody>
						</table>
					</td>
					<td width="40" align="left" valign="top">&nbsp;</td>
				  </tr>
				</tbody>
      		</table>
		</td>
     </tr>
	  
	<tr>
      <td align="left" valign="top">
		  <img src="{{$message->embed($section1_bottom)}}" width="700" height="80" class="fallback-font" style="display: inline-block; white-space: pre; font-family:HPSimplifiedLight, arial; background-color: #bee8f6; color:#ffffff; outline: none; border: 0px; font-size:18px; line-height:20px;" alt="
		  ">
	  </td>
    </tr>
	
	<tr>
      	<td align="left" valign="top" bgcolor="#FFFFFF">
			<table width="700" border="0" cellspacing="0" cellpadding="0">
				<tbody>
				  <tr>
					<td width="30"><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
					<td>&nbsp;</td>
					<td width="30">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="30">&nbsp;</td>
					<td align="left" valign="top" class="fallback-font" style="font-family:HPSimplifiedLight, arial; font-size:11px; color:#000000;">© Copyright 2019 HP Development Company, L.P.</td>
					<td width="30">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="30">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="30"><img src="{{$message->embed($space)}}" width="30" height="30" style="display: block;" alt=""></td>
				  </tr>
				</tbody>
			</table>
		</td>
    </tr>
  </tbody>
</table>
</body>
</html>
