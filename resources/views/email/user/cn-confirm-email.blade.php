<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Please confirm your email address</title>
<style type="text/css">
body {
    background-color: #f0f0f0;
    margin-left: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
}
</style>
</head>

<body>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top" bgcolor="#ffffff"><img src="{{$message->embed($kv_link)}}" width="700" height="400" style="display: block;" alt=""/></td>
    </tr>
    <tr>
      <td align="left" valign="top" bgcolor="#ffffff"><table width="700" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="40"><span style="font-size: 0px; line-height: 20px;"><img src="{{$message->embed($space)}}" width="40" height="20" style="display: block;" alt=""/></span></td>
            <td>&nbsp;</td>
            <td width="40">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td style="font-family: arial; font-size: 14px; line-height: normal; color: #000000;">Dear {{$user_name}},<br>
              <br>
				@php
					$firstCharacter = substr($link, 0, 1);
					if($firstCharacter == '=') {
						$str = ltrim($link, '='); 
					} else {
						$str = $link; 
					}
				@endphp
              请点击<a href="{{$str}}" style="color: #1e96d4 !important;">此</a>处激活您的帐户，
              或将以下网址粘贴到您的浏览器中： <br>
              <a style="color: #1e96d4 !important;">{{$str}}</a><br>
              <br>
              此致！<br>
              惠普活力星球竞赛管理员<br>              </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><span style="font-size: 0px; line-height: 20px;"><img src="{{$message->embed($space)}}" width="40" height="20" style="display: block;" alt=""/></span></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td align="left" valign="top"><img src="{{$message->embed($line_break_link)}}" width="700" height="60" style="display: block;" alt=""/></td>
    </tr>
    <tr>
      <td align="left" valign="top" bgcolor="#cae7f5"><table width="700" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="40" style="font-size: 0px; line-height: 20px;"><img src="{{$message->embed($space)}}" width="40" height="20" style="display: block;" alt=""/></td>
            <td  style="font-size: 0px; line-height: 20px;">&nbsp;</td>
            <td width="40"  style="font-size: 0px; line-height: 20px;">&nbsp;</td>
          </tr>
          <tr>
            <td width="40">&nbsp;</td>
            <td style=" font-family: arial; font-size: 11px; line-height: normal; color: #000000;">© Copyright 2019 HP Development Company, L.P.<br>
              The information contained herein is subject to change without notice. The only warranties for HP products and services are set forth in the express warranty statements accompanying such products and services. Nothing herein should be construed as constituting an additional warranty. HP shall not be liable for technical or editorial errors or omissions contained herein.</td>
            <td width="40">&nbsp;</td>
          </tr>
          <tr>
            <td width="40" style="font-size: 0px; line-height: 20px;">&nbsp;</td>
            <td style="font-size: 0px; line-height: 20px;">&nbsp;</td>
            <td width="40" style="font-size: 0px; line-height: 40px;"><img src="{{$message->embed($space)}}" width="40" height="40" style="display: block;" alt=""/></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>
