@extends('layouts.app')

@section('title', 'Register')

@section('content')

@include('layouts.header')

<?php
	foreach ($partner_list as $p_list) {
		$arr[] = $p_list->partner_id;
	}
?>
<style type="text/css">
	[type=radio] { 
		position: absolute;
		/*margin: 3px 3px 0px 5px;*/
		margin: 100px 3px 0px 67px;
		/*opacity: 0;
		width: 0;
		height: 0;*/
	}

	/* IMAGE STYLES */
	[type=radio] + img {
		cursor: pointer;
	}

	/* CHECKED STYLES */
	[type=radio]:checked + img {
		/*outline: 2px solid #f00;*/
	}
</style>
<div class="spacer-30"></div>
<div class="">
	<div class="row" style="margin-right: 0px;margin-left: 0px;">
		<div class="col-md-2 col-sm-2"></div>
		<div class="col-md-8 col-sm-8 d-none d-sm-block">
			<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
			<div class="bottom-left">
				<a href="{{url('/')}}" style="text-decoration: none;">
					<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
				</a>
			</div>
			<div class="bottom-left2">
				<span class="bread_tu_qui_txt login_bread_tu_qui_txt"><a href="{{url('/login')}}" style="text-decoration: none;color: #000;font-size: 15px;">{{__('messages.login')}}</a> / <a style="text-decoration: none;color: #000;font-size: 15px;">{{ __('messages.register') }}</a></span>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 d-block d-sm-none">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #1ab6f1;"></i>
			</a>
			>
			<span><a href="{{url('/login')}}">{{__('messages.login')}}</a> / {{ __('messages.register') }}</span>
		</div>
		<div class="col-md-2 col-sm-2"></div>
	</div>
	<div class="spacer-20"></div>

	<div class="register_section d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<!-- <h3>Register</h3> -->
				<h3>{{ __('messages.register_title') }}</h3>
				@include('layouts.messages')
				<div class="spacer-5"></div>
				<div class="row">
					<div class="col-sm-6 registerform">
						<div class="row registerborder">
							<form action="{{url('/register')}}" style="font-family: Gotham Light;" class="needs-validation" id="register_form" method="post" enctype="multipart/form-data" novalidate>
							{{ csrf_field() }}
								<div class="form-group">
									<div class="row" style="width: 100%;margin-left: 0px;">
										<div class="col-sm-6 hp_first">
											<input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{old('first_name')}}" placeholder="{{ __('messages.first_name') }}" required>
											<div class="invalid-feedback">
												{{ __('messages.enter_fname') }}.
											</div>
										</div>
										<div class="col-sm-6 hp_last">
											<input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{old('last_name')}}" placeholder="{{ __('messages.last_name') }}" required>

											<div class="invalid-feedback">
												{{ __('messages.enter_lname') }}.
											</div>
										</div>
									</div>														
								</div>

								<div class="form-group">
									<select class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" required style="color: #999!important;">
										<option value="" {{ old('country') == '0' ? 'selected':'' }}>{{__('messages.country')}}</option>
										<option value="China (Greater China)" {{ old('country') == '1' ? 'selected':'' }}>{{__('messages.c_greate_c')}}</option>
										<option value="Hong Kong (Greater China)" {{ old('country') == '2' ? 'selected':'' }}>{{__('messages.hk_greate_c')}}</option>
										<option value="Taiwan (Greater China)" {{ old('country') == '3' ? 'selected':'' }}>{{__('messages.taiwan')}}</option>
										<option value="India" {{ old('country') == '4' ? 'selected':'' }}>{{__('messages.india')}}</option> 
										<option value="Indonesia (SEA-K)" {{ old('country') == '5' ? 'selected':'' }}>{{__('messages.indo')}}</option> 
										<option value="Malaysia (SEA-K)" {{ old('country') == '6' ? 'selected':'' }}>{{__('messages.malaysia')}}</option> 
										<option value="Philippines (SEA-K)" {{ old('country') == '7' ? 'selected':'' }}>{{__('messages.philippines')}}</option> 
										<option value="Singapore (SEA-K)" {{ old('country') == '8' ? 'selected':'' }}>{{__('messages.singapore')}}</option> 
										<option value="Thailand (SEA-K)" {{ old('country') == '9' ? 'selected':'' }}>{{__('messages.thai')}}</option> 
										<option value="Vietnam (SEA-K)" {{ old('country') == '10' ? 'selected':'' }}>{{__('messages.vietnam')}}</option>
										<option value="Korea (SEA-K)" {{ old('country') == '11' ? 'selected':'' }}>{{__('messages.korea')}}</option> 
										<option value="Australia (SPAC)" {{ old('country') == '12' ? 'selected':'' }}>{{__('messages.australia')}}</option> 
										<option value="New Zealand (SPAC)" {{ old('country') == '13' ? 'selected':'' }}>{{__('messages.new_zea')}}</option>
									</select>
									<div class="invalid-feedback">
										{{ __('messages.select_country') }}.
									</div>
								</div>

								<div class="form-group">
									<select class="form-control {{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" id="designation" required style="color: #999!important;">
										<option value="" {{ old('designation') == '0' ? 'selected':'' }}>{{__('messages.designation')}}</option>
										<option value="Administration" {{ old('designation') == '1' ? 'selected':'' }}>{{__('messages.administration')}}</option>
										<option value="Customer Service" {{ old('designation') == '2' ? 'selected':'' }}>{{__('messages.customer_service')}}</option>
										<option value="Engineering" {{ old('designation') == '3' ? 'selected':'' }}>{{__('messages.engineering')}}</option>
										<option value="Finance" {{ old('designation') == '4' ? 'selected':'' }}>{{__('messages.finance')}}</option> 
										<option value="IT/Technology" {{ old('designation') == '5' ? 'selected':'' }}>{{__('messages.it')}}</option> 
										<option value="Logistics" {{ old('designation') == '6' ? 'selected':'' }}>{{__('messages.logistics')}}</option> 
										<option value="Marketing" {{ old('designation') == '7' ? 'selected':'' }}>{{__('messages.marketing')}}</option> 
										<option value="Retail Service" {{ old('designation') == '8' ? 'selected':'' }}>{{__('messages.retail')}}</option> 
										<option value="Sales" {{ old('designation') == '9' ? 'selected':'' }}>{{__('messages.sales')}}</option> 
										<option value="Technical" {{ old('designation') == '10' ? 'selected':'' }}>{{__('messages.technical')}}</option>
										<option value="Others" {{ old('designation') == '11' ? 'selected':'' }}>{{__('messages.other_design')}}</option>
									</select>
									<div class="invalid-feedback">
										{{ __('messages.select_designation') }}.
									</div>
								</div>

								<div class="form-group d-none" id="other_designation">
									<input type="text" class="form-control {{ $errors->has('other_designation') ? ' is-invalid' : '' }}" name="other_designation" id="other_designation_txt" value="{{old('other_designation')}}" placeholder="{{__('messages.other_design')}}">

									<div class="invalid-feedback">
										{{ __('messages.select_designation') }}.
									</div>
								</div>

								<div class="form-group partnerid">
									<input type="text" class="form-control {{ $errors->has('partner_id') ? ' is-invalid' : '' }}" name="partner_id" id="partner_id" value="{{old('partner_id')}}" placeholder="{{ __('messages.partnerid') }}" required>
									<div class="invalid-feedback">
										{{ __('messages.enter_partnerid') }}.
									</div>
								</div>

								<div class="form-group">
									<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" placeholder="{{ __('messages.email') }}" required>
									<div class="invalid-feedback">
										<!-- Please enter a valid Email -->
										{{ __('messages.enter_cemail') }}.
									</div>
								</div>

								<div class="form-group">
									<select class="form-control {{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" required style="color: #999!important;">
										<option value="" {{ old('gender') == '0' ? 'selected':'' }}>{{__('messages.gender')}}</option>
										<option value="1" {{ old('gender') == '1' ? 'selected':'' }}>{{__('messages.male')}}</option>
										<option value="2" {{ old('gender') == '2' ? 'selected':'' }}>{{__('messages.female')}}</option>
									</select>
									<div class="invalid-feedback">
										{{ __('messages.select_gender') }}.
									</div>
								</div>		

								<div class="form-group">
									<input type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name')}}" placeholder="{{ __('messages.company_name') }}" required>

									<div class="invalid-feedback">
										{{ __('messages.enter_companyname') }}.
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="width: 100%;margin-left: 0px;">
										<div class="col-sm-6 hp_password">
											<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="{{ __('messages.password') }}" required>
											<div class="invalid-feedback">
												{{ __('messages.enter_password') }}.
											</div>
										</div>
										<div class="col-sm-6 hp_cpassword">
											<input type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" id="confirm_password" placeholder="{{__('messages.confirm_password')}}" required>
											<div class="invalid-feedback">
												{{ __('messages.enter_cpassword') }}.
											</div>
										</div>
									</div>
								</div>
			
								<div class="spacer-10"></div>

								<div class="hp_reg_checkbox">
								  
								<label class="customcheck"> 
								  	{{__('messages.iagreetothe')}} <a href="{{url('/term_of_use')}}" style="color: #000"><u>{{__('messages.term_condi')}}</u></a>.
								  	<input type="checkbox" class="" required>
								  	<span class="checkmark"></span>
								  </label>
								</div>

								@if (App::isLocale('cn'))
								<img src="{{url('/img/register_button_chinese.png')}}" id="register_btn" style="cursor: pointer;">
								@else
								<img src="{{url('/img/register-button.png')}}" id="register_btn" style="cursor: pointer;">
								@endif
								
								<!-- <button type="button" id="submit_btn" class="btn btn-primary">Submit</button>
								<button type="submit" id="submit" class="d-none">sub</button> -->
							</form>
						</div>
					</div>
					<div class="col-sm-6 register">
						<div class="row register_img_border">
							<div class="register_over_img1">{{__('messages.already')}}</div><br>
							<div class="register_over_img2">
								<a href="{{url('/login')}}">{{__('messages.click_to_login')}}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
		<div class="row hpcopyright"  style="padding: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<div class="row d-none d-sm-block">
					<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="row">
							<div class="col-sm-8 col-md-8 footer_div">
								<span>{{__('messages.copy_right')}}</span>
							</div>
							<div class="col-sm-4 col-md-4 footer_div text-right">
								<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
								</a>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{__('messages.copy_right')}}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
	</div>
	<div class="register_mobilesection d-block d-sm-none">
		<div class="container">
			<h3>{{ __('messages.register_title') }}</h3>
			@include('layouts.messages')
			<div class="spacer-5"></div>
			<div class="col-md-12 registerform">
				<div class="row registerborder justify-content-center align-items-center h-100">
					<form action="{{url('/register')}}" class="needs-validation" id="register_form_xs" method="post" enctype="multipart/form-data" novalidate>
					{{ csrf_field() }}
						<div class="form-group">
							<input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{old('first_name')}}" placeholder="{{ __('messages.first_name') }}" required>

							<div class="invalid-feedback">
								{{ __('messages.fname') }}.
							</div>
						</div>

						<div class="form-group">
							<input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{old('last_name')}}" placeholder="{{ __('messages.last_name') }}" required>

							<div class="invalid-feedback">
								{{ __('messages.lname') }}.
							</div>			
						</div>

						<div class="form-group">
							<select class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" required>
								<option value="" {{ old('country') == '0' ? 'selected':'' }}>{{__('messages.country')}}</option>
								<option value="China (Greater China)" {{ old('country') == '1' ? 'selected':'' }}>{{__('messages.c_greate_c')}}</option>
								<option value="Hong Kong (Greater China)" {{ old('country') == '2' ? 'selected':'' }}>{{__('messages.hk_greate_c')}}</option>
								<option value="Taiwan (Greater China)" {{ old('country') == '3' ? 'selected':'' }}>{{__('messages.taiwan')}}</option>
								<option value="India" {{ old('country') == '4' ? 'selected':'' }}>{{__('messages.india')}}</option> 
								<option value="Indonesia (SEA-K)" {{ old('country') == '5' ? 'selected':'' }}>{{__('messages.indo')}}</option> 
								<option value="Malaysia (SEA-K)" {{ old('country') == '6' ? 'selected':'' }}>{{__('messages.malaysia')}}</option> 
								<option value="Philippines (SEA-K)" {{ old('country') == '7' ? 'selected':'' }}>{{__('messages.philippines')}}</option> 
								<option value="Singapore (SEA-K)" {{ old('country') == '8' ? 'selected':'' }}>{{__('messages.singapore')}}</option> 
								<option value="Thailand (SEA-K)" {{ old('country') == '9' ? 'selected':'' }}>{{__('messages.thai')}}</option> 
								<option value="Vietnam (SEA-K)" {{ old('country') == '10' ? 'selected':'' }}>{{__('messages.vietnam')}}</option>
								<option value="Korea (SEA-K)" {{ old('country') == '11' ? 'selected':'' }}>{{__('messages.korea')}}</option> 
								<option value="Australia (SPAC)" {{ old('country') == '12' ? 'selected':'' }}>{{__('messages.australia')}}</option> 
								<option value="New Zealand (SPAC)" {{ old('country') == '13' ? 'selected':'' }}>{{__('messages.new_zea')}}</option>
							</select>
							<div class="invalid-feedback">
								{{ __('messages.select_country') }}.
							</div>
						</div>

						<div class="form-group">
							<select class="form-control {{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" id="designation" required style="color: #999!important;">
								<option value="" {{ old('designation') == '0' ? 'selected':'' }}>{{__('messages.designation')}}</option>
								<option value="Administration" {{ old('designation') == '1' ? 'selected':'' }}>{{__('messages.administration')}}</option>
								<option value="Customer Service" {{ old('designation') == '2' ? 'selected':'' }}>{{__('messages.customer_service')}}</option>
								<option value="Engineering" {{ old('designation') == '3' ? 'selected':'' }}>{{__('messages.engineering')}}</option>
								<option value="Finance" {{ old('designation') == '4' ? 'selected':'' }}>{{__('messages.finance')}}</option> 
								<option value="IT/Technology" {{ old('designation') == '5' ? 'selected':'' }}>{{__('messages.it')}}</option> 
								<option value="Logistics" {{ old('designation') == '6' ? 'selected':'' }}>{{__('messages.logistics')}}</option> 
								<option value="Marketing" {{ old('designation') == '7' ? 'selected':'' }}>{{__('messages.marketing')}}</option> 
								<option value="Retail Service" {{ old('designation') == '8' ? 'selected':'' }}>{{__('messages.retail')}}</option> 
								<option value="Sales" {{ old('designation') == '9' ? 'selected':'' }}>{{__('messages.sales')}}</option> 
								<option value="Technical" {{ old('designation') == '10' ? 'selected':'' }}>{{__('messages.technical')}}</option>
								<option value="Others" {{ old('designation') == '11' ? 'selected':'' }}>{{__('messages.other_design')}}</option>
							</select>
							<div class="invalid-feedback">
								{{ __('messages.select_designation') }}.
							</div>
						</div>

						<div class="form-group d-none" id="other_designation">
							<input type="text" class="form-control {{ $errors->has('other_designation') ? ' is-invalid' : '' }}" name="other_designation" id="other_designation_txt" value="{{old('other_designation')}}" placeholder="{{__('messages.other_design')}}">

							<div class="invalid-feedback">
								{{ __('messages.select_designation') }}.
							</div>
						</div>

						<div class="form-group">
							<input type="text" class="form-control {{ $errors->has('partner_id') ? ' is-invalid' : '' }}" name="partner_id" id="partner_id" value="{{old('partner_id')}}" placeholder="{{ __('messages.partnerid') }}" required>
							<div class="invalid-feedback">
								{{ __('messages.enter_partnerid') }}.
							</div>
						</div>

						<div class="form-group">
							<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" placeholder="{{ __('messages.email') }}" required>
							<div class="invalid-feedback">
								{{ __('messages.enter_cemail') }}.
							</div>
						</div>

						<div class="form-group">
							<select class="form-control {{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" required>
								<option value="" {{ old('gender') == '0' ? 'selected':'' }}>{{__('messages.gender')}}</option>
								<option value="1" {{ old('gender') == '1' ? 'selected':'' }}>{{__('messages.male')}}</option>
								<option value="2" {{ old('gender') == '2' ? 'selected':'' }}>{{__('messages.female')}}</option>
							</select>
							<div class="invalid-feedback">
								{{ __('messages.select_gender') }}.
							</div>
						</div>

						<div class="form-group">
							<input type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name')}}" placeholder="{{ __('messages.company_name') }}" required>

							<div class="invalid-feedback">
								{{ __('messages.enter_companyname') }}.
							</div>
						</div>

						<div class="form-group">
							<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="{{ __('messages.password') }}" required>
							<div class="invalid-feedback">
								{{ __('messages.enter_password') }}.
							</div>
						</div>
						<div class="form-group">
							<input type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" id="confirm_password" placeholder="{{__('messages.confirm_password')}}" required>
								<div class="invalid-feedback">
									{{ __('messages.enter_cpassword') }}.
								</div>
						</div>

						<div class="spacer-10"></div>

						<div class="hp_reg_checkbox">
						  
						  <label class="customcheck"> 
						  	{{__('messages.iagreetothe')}} <u>{{__('messages.term_condi')}}</u>.
						  	<input type="checkbox" class="" required>
						  	<span class="checkmark"></span>
						  </label>
						</div>

						
						@if (App::isLocale('cn'))
							<img src="{{url('/img/register_button_chinese.png')}}" id="register_btn_xs" style="cursor: pointer;"> <br>
						@else
							<img src="{{url('/img/register-button.png')}}" id="register_btn_xs" style="cursor: pointer;"> <br>
						@endif

						<div class="spacer-20"></div>

						<span style="font-weight: 700;">{{__('messages.already')}}</span>
						<span><a href="{{url('/login')}}" style="text-decoration: none !important;">{{__('messages.click_to_login')}}</a></span>
					</form>
				</div>
			</div>
				
			<div class="row hpcopyright">
				<div class="col-md-12">
					<div class="row d-none d-sm-block">
						<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
							<div class="row">
								<div class="col-sm-8 col-md-8 footer_div">
									<span>{{__('messages.copy_right')}}</span>
								</div>
								<div class="col-sm-4 col-md-4 footer_div text-right">
									<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row d-block d-sm-none" style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="col-sm-8 col-md-8 footer_div">
							<span>{{__('messages.copy_right')}}</span>
						</div>
						<div class="col-sm-4 col-md-4 footer_div">
							<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="spacer-100"></div>
</div>


<script type="text/javascript">
	var p_list_arr = <?php echo json_encode($arr); ?>;

	$("#submit_btn").click(function() {
		var partner_id = $("#partner_id").val();

		var ans = p_list_arr.includes(partner_id);

		if (partner_id != '' && ans == false) {
			alert("The partner id you entered isn't available.Please contact for more information.");
		} else {
			$("#submit").click();
		}
	});

	function include(arr,obj) {
		return (arr.indexOf(obj) != -1);
	}

	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();

	$("#designation").change(function() {
		if ($(this).val() == 'Others') {
			$("#other_designation").removeClass('d-none');
			$("#other_designation_txt").attr('required','true');
		} else {
			$("#other_designation").addClass('d-none');
			$("#other_designation_txt").removeAttr('required');
		}
	});

	$("#register_btn").click(function() {
		$("#register_form").submit();
	});

	$("#register_btn_xs").click(function() {
		$("#register_form_xs").submit();
	});
</script>
@endsection