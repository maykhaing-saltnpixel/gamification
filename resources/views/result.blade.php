@extends('layouts.app')

@section('title', 'Result')

@section('content')

@include('layouts.header')

<?php
	$correct_count = 0;
	$bonus_time = 0;
	$bonus = 0;
	foreach ($user_answered as $u_ans) {
		if ($u_ans->correct_flg == 0 || $u_ans->correct_flg == 9) {
			$correct_count = $correct_count + 1;
		}
	}

	$total_point = 100 * $correct_count;

	foreach ($user_answered as $u_ans) {
		$bonus += $u_ans->bonus_time;
		$bonus_time = $bonus_time + $u_ans->bonus_time;
	}

	$final_total_point = $total_point+$bonus;

	$date = Date('Y/m/d');
	$dates = '2019/08/30';

?>

<link rel="stylesheet" href="{{url('/css/progress.css?v=' . time())}}">

<div class="spacer-30"></div>
<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<span class="bread_tu_qui_txt">Tutorials & Quizzes</span>
			<!-- <span class="breadcurmb_gmae_title">WEEK 1 - {{$game->game_title}}</span> -->
		</div>
		<div class="bottom-left3">
			<span class="bread_tu_qui_txt">WEEK 1 - 
				@if (App::isLocale('cn'))
					{{$game->cn_game_title}}
				@else
					{{$game->game_title}}
				@endif
			</span>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span>Tutorials & Quizzes</span>
		>
		<span>WEEK 1 - {{$game->game_title}}</span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>
<div class="spacer-20"></div>

<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-sm-3 col-xs-12"></div>
	<div class="col-sm-6 col-xs-12" style="background-color: #c0d2dc !important;">
		<div class="row result_banner">
			<div class="col-sm-1"></div>
			<div class="col-sm-4" style="padding: 0px;">
				<p class="answer_points">{{$total_point}}<sub style="">POINTS</sub></p>
				<p class="answer_correct_count">{{$correct_count}} CORRECT ANSWER</p>
			</div>
			<div class="col-sm-2 text-center">
				<p class="final_points">{{$final_total_point}}</p>
			</div>
			<div class="col-sm-3 text-center">
				<p class="timer_points">+{{$bonus_time}}<sub style="">POINTS</sub></p>
				<p class="timer_bonus">TIME BONUS</p>
			</div>
		</div>
		<div class="row" style="background-color: #e8f2f7;padding: 10px;">
			<div class="col-sm-12" style="border: 1px solid #fff;padding: 15px;width: 100%;">
				<div class="spacer-20"></div>
				@if(auth()->user()->first_registrant_flg == 0)
					<div class="col-sm-12 text-center">
						<p class="result_congratulation">CONGRATULATIONS</p>
						<p class="result_achievement">You just unlocked <b>{{count($user_answered_list)+1}}</b> achievements.</p>
					</div>
					<div class="spacer-20"></div>

					<div class="row d-block d-sm-none" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="{{url('/img/first_five.png')}}" class="achievement_images" style="margin: 0 auto;">
							<p class="achievement_text">First 500 Registrant</p>
						</div>
						@php $count = 1; @endphp
						@foreach($game_list as $g_list)
							@php
								$publish_date = date('Y/m/d', strtotime($g_list->from_date));
								$end_date = date('Y/m/d', strtotime($g_list->to_date));
							@endphp
							@foreach($user_answered_list as $u_a_list)
								@if($g_list->id == $u_a_list->game_id)
									@if($date > $end_date && $end_date == '2019/08/29')
										<div class="col-sm-6 col-xs-6 text-center">
											<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed {{$count}}</p>
										</div>
									@endif
									@php $count = $count + 1; @endphp
								@endif
							@endforeach
						@endforeach
						<div class="col-sm-6 col-xs-6 text-center">
							@if($date == '2019/08/29')
								<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 1</p>
							@elseif($date == '2019/08/30')
								<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 2</p>
							@elseif($date == '2019/08/31')
								<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 3</p>
							@elseif($date >= '2019/09/01')
								<img src="{{url('/img/achievement4.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 4</p>
							@endif
						</div>
					</div>

					<div class="col-12 d-none d-sm-block">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-sm-6 col-xs-6 text-right">
							<img src="{{url('/img/first_five.png')}}" class="achievement_images" style="margin: 0 auto;">
							<p class="achievement_text">First 500 Registrant</p>
						</div>
						@php $g_count = 1; @endphp
						@foreach($game_list as $g_list)
							@php
								$publish_date = date('Y/m/d', strtotime($g_list->from_date));
								$end_date = date('Y/m/d', strtotime($g_list->to_date));
							@endphp

							@foreach($user_answered_list as $u_a_list)
								@if($g_list->id == $u_a_list->game_id)
									@if($date >= $end_date && $end_date == '2019/08/29')
										@if(($g_count)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 1</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/08/30')
										@if(($g_count)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 2</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/08/31')
										@if(($g_count)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 3</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/09/01')
										@if(($g_count)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 4</p>
										</div>
									@endif
									@php $g_count = $g_count + 1; @endphp
								@endif
							@endforeach
						@endforeach
						@if(($g_count-1)%2 == 0)
							<div class="col-sm-6 col-xs-6 text-right">
						@else
							<div class="col-sm-6 col-xs-6 text-left">
						@endif
							<!-- @if($date >= '2019/08/28' && $date <= '2019/08/28')
								<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 1</p>
							@elseif($date >= '2019/08/29' && $date <= '2019/08/29')
								<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 2</p>
							@elseif($date >= '2019/08/30' && $date <= '2019/08/30')
								<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 3</p>
							@elseif($date >= '2019/08/31')
								<img src="{{url('/img/achievement4.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 4</p>
							@endif -->
						</div>
					</div>
				@elseif(auth()->user()->first_registrant_flg == 1)
					<div class="col-sm-12 text-center">
						<p class="result_congratulation">CONGRATULATIONS</p>
						@if(count($user_answered_list) == 1)
							<p class="result_achievement">You just unlocked <b>1</b> achievement.</p>
						@else
							<p class="result_achievement">You just unlocked <b>{{count($user_answered_list)}}</b> achievements.</p>
						@endif
					</div>
					<div class="spacer-20"></div>

					<div class="row d-block d-sm-none" style="width: 100%;margin-left: 0px;">
						@php $count = 1; @endphp
						@foreach($game_list as $g_list)
							@php
								$publish_date = date('Y/m/d', strtotime($g_list->from_date));
								$end_date = date('Y/m/d', strtotime($g_list->to_date));
							@endphp
							@foreach($user_answered_list as $u_a_list)
								@if($g_list->id == $u_a_list->game_id)
									@if($date > $end_date && $end_date == '2019/08/29')
										<div class="col-sm-6 col-xs-6 text-center">
											<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week {{$count}}</p>
										</div>
									@endif
									@php $count = $count + 1; @endphp
								@endif
							@endforeach
						@endforeach
						<div class="col-sm-6 col-xs-6 text-center">
							@if($date >= $end_date && $end_date == '2019/08/29')
								<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 1</p>
							@elseif($date >= $end_date && $end_date == '2019/08/30')
								<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 2</p>
							@elseif($date >= $end_date && $end_date == '2019/08/31')
								<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 3</p>
							@elseif($date >= $end_date && $end_date == '2019/09/01')
								<img src="{{url('/img/achievement4.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 4</p>
							@endif
						</div>
					</div>

					<div class="col-12 d-none d-sm-block">
						<div class="row" style="width: 100%;margin-left: 0px;">
						@php $g_count = 1; @endphp
						@foreach($game_list as $g_list)
							@php
								$publish_date = date('Y/m/d', strtotime($g_list->from_date));
								$end_date = date('Y/m/d', strtotime($g_list->to_date));
							@endphp
							@foreach($user_answered_list as $u_a_list)
								@if($g_list->id == $u_a_list->game_id)
									@if($date >= $end_date && $end_date == '2019/08/29')
										@if(($g_count-1)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 1</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/08/30')
										@if(($g_count-1)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 2</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/08/31')
										@if(($g_count-1)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 3</p>
										</div>
									@elseif($date >= $end_date && $end_date == '2019/09/01')
										@if(($g_count-1)%2 == 0)
											<div class="col-sm-6 col-xs-6 text-right">
										@else
											<div class="col-sm-6 col-xs-6 text-left">
										@endif
											<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
											<p class="achievement_text">Completed Week 4</p>
										</div>
									@endif
									@php $g_count = $g_count + 1; @endphp
								@endif
							@endforeach
						@endforeach
						@if(($g_count)%2 == 0)
							<div class="col-sm-6 col-xs-6 text-right">
						@else
							<div class="col-sm-6 col-xs-6 text-left">
						@endif
							@if($date >= $end_date && $end_date == '2019/08/29')
								<img src="{{url('/img/achievement1.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 1</p>
							@elseif($date >= $end_date && $end_date == '2019/08/30')
								<img src="{{url('/img/achievement2.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 2</p>
							@elseif($date >= $end_date && $end_date == '2019/08/31')
								<img src="{{url('/img/achievement3.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 3</p>
							@elseif($date >= $end_date && $end_date == '2019/09/01')
								<img src="{{url('/img/achievement4.png')}}" class="achievement_images" style="margin: 0 auto;">
								<p class="achievement_text">Completed Week 4</p>
							@endif
						</div>
					</div>
				@endif
				<div class="spacer-20"></div>
				<div class="row text-center">
					<a href="{{url('/knowledge_meter')}}">
						<img class="ranking_btn" src="{{url('/img/view-ranking_button.png')}}" style="margin: 0 auto;">
					</a>
				</div>
				<div class="spacer-100"></div>
			</div>
		</div>
	</div>
	<div class="row d-none d-sm-block">
		<div class="col-12 " style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
			<div class="row">
				<div class="col-sm-8 col-md-8 footer_div">
					<span>{{ __('messages.copy_right') }}</span>
				</div>
				<div class="col-sm-4 col-md-4 footer_div text-right">
					<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
					</a>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="row d-block d-sm-none" style="border-top: 1px solid #fff;height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
		<div class="col-sm-8 col-md-8 footer_div">
			<span>{{ __('messages.copy_right') }}</span>
		</div>
		<div class="col-sm-4 col-md-4 footer_div">
			<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{ __('messages.term_condi') }}</span>
			</a>
		</div>
	</div>
</div>

<div class="spacer-100"></div>


<script type="text/javascript">
	$(document).ready(function(){
	     $('body,html').animate({scrollTop: 550}, 800); 
	});
</script>
@endsection