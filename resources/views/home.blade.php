@extends('layouts.app')

@section('title', 'Home')

@section('content')

@include('layouts.header')
@php
	$date = date('Y/m/d');
	$dates = '2019/05/09';
@endphp
	<div class="spacer-30"></div>
	<!-- quiz-schedule -->
	<div class="spacer-30"></div>
	<div class="quiz-schedule d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6"  style="padding: 0px;">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
						<h6>{{ __('messages.4weeks') }}</h6>
						
						<span>{{ __('messages.difference') }}</span>
						<div class="spacer-20"></div>
						<p>
							{{ __('messages.quiz_schedule_text1') }}
						</p>
						<p>
							{{ __('messages.quiz_schedule_text2') }}
						</p>
					</div>
					@if (App::isLocale('cn'))
					<div class="col-md-6 col-sm-6 col-xs-12" style="margin: 0 auto;text-align: center;">
						@if($date < '2019/08/29')
							<img src="{{url('/img/quiz-schedule_chinese.png')}}" class="img_fluid">
						@elseif($date == '2019/08/29')
							<img src="{{url( '/img/quiz-schedule_chinese.png')}}" class="img_fluid">
						@elseif($date == '2019/08/30')
							<img src="{{url('/img/Quiz_Schedule_Chinese_2.png')}}" class="img_fluid">
						@elseif($date == '2019/08/31')
							<img src="{{url('/img/Quiz_Schedule_Chinese_3.png')}}" class="img_fluid">
						@elseif($date >= '2019/09/01')
							<img src="{{url('/img/Quiz_Schedule_Chinese_4.png')}}" class="img_fluid">
						@endif
					</div>					
					@else
					<div class="col-md-6 col-sm-6 col-xs-12" style="margin: 0 auto;text-align: center;">
						@if($date < '2019/08/29')
							<img src="{{url('/img/quiz-schedule.png')}}" class="img_fluid">
						@elseif($date == '2019/08/29')
							<img src="{{url('/img/Quiz_Schedule_1.png')}}" class="img_fluid">
						@elseif($date == '2019/08/30')
							<img src="{{url('/img/Quiz_Schedule_2.png')}}" class="img_fluid">
						@elseif($date == '2019/08/31')
							<img src="{{url('/img/Quiz_Schedule_3.png')}}" class="img_fluid">
						@elseif($date >= '2019/09/01')
							<img src="{{url('/img/Quiz_Schedule_4.png')}}" class="img_fluid">
						@endif
					</div>
					@endif
				</div>
			</div>	
			<div class="col-3" style="padding: 0px;"></div>
		</div>
	</div>	
	<div class="quiz-schedule d-block d-sm-none">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-12">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-6 col-xs-12" style="padding: 0px;">
						<h6>{{ __('messages.4weeks') }}</h6>
						
						<span style="font-family: 'Gotham Light';font-size: 15px !important;line-height: 1em !important;">{{ __('messages.difference') }}</span>
						<div class="spacer-20"></div>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;">
							{{ __('messages.quiz_schedule_text1') }}
						</p>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;">
							{{ __('messages.quiz_schedule_text2') }}
						</p>
						<div class="spacer-20"></div>
					</div>
					@if (App::isLocale('cn'))
					<div class="col-sm-6 col-xs-12" style="padding: 0px;height: 200px;">
						@if($date < '2019/08/29')
							<img src="{{url('/img/quiz-schedule_chinese.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/29')
							<img src="{{url('/img/quiz-schedule_chinese.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/30')
							<img src="{{url('/img/Quiz_Schedule_Chinese_2.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/31')
							<img src="{{url('/img/Quiz_Schedule_Chinese_3.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date >= '2019/09/01')
							<img src="{{url('/img/Quiz_Schedule_Chinese_4.png')}}" class="img_fluid" style="width: 70%!important;">
						@endif
					</div>
					@else
					<div class="col-sm-6 col-xs-12" style="padding: 0px;height: 200px;">
						@if($date < '2019/08/29')
							<img src="{{url('/img/quiz-schedule.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/29')
							<img src="{{url('/img/Quiz_Schedule_1.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/30')
							<img src="{{url('/img/Quiz_Schedule_2.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date == '2019/08/31')
							<img src="{{url('/img/Quiz_Schedule_3.png')}}" class="img_fluid" style="width: 70%!important;">
						@elseif($date >= '2019/09/01')
							<img src="{{url('/img/Quiz_Schedule_4.png')}}" class="img_fluid" style="width: 70%!important;">
						@endif
					</div>
					@endif
				</div>
			</div>	
		</div>
	</div>

	<!-- quiz1 -->
	<!-- hidden xs -->
	@if (App::isLocale('cn'))
		@if($date < '2019/08/29')
			<!-- Week1 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/quiz1.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week1') }}:</h4>
								<span style="">{{ __('messages.ocean_clean_up') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week1_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/29')
			<!-- Week1 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/quiz1.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week1') }}:</h4>
								<span style="">{{ __('messages.ocean_clean_up') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week1_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/30')
			<!-- Week2 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-2.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week2') }}:</h4>
								<span style="">{{ __('messages.breathe_easy') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week2_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/31')
			<!-- Week3 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-3.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week3') }}:</h4>
								<span style="">{{ __('messages.science_inks') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week3_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date >= '2019/09/01')
			<!-- Week4 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-4.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week4') }}:</h4>
								<span style="">{{ __('messages.putting_counterfeit') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week4_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@endif
	@else
		@if($date < '2019/08/29')
			<!-- Week1 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/quiz1.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week1') }}:</h4>
								<span style="">{{ __('messages.ocean_clean_up') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week1_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/29')
			<!-- Week1 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/quiz1.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week1') }}:</h4>
								<span style="">{{ __('messages.ocean_clean_up') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week1_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/30')
			<!-- Week2 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-2.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week2') }}:</h4>
								<span style="">{{ __('messages.breathe_easy') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week2_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date == '2019/08/31')
			<!-- Week3 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-3.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week3') }}:</h4>
								<span style="">{{ __('messages.science_inks') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week3_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@elseif($date >= '2019/09/01')
			<!-- Week4 -->
			<div class="quiz1">
				<div class="row" style="width: 100%;margin-left: 0px;" id="week">
					<div class="col-3" style="padding: 0px;"></div>
					<div class="col-6"  style="padding: 0px;">
						<div class="row" style="width: 100%;margin-left: 0px;">
							<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<img class="quiz1img" src="{{url('/img/Game-HOME-KV-4.png')}}">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 ocean" style="padding: 0px;">
								<div class="spacer-100"></div>
								<div class="spacer-50"></div>
								<h4>{{ __('messages.week4') }}:</h4>
								<span style="">{{ __('messages.putting_counterfeit') }}</span>
								<div class="spacer-20"></div>
								<p>{{ __('messages.week4_text') }}</p>

								@if (App::isLocale('cn'))
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
									@endif
								@else
									@if($game_list)
										<a href="{{url('/game/start/'.$game_list->id)}}">
											<img src="{{url('/img/start-now_button.png')}}" class="start">
										</a>
									@else
										<img src="{{url('/img/commingsoon.png')}}" class="start">
									@endif
								@endif
							</div>
						</div>
					</div>	
					<div class="col-3" style="padding: 0px;"></div>
				</div>
			</div>
		@endif
	@endif

	<!-- visible xs -->
	@php
	$date = Date('Y/m/d');
	$dates = '2019/05/25';
	@endphp
	@if (App::isLocale('cn'))
	
		@if($date < '2019/08/29')
		<!-- Week1 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week1') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.ocean_clean_up') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week1_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/quiz1.png')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/29')
		<!-- Week1 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week1') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.ocean_clean_up') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week1_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/quiz1.png')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/30')
		<!-- Week2 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week2') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.breathe_easy') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week2_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-2.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/31')
		<!-- Week3 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week3') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.science_inks') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week3_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-3.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date >= '2019/09/01')
		<!-- Week4 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week4') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.putting_counterfeit') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week4_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-4.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@endif
	@else
		@if($date < '2019/08/29')
		<!-- Week1 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week1') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.ocean_clean_up') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week1_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/quiz1.png')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/29')
		<!-- Week1 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week1') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.ocean_clean_up') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week1_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/quiz1.png')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/30')
		<!-- Week2 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week2') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.breathe_easy') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week2_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-2.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date == '2019/08/31')
		<!-- Week3 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week3') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.science_inks') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week3_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-3.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@elseif($date >= '2019/09/01')
		<!-- Week4 -->
		<div class="quiz1">
			<div class="row" style="width: 100%;margin-left: 0px;" id="week1">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6"  style="padding: 15px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-6 col-xs-12" style="padding: 0px;">
							<div class="spacer-50"></div>
							<div class="spacer-20"></div>
							<h4 style="font-family: 'Gotham Book' !important;font-size: 30px !important;">{{ __('messages.week4') }}:</h4>
							<span style="font-family: 'Gotham XLight';font-size: 30px !important;">{{ __('messages.putting_counterfeit') }}</span>
							<div class="spacer-20"></div>
							<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.week4_text') }}</p>

							@if (App::isLocale('cn'))
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button_chinese.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/chinese_commingsoon.png')}}" class="start">
								@endif
							@else
								@if($game_list)
									<a href="{{url('/game/start/'.$game_list->id)}}">
										<img src="{{url('/img/start-now_button.png')}}" class="start">
									</a>
								@else
									<img src="{{url('/img/commingsoon.png')}}" class="start">
								@endif
							@endif
						</div>
						<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
							<img src="{{url('/img/Game-HOME-KV-4.jpg')}}" class="start" style="width: 70%;">
						</div>
					</div>
				</div>	
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
		@endif
	@endif

	<!-- HP Supplies -->
	<div class="hp-supplies d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;background:  #cae7f5;">
			<div class="col-sm-3" style="padding: 0px;"></div>
			<div class="col-sm-6" style="padding: 0px;">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-6" style="padding: 0px;">
						<h3>{{__('messages.hp_menu2')}}</h3>
						<p>{{ __('messages.hp_portfolio_text') }}</p>

						<div class="spacer-30"></div>

						@if (App::isLocale('cn'))
						<a href="{{url('/portfolio')}}">
							<img src="{{url('/img/learn-more_button_chinese.png')}}" class="learn-more_button">
						</a>
						@else
						<a href="{{url('/portfolio')}}">
							<img src="{{url('/img/learn-more_button.png')}}" class="learn-more_button">
						</a>
						@endif
					</div>
					<div class="col-sm-6" style="padding: 0px;">
						<img class="hp-supplies-portfolio" src="{{url('/img/hp-supplies-portfolio.jpg')}}">
					</div>
				</div>	
			</div>
			<div class="col-sm-3" style="padding: 0px;"></div>
		</div>
	</div>
	<div class="hp-supplies d-block d-sm-none">
		<div class="row" style="width: 100%;margin-left: 0px;background:  #cae7f5;">
			<div class="col-sm-3" style="padding: 0px;"></div>
			<div class="col-sm-6" style="padding: 15px;">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-6 col-xs-12" style="padding: 0px;">
						<h3>HP Supplies Portfolio</h3>
						<div class="spacer-20"></div>
						<p style="font-family: 'Gotham Light';font-size: 13px !important;">{{ __('messages.hp_portfolio_text') }}</p>

						<div class="spacer-10"></div>

						
						@if (App::isLocale('cn'))
						<img src="{{url('/img/learn-more_button_chinese.png')}}" style="width: 170px;">
						@else
						<img src="{{url('/img/learn-more_button.png')}}" style="width: 170px;">
						@endif
						<div class="spacer-20"></div>
					</div>
					<div class="col-sm-6 col-xs-12 text-center" style="padding: 0px;margin: 0 auto;">
						<img class="hp-supplies-portfolio" src="{{url('/img/hp-supplies-portfolio.jpg')}}">
					</div>
				</div>	
			</div>
			<div class="col-sm-3" style="padding: 0px;"></div>
		</div>
	</div>


	<!-- prizes -->
	<div class="prizes d-none d-sm-block">
		<div class="spacer-50"></div>
		<div class="spacer-50"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6" style="padding: 0px;">
				<div class="row">
					<div class="col-sm-8">
						<h3>{{ __('messages.prizes') }}</h3>
						<div class="spacer-10"></div>

						<p class="collect">{{ __('messages.prize_collect') }}</p>
						<span class="prize_list">{{ __('messages.prize_qualify') }}</span><br>
						<ol class="prize_list1">
							<li>{{ __('messages.prizes_qualify_list1') }}</li>
							<li>{{ __('messages.prizes_qualify_list2') }}</li>
							<li>{{ __('messages.prizes_qualify_list3') }}</li>
						</ol>
						
					</div>
					<div class="col-sm-4"></div>
				</div>
				
			</div>
			<div class="col-3" style="padding: 0px;"></div>
		</div>
		<div class="spacer-10"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-2" style="padding: 0px;">
			</div>
			@if (App::isLocale('cn'))
			<div class="col-md-8 tableprize_chinese" style="padding: 0px;">
			@else
			<div class="col-md-8 tableprize" style="padding: 0px;">
			@endif
				<div class="row">
					<div class="col-sm-12" style="padding-right: 0px;">
						<table class="table prizetable w-100 d-block">
							<tbody>
								<tr>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.homecountry') }}</th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.eVouchers') }}</th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.1stprize') }}<!-- 1<sup style="color: #956407;font-size: 9px;">st</sup> Prize --></th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.2ndprize') }}<!-- 2<sup style="color: #956407;font-size: 9px;">nd</sup> Prize --></th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.3rdprize') }}<!-- 3<sup style="color: #956407;font-size: 9px;">rd</sup> Prize --></th>
								</tr>
								<tr>
									<td>{{ __('messages.SPAC') }}<sup style="font-size: 9px;">1</sup></td>
									<td>Amazon.com</td>
									<td>
										<span>US$200</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>US$100</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$50</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="">
									<td>{{ __('messages.SEA-K') }}<sup style="font-size: 9px;">2</sup></td>
									<td>Amazon.com</td>
									<td>
										<span>US$200</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$100</span><br>
										<span class="winnerprizes">9 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$50</span><br>
										<span class="winnerprizes">30 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="">
									<td>{{ __('messages.greaterchina') }}<sup style="font-size: 9px;">3</sup></td>
									<td>JD.com</td>
									<td>
										<span>1400元</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>700元</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>350元</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="" style="border-bottom: 1px solid #fbce76;">
									<td>{{ __('messages.india') }}</td>
									<td>Amazon.in</td>
									<td>
										<span>₹14,000</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>₹7,000</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>₹3,500</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
							</tbody>
						</table>
						
						<p class="note1" style="font-family: 'Gotham Light';">
							<span style="font-family: 'Gotham Medium';">{{ __('messages.prizes_note1') }}</span> {{ __('messages.prizes_note2') }}
						</p>
						<!-- <p class="note2" style="">
							• Participants in SPAC, SEA-K and India will win Amazon eVouchers. • Participants in China will win JD.com eVouchers.
						</p> -->
					</div>
				</div>
			</div>
			<div class="col-md-2" style="padding: 0px;"></div>
		</div>
	</div> 
	<div class="prizes d-block d-sm-none">
		<div class="spacer-50"></div>
		<div class="spacer-20"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6" style="padding: 0px;">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-6" style="padding: 0px;">
						<h3 style="font-family: 'Gotham Book';font-size: 25px !important;">{{ __('messages.prizes') }}</h3>
						<div class="spacer-10"></div>

						<p class="collect">{{ __('messages.prize_collect') }}</p>
						<span style="font-size: 11px !important;font-family: 'Gotham Medium';">{{ __('messages.prize_qualify') }}</span><br>
						<ol style="font-size: 11px !important;font-family: 'Gotham Light';padding-left: 12px;">
							<li>{{ __('messages.prizes_qualify_list1') }}</li>
							<li>{{ __('messages.prizes_qualify_list2') }}</li>
							<li>{{ __('messages.prizes_qualify_list3') }}</li>
						</ol>
					</div>
					<div class="col-sm-6" style="padding: 0px;"></div>
				</div>
			</div>
			<div class="col-3" style="padding: 0px;"></div>
		</div>
		<div class="spacer-10"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-2" style="padding: 0px;">
			</div>
			<div class="col-md-8 tableprize tableprize_chinese" style="padding: 0px;background: #ffe8bc;">
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-12">
						<table class="table prizetable w-100 d-block">
							<tbody>
								<tr>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.homecountry') }}</th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.eVouchers') }}</th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.1stprize') }}<!-- 1<sup style="color: #956407;font-size: 9px;">st</sup> Prize --></th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.2ndprize') }}<!-- 2<sup style="color: #956407;font-size: 9px;">nd</sup> Prize --></th>
									<th class="head_th" style="font-family: 'Gotham Medium';">{{ __('messages.3rdprize') }}<!-- 3<sup style="color: #956407;font-size: 9px;">rd</sup> Prize --></th>
								</tr>
								<tr>
									<td>{{ __('messages.SPAC') }}<sup style="font-size: 9px;">1</sup></td>
									<td>Amazon.com</td>
									<td>
										<span>US$200</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>US$100</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$50</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="">
									<td>{{ __('messages.SEA-K') }}<sup style="font-size: 9px;">2</sup></td>
									<td>Amazon.com</td>
									<td>
										<span>US$200</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$100</span><br>
										<span class="winnerprizes">9 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>US$50</span><br>
										<span class="winnerprizes">30 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="">
									<td>{{ __('messages.greaterchina') }}<sup style="font-size: 9px;">3</td>
									<td>JD.com</td>
									<td>
										<span>1400元</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>700元</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>350元</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
								<tr class="" style="border-bottom: 1px solid #fbce76;">
									<td>{{ __('messages.india') }}</td>
									<td>Amazon.in</td>
									<td>
										<span>₹14,000</span><br>
										<span class="winnerprizes">1 {{ __('messages.winner') }}</span>
									</td>
									<td>
										<span>₹7,000</span><br>
										<span class="winnerprizes">3 {{ __('messages.winners') }}</span>
									</td>
									<td>
										<span>₹3,500</span><br>
										<span class="winnerprizes">10 {{ __('messages.winners') }}</span>
									</td>
								</tr>
							</tbody>
						</table>
						
						<p class="note1" style="font-family: 'Gotham Light';font-size: 10px !important;">
							<span style="font-family: 'Gotham Medium';">{{ __('messages.prizes_note1') }}</span> {{ __('messages.prizes_note2') }}
						</p>
						<!-- <p class="note2" style="">
							• Participants in SPAC, SEA-K and India will win Amazon eVouchers. • Participants in China will win JD.com eVouchers.
						</p> -->
					</div>
				</div>
			</div>
			<div class="col-md-2" style="padding: 0px;"></div>
		</div>
	</div> 
	
	<!-- quiz rules -->
	<div class="quiz-rules d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>	
			<div class="col-md-6" style="padding: 0px;">
				<div class="quiz">
					<span>{{ __('messages.quiz_rules') }}</span>
				</div>
				
				@if (App::isLocale('cn'))
				<img class="img_fluid" src="{{url('/img/quiz-rules_chinese.png')}}" class="mx-auto d-block">
				@else
				<img class="img_fluid" src="{{url('/img/quiz-rules.png')}}" class="mx-auto d-block">
				@endif
			</div>	
			<div class="col-md-3" style="padding: 0px;"></div>	
		</div>
	</div>
	<div class="quiz-rules d-block d-sm-none">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>	
			<div class="col-md-6 text-center" style="padding: 0px;">
				<div class="quiz">
					<span>{{ __('messages.quiz_rules') }}</span>
				</div>
				<div class="spacer-50"></div>
				<div class="spacer-20"></div>
				
				@if (App::isLocale('cn'))
				<img class="img_fluid" src="{{url('/img/quiz-rule_mobile_chinese.png')}}" class="mx-auto d-block">
				@else
				<img class="img_fluid" src="{{url('/img/quiz-rule_mobile.png')}}" class="mx-auto d-block">
				@endif
			</div>	
			<div class="col-md-3" style="padding: 0px;"></div>	
		</div>
	</div>

	<!-- points system -->
	<div class="points d-none d-sm-none d-md-block">
		<div class="spacer-10"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6" style="padding: 0px;">
				<h3 style="color:  #efffec;font-family: 'Gotham Book';">{{ __('messages.pointsystem') }}</h3>
				<div class="spacer-5"></div>
			</div>
			<div class="col-3" style="padding: 0px;"></div>
		</div>
		<div class="">
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6" style="padding: 0px;">
					<div class="row">
						<div class="col-sm-4">
							<div class="pointsimg1">
								<img class="img_fluid w-100" src="{{url('/img/points-system_participation.png')}}">

								<h5 class="top-center-participation1">{{ __('messages.participation') }}</h5>
								<span class="top-center-participation2">{{ __('messages.receive1') }} <span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive2') }}</span> {{ __('messages.receive3') }} <span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive4') }}</span>{{ __('messages.receive5') }}</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="pointsimg2">
								<img class="img_fluid w-100" src="{{url('/img/points-system_know.png')}}">

								<h5 class="top-center-staff1">{{ __('messages.knowyourstuff') }}</h5>
								<span class="top-center-staff2">{{ __('messages.100points') }}</span>
							</div>						
						</div>
						<div class="col-sm-4">
							<div class="pointsimg3">
								<img class="img_fluid w-100" src="{{url('/img/points-system_fastest.png')}}">
								
								<h5 class="top-center-finger1">{{ __('messages.fastestfingersfirst') }}</h5>
								<span class="top-center-finger2">{{ __('messages.complete') }}</span>
								
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
	</div>

	<!-- tablet -->
	<div class="points d-none d-sm-block d-md-none">
		<div class="spacer-10"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-12 text-center" style="padding: 0px;">
				<h3 style="color:  #efffec;font-family: 'Gotham Book';">{{ __('messages.pointsystem') }}</h3>
				<div class="spacer-5"></div>
			</div>
		</div>
		<div class="">
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6" style="padding: 0px;">
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-2" style="padding: 0px;"></div>
						<div class="col-sm-4" style="padding: 0px;">
							<div class="pointsimg1">
								<img class="img_fluid w-100" src="{{url('/img/points-system_participation.png')}}">

								<h5 class="top-center-participation1">{{ __('messages.participation') }}</h5>
								<span class="top-center-participation2">{{ __('messages.receive1') }} <span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive2') }}</span> {{ __('messages.receive3') }}<span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive4') }}</span>{{ __('messages.receive5') }}</span>
							</div>
						</div>
						<div class="col-sm-4" style="padding: 0px;">
							<div class="pointsimg2">
								<img class="img_fluid w-100" src="{{url('/img/points-system_know.png')}}">

								<h5 class="top-center-staff1">{{ __('messages.knowyourstuff') }}</h5>
								<span class="top-center-staff2">{{ __('messages.100points') }}</span>
							</div>						
						</div>
						<div class="col-sm-2" style="padding: 0px;"></div>
					</div>
					<div class="row" style="width: 100%;margin-left: 0px;">
						<div class="col-sm-4" style="padding: 0px;"></div>
						<div class="col-sm-4" style="padding: 0px;">
							<div class="pointsimg3">
								<img class="img_fluid w-100" src="{{url('/img/points-system_fastest.png')}}">
								
								<h5 class="top-center-finger1">{{ __('messages.fastestfingersfirst') }}</h5>
								<span class="top-center-finger2">{{ __('messages.complete') }}</span>
								
							</div>
						</div>
						<div class="col-sm-4" style="padding: 0px;"></div>
					</div>	
				</div>
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
	</div>

	<!-- mobile -->
	<div class="points d-block d-sm-none">
		<div class="spacer-10"></div>
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-3" style="padding: 0px;"></div>
			<div class="col-6" style="padding: 0px;">
				<h3 style="color:  #efffec;font-family: 'Gotham Book';">{{ __('messages.pointsystem') }}</h3>
				<div class="spacer-5"></div>
			</div>
			<div class="col-3" style="padding: 0px;"></div>
		</div>
		<div class="">
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-md-3" style="padding: 0px;"></div>
				<div class="col-md-6" style="padding: 0px;">
					<div class="">
						<div class="col-sm-4">
							<div class="pointsimg1">
								<img class="img_fluid w-100" src="{{url('/img/points-system_participation.png')}}">

								<h5 class="top-center-participation1">{{ __('messages.participation') }}</h5>
								<span class="top-center-participation2">{{ __('messages.receive1') }} <span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive2') }}</span> {{ __('messages.receive3') }} <span style="font-family: Gotham Medium;color: #000;">{{ __('messages.receive4') }}</span>{{ __('messages.receive5') }}</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="pointsimg2">
								<img class="img_fluid w-100" src="{{url('/img/points-system_know.png')}}">

								<h5 class="top-center-staff1">{{ __('messages.knowyourstuff') }}</h5>
								<div class="top-center-staff2">
									<span >{{ __('messages.100points') }}</span>
								</div>
								
							</div>						
						</div>
						<div class="col-sm-4">
							<div class="pointsimg3">
								<img class="img_fluid w-100" src="{{url('/img/points-system_fastest.png')}}">
								
								<h5 class="top-center-finger1">{{ __('messages.fastestfingersfirst') }}</h5>
								<div class="top-center-finger2">
									<span>{{ __('messages.complete') }}</span>
								</div>
								
								
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3" style="padding: 0px;"></div>
			</div>
		</div>
	</div>

@include('layouts.footer-home')

@endsection

