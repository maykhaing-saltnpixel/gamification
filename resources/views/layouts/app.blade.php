<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - {{ config('app.name') }}</title>
        <link rel="icon" href="{{url('/img/hp.png')}}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">

        <!-- <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"> -->
        <!-- <link rel="stylesheet" type="text/css" href="http://www.www8-hp.com/sg/en/system/styles/hpi/hpi-fontface-core.css?v=4.0.2" /> -->
        <link rel="stylesheet" type="text/css" href="https://www8.hp.com/cn/zh/system/styles/hpi/hpi-fontface-core.css?v=4.0.1">

        <!-- <link rel="stylesheet" href="{{url('/css/hp-font.css?v=' . time())}}"> -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
        @if (App::isLocale('cn'))
            <link rel="stylesheet" href="{{url('/css/style_chinese.css?v=' . time())}}">
        @else
            <link rel="stylesheet" href="{{url('/css/style.css?v=' . time())}}">
        @endif
        <link rel="stylesheet" href="{{url('/css/all.css?v=' . time())}}">
        <link rel="stylesheet" href="{{url('/css/jquery.datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{url('/css/bootstrap.min.css?v=' . time())}}">        

        <script
          src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous"></script>
        <script src="{{url('/js/bootstrap.min.js')}}"></script>
        <script src="{{url('/js/jquery.datetimepicker.full.min.js')}}"></script>
        <script src="{{url('/js/moment.min.js')}}"></script>
    </head>

    <body>
        @yield('content')

        <script src="{{url('/js/popper.min.js')}}"></script>
        

        @yield('js')
    </body>
</html>
