@php
	$date = Date('Y/m/d');
	$dates = '2019/05/26';
@endphp
@if (App::isLocale('cn'))
<!-- background-image: url('../img/banner_for_chinese.jpg'); -->
	@if($date < '2019/08/29')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/banner_for_chinese.jpg')}});">
	@elseif($date == '2019/08/29')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/banner_for_chinese.jpg')}});">
	@elseif($date == '2019/08/30')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/banner_for_chinese_2.jpg')}});">
	@elseif($date == '2019/08/31')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/banner_for_chinese_3.jpg')}});">
	@elseif($date >= '2019/09/01')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/banner_for_chinese_4.jpg')}});">
	@endif
@else
	@if($date < '2019/08/29')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/Teaser-Banner-KV.jpg')}});">
	@elseif($date == '2019/08/29')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/Banner-KV-1.jpg')}});">
	@elseif($date == '2019/08/30')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/Banner-KV-2.jpg')}});">
	@elseif($date == '2019/08/31')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/Banner-KV-3.jpg')}});">
	@elseif($date >= '2019/09/01')
		<div class="homeocean d-none d-sm-block" style="background-image: url({{url('./img/Banner-KV-4.jpg')}});">
	@endif
@endif
<!-- <div class="homeocean d-none d-sm-block"> -->

	<div class="spacer-30"></div>
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-2 col-sm-2 col-xs-12" style="padding: 0px;"></div>
		<div class="col-md-8 col-sm-8 col-xs-12" style="padding: 0px;">
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-sm-6"  style="padding: 0px 45px;">
					@if(auth()->check())
						<span class="hp_welcome">{{ __('messages.welcomeguest') }} {{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
					@else
						<span class="hp_welcome">{{ __('messages.welcomeguest') }} Guest</span>
					@endif
				</div>
				<div class="col-sm-6 text-right login_register"  style="padding: 0px 40px;">
					@if(auth()->check())
						<a href="{{url('/logout')}}" class="hplogin" style="color: #fff;text-decoration: none;font-size: 12px;">{{ __('messages.logout') }}</a>
					@else
						<a href="{{url('/login')}}" class="hplogin">{{__('messages.login')}}</a>
						<div style="border-right: 2px solid #ddd;display: inline-block;"></div>
						<a href="{{url('/register')}}" class="hpregister">{{__('messages.register')}}</a>
					@endif
					
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-12" style="padding: 0px;"></div>
	</div>
	<div class="spacer-5"></div>
	<div class="">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-2 col-sm-2 col-xs-12" style="padding: 0px;"></div>
			<div class="col-md-8 col-sm-8 col-xs-12 hpmenu" style="padding: 0px;">
				<nav class="navbar navbar-expand-sm">
				    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarMobile"  aria-controls="navbarMobile" aria-expanded="false" aria-label="Toggle navigation">
				      <span class="navbar-toggler-icon"></span>
				    </button>
				    
				    <div class="collapse navbar-collapse hpmenu_collapse" id="nav-left">
						<ul class="nav navbar-nav first_nav">
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{url('/tutorial_quiz')}}" style="color: #000;text-decoration: none;">{{__('messages.hp_menu1')}} </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" href="{{url('/participants_info')}}" style="color: #000;text-decoration: none;">{{__('messages.hp_menu4')}}</a>
						  	</li>
						</ul>
				    </div>
				    
					@if (App::isLocale('cn'))
					 <div>
				    	<a class="navbar-brand mx-auto" href="{{url('/')}}">
					    	<img class="headerimg_hp_chinese" src="{{url('/img/logo-for-chinese.png')}}">
					    </a>
				    </div>
				   	@else
				    <div>
				    	<a class="navbar-brand mx-auto" href="{{url('/')}}">
					    	<img class="headerimg_hp" src="{{url('/img/final-logo.png')}}">
					    </a>
				    </div>
				    @endif
				   
				   
				    <div class="collapse navbar-collapse hpmenu_collapse">
				      <ul class="nav navbar-nav second_nav">
				          <li class="nav-item">
				              <a class="nav-link" href="{{url('/knowledge_meter')}}" style="color: #000;text-decoration: none;">{{__('messages.hp_menu3')}}</a>
				          </li>
				        <li class="nav-item">
				              <a class="nav-link" href="{{url('/portfolio')}}" style="color: #000;text-decoration: none;">{{__('messages.hp_menu2')}}</a>
				        </li>
				      </ul>
				    </div>
				  
				</nav>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12" style="padding: 0px;"></div>
		</div>
	</div>		
</div>

<!-- Mobile -->
<div class="d-block d-sm-none">	
	<div class="">		
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  	
		  	@if (App::isLocale('cn'))
		  	<a class="navbar-brand" href="{{url('/')}}">
		  		<img class="" src="{{url('/img/logo-for-chinese.png')}}" style="width: 75px;">
		  	</a>
		  	@else
		  	<a class="navbar-brand" href="{{url('/')}}">
		  		<img class="" src="{{url('/img/final-logo.png')}}" style="width: 75px;">
		  	</a>
		  	@endif
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    	<span class="navbar-toggler-icon"></span>
		  	</button>
		 	<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		    	<div class="navbar-nav" style="padding-left:10px;margin:0px;margin-left:0px;margin-right:0px;">
					@if(auth()->check())
						<span class="hp_welcomemobile">Welcome {{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
					@else
						<span class="hp_welcomemobile">{{ __('messages.welcomeguest') }}</span>
					@endif
				
					@if(auth()->check())
						<a href="{{url('/logout')}}" class="nav-item nav-link hploginmobile" style="color: #000;text-decoration: none;font-size: 12px;">{{ __('messages.logout') }}</a>
					@else
						<a href="{{url('/login')}}" class="nav-item nav-link hploginmobile">{{__('messages.login')}}</a>
						<div style="border-right: 2px solid #ddd;display: inline-block;"></div>
						<a href="{{url('/register')}}" class="nav-item nav-link hpregistermobile">{{__('messages.register')}}</a>
					@endif
					
			      	<a class="nav-item nav-link" href="{{url('/tutorial_quiz')}}">{{__('messages.hp_menu1')}}</a>
			      	<a class="nav-item nav-link" href="{{url('/participants_info')}}">{{__('messages.hp_menu4')}}</a>
			      	<a class="nav-item nav-link" href="{{url('/knowledge_meter')}}">{{__('messages.hp_menu3')}}</a>
			      	<a class="nav-item nav-link" href="{{url('/portfolio')}}">{{__('messages.hp_menu2')}}</a>
		    	</div>
		  	</div>
		</nav>
	</div>	

	@if (App::isLocale('cn'))
		@if($date < '2019/08/29')
			<div style="background-image: url({{url('./img/Banner-KV_W1_SC_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date == '2019/08/29')
			<div style="background-image: url({{url('./img/Banner-KV_W1_SC_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>			
		@elseif($date == '2019/08/30')
			<div style="background-image: url({{url('./img/Banner-KV_W2_SC_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date == '2019/08/31')
			<div style="background-image: url({{url('./img/Banner-KV_W3_SC_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date >= '2019/09/01')
			<div style="background-image: url({{url('./img/Banner-KV_W4_SC_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@endif	
	@else
		@if($date < '2019/08/29')
			<div style="background-image: url({{url('./img/banner-KV-1_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date == '2019/08/29')
			<div style="background-image: url({{url('./img/banner-KV-1_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>			
		@elseif($date == '2019/08/30')
			<div style="background-image: url({{url('./img/banner-KV-2_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date == '2019/08/31')
			<div style="background-image: url({{url('./img/banner-KV-3_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@elseif($date >= '2019/09/01')
			<div style="background-image: url({{url('./img/banner-KV-4_mobile.jpg')}});background-repeat: no-repeat !important;background-size: contain !important;height: 300px;">
			</div>
		@endif	
	@endif	
</div>