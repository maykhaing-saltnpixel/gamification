<footer>
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-3" style="padding: 0px;"></div>
		<div class="col-6" style="padding: 0px;">
			<p class="footer_home_1" style="font-family: 'Gotham Light';margin-block-start: 0.3em;
		margin-block-end: 0.3em;"><sup>1</sup>{{ __('messages.SPAC') }}{{ __('messages.footer_home_1') }}</p>
			<p class="footer_home_2" style="font-family: 'Gotham Light';margin-block-start: 0.3em;
		margin-block-end: 0.3em;"><sup>2</sup>{{ __('messages.SEA-K') }}{{ __('messages.footer_home_2') }}</p>
			<p class="footer_home_3" style="font-family: 'Gotham Light';margin-block-start: 0.3em;
		margin-block-end: 0.3em;margin-bottom: 10px;"><sup>3</sup>{{ __('messages.footer_home_3') }}</p>
			<div class="row" style="width: 100%;margin-left: 0px;">
				<div class="col-sm-6" style="padding: 0px;">
					<p style="font-family: Gotham Light;" class="footer_home_4">
						{{ __('messages.copy_right') }}
					</p>
				</div>
				<div class="col-sm-6 text-right d-none d-sm-block" style="padding: 0px;">
					<a href="{{url('/term_of_use')}}" class="footer_home_5" style="color: #000;text-decoration: underline;font-family: Gotham Light;">{{ __('messages.term_condi') }}</a>
				</div>
				<div class="col-sm-6 d-block d-sm-none" style="padding: 0px;">
					<a href="{{url('/term_of_use')}}" class="footer_home_5" style="color: #000;text-decoration: underline;font-family: Gotham Light;">{{ __('messages.term_condi') }}</a>
				</div>
			</div>
		</div>
		<div class="col-3" style="padding: 0px;"></div>
	</div>
	<div class="spacer-20"></div>
</footer>


