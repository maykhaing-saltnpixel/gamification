@extends('layouts.app')

@section('title', 'User Detail')

@section('content')
<style type="text/css">
	.borderless td, .borderless th {
	    border: none;
	}
</style>
	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '1'])

			</div>

			<div class="main-right">
				<div class="container-fluid m-height">

					<h1 class="main-title">User Detail</h1>

					<h5 style="font-size: 16px;text-decoration: underline;"><a href="{{url('/register/user_list')}}"> <i class="fa fa-angle-left"></i> Back To All Registrant </a></h5>
					@include('layouts.messages')

					@if($error != '')
						<div class="alert alert-danger" role="alert">
							{{ $error }}
						</div>
					@else
						<table class="table borderless" style="background-color: #fff">
							<thead style="background-color: #0096D6;color: #fff">
								<tr>
									<td colspan="2">User Detail</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="10%">First name</td>
									<td>{{$user_data->first_name}}</td>
								</tr>
								<tr>
									<td>Last name</td>
									<td>{{$user_data->last_name}}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>{{$user_data->email}}</td>
								</tr>
								<tr>
									<td>Partner ID</td>
									<td>{{$user_data->partner_id}}</td>
								</tr>
								<tr>
									<td>Country</td>
									<td>{{$user_data->country}}</td>
								</tr>
								<tr>
									<td>Company name</td>
									<td>{{$user_data->company_name}}</td>
								</tr>
								<tr>
									<td>Designation</td>
									<td>{{$user_data->designation}}</td>
								</tr>
								<tr>
									<td>Character</td>
									<td>
										<img src="{{url('/img/character/avatar'.$user_data->character.'.png')}}">
									</td>
								</tr>
								<tr>
									<td>Points</td>
									<td>
										@if($user_data->points == '')
											- &nbsp;&nbsp;
											<i class="fa fa-question-circle" data-toggle="tooltip" title="Haven't confirm email yet!" data-placement="right"></i>
										@else
											{{$user_data->points}} points
										@endif
									</td>
								</tr>
								<tr>
									<td>Created date</td>
									<td>{{date('Y/m/d H:i', strtotime($user_data->created_at))}}</td>
								</tr>
							</tbody>
						</table>
					@endif
					
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#country").val('');
			$("#partner_id").val('');
			$("#search_form").submit();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
@endsection
