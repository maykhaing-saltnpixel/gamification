@extends('layouts.app')

@section('title', 'Forget Password')

@section('content')

<div  class="container">
	<h3>Forget Password</h3>
	<hr>
	@include('layouts.messages')
	<form action="{{url('/admin/forget')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
		{{ csrf_field() }}
	    <div class="form-group">
			<label>Email</label>
			<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required>
			@if ($errors->has('email'))
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
	        @else
	        	<div class="invalid-feedback">
		          Please enter a valid email.
		        </div>
	        @endif
	    </div>
		<div class="spacer-20"></div>
		<button type="submit" id="submit_btn" class="btn btn-primary">Submit</button>
	</form>
</div>

<script type="text/javascript">
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();
</script>
@endsection