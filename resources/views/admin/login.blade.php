@extends('layouts.app')

@section('title', 'Login')

@section('content')

<div  class="container">
	<div class="spacer-100"></div>
	@include('layouts.messages')
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<h3 style="text-transform: uppercase;font-weight: 700;">AdminLogin</h3>
			@include('layouts.messages')
			<div class="spacer-5"></div>
			<div class="row" style="padding: 10px;background-color:#e9f2f7;">
				<div class="col-12" style="border: 1px solid #fff">
					<div class="col-sm-12 text-cent" style="padding: 20px;">				
						<form action="{{url('/admin/login')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
							@csrf
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						    <div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required>
								@if ($errors->has('email'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('email') }}</strong>
						            </span>
						        @else
						        	<div class="invalid-feedback">
							          Please enter a valid email.
							        </div>
						        @endif
						    </div>
						    <div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" required>
								@if ($errors->has('password'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('password') }}</strong>
						            </span>
						        @else
						        	<div class="invalid-feedback">
							          Please enter a password.
							        </div>
						        @endif
								
						    </div>
							<!-- <a href="{{url('/admin/forget')}}">Forgot your password?</a> -->
							<div class="spacer-20"></div>
							<button type="submit" id="submit_btn" class="btn btn-primary">Submit</button>
						</form>
					</div>
					<div class="col-sm-6" style="padding: 0px;"></div>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
	
</div>

<script type="text/javascript">
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();
</script>
@endsection