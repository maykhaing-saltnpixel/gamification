<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>User List</title>

        <style>
            
            .mb-0 {
                margin-bottom: 5px;
                margin-top: 0;
            }
            .fade {
                color: #666;
            }
            td {
                vertical-align: top;
            }
            .table-bordered td,.table-bordered th{
                border: 1px solid #ddd;
            }
            .table-bordered td {
                padding-top: 10px;
                padding-bottom: 10 px;
            }
            .table-bordered .thead-dark th {
                color: #fff;
                background-color: #343a40;
                border-color: #454d55;
                line-height: 40px;
            }
            .text-center {
                text-align: center;
            }
            .text-left{
                text-align: left;
            }
            .h3 {
                font-size: 2em;
            }
            .green {
                background: #5eb132;
                color: #fff;
            }
            .h4 {
                font-size: 1.2em;
            }
            .strong {
                font-weight: 700;
            }
            .main-table td:first-child {
                padding-right: 20px;
            }
            tr.noBorder td {
                border: 0;
            }
            .text-center{
                text-align: center;
            }
            .vertical-center {
              margin: 0;
              position: absolute;
              top: 50%;
              -ms-transform: translateY(-50%);
              transform: translateY(-50%);
            }
            .table {
                width: 100%;
                margin-bottom: 1rem;
                color: #212529;
                cell-spacing:0!important;
            }
        </style>
    </head>

    <body>
        <!-- <img src="{{url('/img/logo-small.png')}}" class="img-responsive cen"> -->
        <h2>User List</h2>
        <table class="table-bordered" cellspacing="0" cellpadding="0">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th width='170px'>Name</th>
                    <th width='200px'>E-mail</th>
                    <th width='170px'>Country</th>
                    <th width='150px'>Company Name</th>
                    <th width='150px'>Designation</th>
                    <th width='80px'>Point</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="text-center">
                    <td width='170px'>{{$user->first_name}} {{$user->last_name}}</td>
                    <td width='200px'>{{$user->email}}</td>                    
                    <td width='170px'>{{$user->country}}</td>
                    <td width='150px'>{{$user->company_name}}</td>  
                    <td width='150px'>{{$user->designation}}</td>
                    <td width='80px'>{{$user->points}}</td>
                </tr>
            @endforeach
            </tbody>
          
        </table>

        <br><br>
    </body>
</html>
