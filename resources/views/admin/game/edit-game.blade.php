@extends('layouts.app')

@section('title', 'Edit Game Title')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '2'])

			</div>

			<div class="main-right">
				<div class="container-fluid m-height">

					<h1 class="main-title">Edit Game Title</h1>

					<h5 style="font-size: 16px;text-decoration: underline;"><a href="{{url('/game')}}"> <i class="fa fa-angle-left"></i> Back To All Game </a></h5>

					<div class="spacer-20"></div>

					@include('layouts.messages')

					<form action="{{url('/update/game/title')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
						{{ csrf_field() }}
						<input type="hidden" name="game_id" value="{{$game_data->id}}">
						<div class="row">
							<div class="col-6 form-group">
								<input type="text" name="game_title" class="form-control {{ $errors->has('game_title') ? ' is-invalid' : '' }}" placeholder="Game title" value="{{$game_data->game_title}}" required>

								@if ($errors->has('game_title'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('game_title') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter a game title.
									</div>
								@endif
							</div>

							<div class="col-6 form-group">
								<input type="text" name="cn_game_title" class="form-control {{ $errors->has('cn_game_title') ? ' is-invalid' : '' }}" placeholder="Game title" value="{{$game_data->cn_game_title}}" required>

								@if ($errors->has('cn_game_title'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('cn_game_title') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter a game title.
									</div>
								@endif
							</div>
						</div>
						<div class="spacer-20"></div>
						<div class="row">
							<div class="col-12 form-group">
								
								<img src="{{url('/storage/'.$game_data->game_image)}}" alt="{{$game_data->game_title}}" style="width: 20%;">
								<div class="spacer-10"></div>
								<input type="text" name="game_image_old" id="game_image_old" class="form-control d-none" value="{{$game_data->game_image}}">
								<input type="text" id="active_game_txt" class="form-control {{ $errors->has('inactive_game_image') ? ' is-invalid' : '' }}" placeholder="Browse Game Active image">
								<input type="file" name="game_image" id="game_image" class="form-control d-none" placeholder="Game image">

								@if ($errors->has('game_image'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('game_image') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter a game title.
									</div>
								@endif
							</div>
						</div>
						<div class="spacer-20"></div>
						<div class="row">
							<div class="col-12 form-group">
								
								<img src="{{url('/storage/'.$game_data->inactive_game_image)}}" alt="{{$game_data->game_title}}" style="width: 20%;">
								<div class="spacer-10"></div>
								<input type="text" name="inactive_game_image_old" id="inactive_game_image_old" class="form-control d-none" value="{{$game_data->inactive_game_image}}">
								<input type="text" id="inactive_game_txt" class="form-control {{ $errors->has('inactive_game_image') ? ' is-invalid' : '' }}" placeholder="Browse Game Inactive image">
								<input type="file" name="inactive_game_image" id="inactive_game_image" class="d-none" placeholder="Game image">

								@if ($errors->has('inactive_game_image'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('inactive_game_image') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter a game title.
									</div>
								@endif
							</div>
						</div>
						<div class="spacer-20"></div>
						<div class="row">
							<div class="col-6 form-group">
								<input type="text" class="form-control {{ $errors->has('from_date') ? ' is-invalid' : '' }}" name="from_date" id="from_date" value="{{$game_data->from_date}}" onkeypress="false" placeholder="From date" required autocomplete="off">

								@if ($errors->has('from_date'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('from_date') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter from date.
									</div>
								@endif
							</div>
							<div class="col-6 form-group">
								<input type="text" class="form-control {{ $errors->has('to_date') ? ' is-invalid' : '' }}" name="to_date" id="to_date" value="{{$game_data->to_date}}" placeholder="To date" required autocomplete="off">

								@if ($errors->has('to_date'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('to_date') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter to date.
									</div>
								@endif
							</div>
						</div>
						<input type="hidden" id="currentdate" value="<?php echo date("Y/m/d H:i"); ?>">
						<input type="text" class="d-none" name="publish_flg" id="publish_flg" value="">
						<div class="spacer-20"></div>
						<!-- <button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Create</button> -->
						<button type="button" id="btn_save" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Update</button>
						<button type="button" id="btn_publish" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Publish</button>
						<button type="submit" class="d-none" id="btn_save_submit">submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function () {
			jQuery(function(){
				jQuery('#from_date').datetimepicker({
					onShow:function( ct ){
						this.setOptions({
							maxDateTime:jQuery('#to_date').val()?jQuery('#to_date').val():false,
							minDateTime:jQuery('#currentdate').val()?jQuery('#currentdate').val():false
						})
					}
				});

				jQuery('#to_date').datetimepicker({
					onShow:function( ct ){
						this.setOptions({
							minDateTime:jQuery('#from_date').val()?jQuery('#from_date').val():jQuery('#currentdate').val()
						})
					}
				});
			});
		});

		(function() {
			'use strict';
			window.addEventListener('load', function() {
				var forms = document.getElementsByClassName('needs-validation');
				var validation = Array.prototype.filter.call(forms, function(form) {
					form.addEventListener('submit', function(event) {
						if (form.checkValidity() === false) {
							event.preventDefault();
							event.stopPropagation();
						}
						form.classList.add('was-validated');
					}, false);
				});
			}, false);
		})();

		$('#from_date').keypress(function(e) {
		    e.preventDefault();
		});

		$('#to_date').keypress(function(e) {
		    e.preventDefault();
		});

		$("#btn_save").click(function() {
			var r = window.confirm("This game will be save as draft & u can edit or published.");
			if (r == true) {
				$("#publish_flg").val('1');
				$("#btn_save_submit").click();
			}			
		});

		$("#btn_publish").click(function() {
			var r = window.confirm("This game will be save as draft & u can edit or published.");
			if (r == true) {
				$("#publish_flg").val('0');
				$("#btn_save_submit").click();
			}
		});

		$("#game_image").change(function(e) {
			$("#game_image_old").val('');
			var fileName = e.target.files[0].name;
            $("#active_game_txt").val(fileName);
		});

		$("#inactive_game_txt").click(function() {
			$("#inactive_game_image").click();
		});

		$('#inactive_game_image').change(function(e){
            var fileName = e.target.files[0].name;
            $("#inactive_game_txt").val(fileName);
            $("#inactive_game_image_old").val('');
        });

        $("#active_game_txt").click(function() {
			$("#game_image").click();
		});
	</script>
@endsection
