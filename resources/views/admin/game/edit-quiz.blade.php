@extends('layouts.app')

@section('title', 'Edit Game Quiz')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '2'])

			</div>

			<div class="main-right">
				<div class="container-fluid m-height">

					<h1 class="main-title">Edit Game Quiz</h1>

					<h5 style="font-size: 16px;text-decoration: underline;"><a href="{{url('/game')}}"> <i class="fa fa-angle-left"></i> Back To All Game </a></h5>

					<div class="spacer-20"></div>

					@include('layouts.messages')

					<div class="row">
						<div class="col-12 text-right">
							<button type="button" id="add_quiz" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Add More quiz</button>
						</div>
					</div>

					<form action="{{url('/game/quiz/update')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
						{{ csrf_field() }}
						<input type="hidden" name="quiz_count" id="quiz_count" value="{{count($question_data)}}">
						<input type="hidden" name="game_id" id="game_id" value="{{$game_data->id}}">
						@php $q_count = 1; @endphp
						@foreach($question_data as $q_data)
							<div class="form-group" id="quiz_{{$q_count}}">
								<label style="font-weight: bold;">{{$q_count}}.</label>
								<div class="row">
									<div class="col-6">
										<!-- <input type="text" name="quiz_q_{{$q_count}}" id="quiz_q_{{$q_count}}" class="form-control" placeholder="Quiz question" value="{{$q_data->question}}" required> -->
										<textarea name="quiz_q_{{$q_count}}" id="quiz_q_{{$q_count}}" class="form-control" placeholder="Quiz question" required>{{$q_data->question}}</textarea>
									</div>
									<div class="col-6">
										<!-- <input type="text" name="cn_quiz_q_{{$q_count}}" id="cn_quiz_q_{{$q_count}}" class="form-control" placeholder="Chinese Quiz question" value="{{$q_data->cn_question}}" required> -->
										<textarea name="cn_quiz_q_{{$q_count}}" id="cn_quiz_q_{{$q_count}}" class="form-control" placeholder="Chinese Quiz question" required>{{$q_data->cn_question}}</textarea>
									</div>
								</div>
								
								@if ($errors->has('quiz_q_$q_count'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('quiz_q_$q_count') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please enter quiz question.
									</div>
								@endif
								<div class="spacer-20"></div>
								<select class="form-control" name="quiz_{{$q_count}}_type" id="quiz_{{$q_count}}_type" onchange="checkType(this,'{{$q_count}}')" required>
									<option value="1" {{ $q_data->question_type == '1' ? 'selected':'' }}>Radio</option>
									<option value="2" {{ $q_data->question_type == '2' ? 'selected':'' }}>Checkbox</option>
								</select>
								@if ($errors->has('quiz_$q_count_type'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('quiz_$q_count_type') }}</strong>
									</span>
								@else
									<div class="invalid-feedback">
										Please select quiz type.
									</div>
								@endif
								<div class="spacer-20"></div>
								<div class="row">
									<div class="col-1">
										<label style="font-weight: bold;">Answer</label>
									</div>
									<div class="col-11">
										<button type="button" onclick="add_answer('{{$q_count}}');" class="btn btn-xs btn-default">
											<i class="fa fa-plus"></i>
										</button>
									</div>
								</div>

								<div class="spacer-20"></div>

								@php $ans_count = 1; @endphp
								@foreach($answer_data as $ans)
									@if($ans->question_id == $q_data->id)
										<div class="row" id="ans_div_{{$q_count}}_{{$ans_count}}">
											<div class="col-10">
												<div class="row">
													<div class="col-6">
														<input type="text" name="quiz_a_{{$q_count}}_{{$ans_count}}" id="quiz_a_{{$q_count}}_{{$ans_count}}" class="form-control" placeholder="Quiz answer" value="{{$ans->answer}}" required>
													</div>
													<div class="col-6">
														<input type="text" name="cn_quiz_a_{{$q_count}}_{{$ans_count}}" id="cn_quiz_a_{{$q_count}}_{{$ans_count}}" class="form-control" placeholder="Chinese Quiz answer" value="{{$ans->cn_answer}}" required>
													</div>
												</div>
												
												@if ($errors->has('quiz_a_$q_count_$ans_count'))
													<span class="invalid-feedback" role="alert">
														<strong>{{ $errors->first('quiz_a_$q_count_$ans_count') }}</strong>
													</span>
												@else
													<div class="invalid-feedback">
														Please enter answer.
													</div>
												@endif
											</div>
											<div class="col-2 ans_radio_div_{{$q_count}}">
												@if($q_data->question_type == 1)
													<input type="radio" class="form-check-input" name="correct_a_{{$q_count}}" id="correct_a_{{$q_count}}" value="{{$q_count}}_{{$ans_count}}" {{ $ans->correct_flg == '0' ? 'checked':'' }} required>
													@if ($errors->has('correct_a_1'))
														<span class="invalid-feedback" role="alert">
															<strong>{{ $errors->first('correct_a_1') }}</strong>
														</span>
													@else
														<div class="invalid-feedback">
															Please select correct answer.
														</div>
													@endif
												@endif
											</div>
										</div>
										<div class="spacer-20"></div>
										@php $ans_count = $ans_count +1; @endphp
									@endif
									
								@endforeach
								<input type="hidden" name="ans_count_{{$q_count-1}}" id="ans_count_{{$q_count}}" value="{{$ans_count-1}}">
							</div>
							@php $q_count = $q_count +1; @endphp
						@endforeach

						<button type="submit" id="btn_save" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Update</button>
						<!-- <button type="button" id="btn_publish" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Publish</button> -->
						<button type="submit" class="d-none" id="btn_save_submit">submit</button>
						<div class="spacer-20"></div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#add_quiz").click(function() {
			var quiz_count = $("#quiz_count").val();
			var quiz_count_new = parseInt(quiz_count)+1;
			$("#quiz_count").val(quiz_count_new);

			var quiz_html = '<div class="spacer-10"></div>'+
							'<hr>'+
							'<div class="form-group" id="quiz_'+quiz_count_new+'">'+
								'<label style="font-weight: bold;">'+quiz_count_new+'.</label>'+
								'<div class="row">'+
									'<div class="col-6">'+
										'<textarea name="quiz_q_'+quiz_count_new+'" id="quiz_q_'+quiz_count_new+'" class="form-control" placeholder="Quiz question" required></textarea>'+
									'</div>'+
									'<div class="col-6">'+
										'<textarea name="cn_quiz_q_'+quiz_count_new+'" id="cn_quiz_q_'+quiz_count_new+'" class="form-control" placeholder="Chinese Quiz question" required></textarea>'+
									'</div>'+
								'</div>'+
									'@if ($errors->has("quiz_q_'+quiz_count_new+'"))'+
										'<span class="invalid-feedback" role="alert">'+
											'<strong>{{ $errors->first("quiz_q_'+quiz_count_new+'") }}</strong>'+
										'</span>'+
									'@else'+
										'<div class="invalid-feedback">'+
											'Please enter quiz question.'+
										'</div>'+
									'@endif'+
									'<div class="spacer-20"></div>'+
									'<select class="form-control" name="quiz_'+quiz_count_new+'_type" id="quiz_'+quiz_count_new+'_type" onchange="checkType(this,'+quiz_count_new+')" required>'+
										'<option value="1">Radio</option>'+
										'<option value="2">Checkbox</option>'+
									'</select>'+
									'@if ($errors->has("quiz_'+quiz_count_new+'_type"))'+
										'<span class="invalid-feedback" role="alert">'+
											'<strong>{{ $errors->first("quiz_'+quiz_count_new+'_type") }}</strong>'+
										'</span>'+
									'@else'+
										'<div class="invalid-feedback">'+
											'Please select quiz type.'+
										'</div>'+
									'@endif'+
									'<div class="spacer-20"></div>'+
									'<div class="row">'+
										'<div class="col-1">'+
											'<label style="font-weight: bold;">Answer</label>'+
										'</div>'+
										'<div class="col-11">'+
											'<button type="button" onclick="add_answer('+quiz_count_new+');" class="btn btn-xs btn-default">'+
												'<i class="fa fa-plus"></i>'+
											'</button>'+
										'</div>'+
									'</div>'+

									'<div class="spacer-20"></div>'+

									'<input type="hidden" name="ans_count_'+quiz_count_new+'" id="ans_count_'+quiz_count_new+'" value="1">'+
									'<div class="row" id="ans_div_'+quiz_count_new+'_1">'+
										'<div class="col-10">'+
											'<div class="row">'+
												'<div class="col-6">'+
													'<input type="text" name="quiz_a_'+quiz_count_new+'_1" id="quiz_a_'+quiz_count_new+'_1" class="form-control" placeholder="Quiz answer" required>'+
												'</div>'+
												'<div class="col-6">'+
													'<input type="text" name="cn_quiz_a_'+quiz_count_new+'_1" id="cn_quiz_a_'+quiz_count_new+'_1" class="form-control" placeholder="Chinese Quiz answer" required>'+
												'</div>'+
											'</div>'+
											'@if ($errors->has("quiz_a_'+quiz_count_new+'_1"))'+
												'<span class="invalid-feedback" role="alert">'+
													'<strong>{{ $errors->first("quiz_a_'+quiz_count_new+'_1") }}</strong>'+
												'</span>'+
											'@else'+
												'<div class="invalid-feedback">'+
													'Please enter answer.'+
												'</div>'+
											'@endif'+
										'</div>'+
										'<div class="col-2 ans_radio_div_'+quiz_count_new+'">'+
										 	'<input type="radio" class="form-check-input" name="correct_a_'+quiz_count_new+'" id="correct_a_'+quiz_count_new+'" value="'+quiz_count_new+'_'+'1" required>'+
										 	'@if ($errors->has("correct_a_'+quiz_count_new+'"))'+
												'<span class="invalid-feedback" role="alert">'+
													'<strong>{{ $errors->first("correct_a_'+quiz_count_new+'") }}</strong>'+
												'</span>'+
											'@else'+
												'<div class="invalid-feedback">'+
													'Please select correct answer.'+
												'</div>'+
											'@endif'+
										'</div>'+
									'</div>'+
								'</div>';

			$("#quiz_"+quiz_count).after(quiz_html);
		});

		$("#btn_save").click(function() {
			$("#publish_flg").val('1');
			$("#btn_save_submit").click();			
		});

		$("#btn_publish").click(function() {
			$("#publish_flg").val('0');
			$("#btn_save_submit").click();
		});

		function add_answer(q_id) {
			var sel_type = $("#quiz_"+q_id+"_type").val();
			var ans_count = $("#ans_count_"+q_id).val();
			var ans_count_new = parseInt(ans_count)+1;
			$("#ans_count_"+q_id).val(ans_count_new);

			if (sel_type == 1) {
				var ans_html = 	'<div class="spacer-20"></div>'+
								'<div class="row" id="ans_div_'+q_id+'_'+ans_count_new+'">'+
									'<div class="col-10">'+
										'<div class="row">'+
											'<div class="col-6">'+
												'<input type="text" name="quiz_a_'+q_id+'_'+ans_count_new+'" id="quiz_a_'+q_id+'_'+ans_count_new+'" class="form-control" placeholder="Quiz answer" required>'+
											'</div>'+
											'<div class="col-6">'+
												'<input type="text" name="cn_quiz_a_'+q_id+'_'+ans_count_new+'" id="cn_quiz_a_'+q_id+'_'+ans_count_new+'" class="form-control" placeholder="Chinese Quiz answer" required>'+
											'</div>'+
										'</div>'+
										'@if ($errors->has("quiz_a_'+q_id+'_'+ans_count_new+'"))'+
											'<span class="invalid-feedback" role="alert">'+
												'<strong>{{ $errors->first("quiz_a_'+q_id+'_'+ans_count_new+'") }}</strong>'+
											'</span>'+
										'@else'+
											'<div class="invalid-feedback">'+
												'Please enter answer.'+
											'</div>'+
										'@endif'+
									'</div>'+
									'<div class="col-2 ans_radio_div_'+q_id+'">'+
									 	'<input type="radio" class="form-check-input" name="correct_a_'+q_id+'" id="correct_a_'+q_id+'" id="correct_a_'+q_id+'" value="'+q_id+'_'+ans_count_new+'" required>'+
									 	'@if ($errors->has("correct_a_'+q_id+'"))'+
											'<span class="invalid-feedback" role="alert">'+
												'<strong>{{ $errors->first("correct_a_'+q_id+'") }}</strong>'+
											'</span>'+
										'@else'+
											'<div class="invalid-feedback">'+
												'Please select correct answer.'+
											'</div>'+
										'@endif'+
									'</div>'+
								'</div>';

				$("#ans_div_"+q_id+'_'+ans_count).after(ans_html);
			} else {
				var ans_html = 	'<div class="spacer-20"></div>'+
								'<div class="row" id="ans_div_'+q_id+'_'+ans_count_new+'">'+
									'<div class="col-9">'+
										'<input type="text" name="quiz_a_'+q_id+'_'+ans_count_new+'" id="quiz_a_'+q_id+'_'+ans_count_new+'" class="form-control" placeholder="Quiz answer" required>'+
										'@if ($errors->has("quiz_a_'+q_id+'_'+ans_count_new+'"))'+
											'<span class="invalid-feedback" role="alert">'+
												'<strong>{{ $errors->first("quiz_a_'+q_id+'_'+ans_count_new+'") }}</strong>'+
											'</span>'+
										'@else'+
											'<div class="invalid-feedback">'+
												'Please enter answer.'+
											'</div>'+
										'@endif'+
									'</div>'+
								'</div>';

				$("#ans_div_"+q_id+'_'+ans_count).after(ans_html);
			}
		}

		(function() {
		  'use strict';
		  window.addEventListener('load', function() {
		    // Fetch all the forms we want to apply custom Bootstrap validation styles to
		    var forms = document.getElementsByClassName('needs-validation');
		    // Loop over them and prevent submission
		    var validation = Array.prototype.filter.call(forms, function(form) {
		      form.addEventListener('submit', function(event) {
		        if (form.checkValidity() === false) {
		          event.preventDefault();
		          event.stopPropagation();
		        }
		        form.classList.add('was-validated');
		      }, false);
		    });
		  }, false);
		})();

		function checkType(sel,que_count) {
			var sel_value = sel.value;
			alert(sel_value);
			if (sel_value == 2) {
				$(".ans_radio_div_"+que_count).addClass('d-none');
				// $(".ans_radio_div_"+que_count).html('');
			} else {
				$(".ans_radio_div_"+que_count).removeClass('d-none');
				$(".correct_a_"+que_count).attr('required','true');
			}
		}
	</script>
@endsection
