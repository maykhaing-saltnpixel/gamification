@extends('layouts.app')

@section('title', 'Edit Game Tutorial')

@section('content')
	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '2'])

			</div>

			<div class="main-right">
				<div class="container-fluid m-height">

					<h1 class="main-title">Edit Game Tutorial</h1>

					<h5 style="font-size: 16px;text-decoration: underline;"><a href="{{url('/game')}}"> <i class="fa fa-angle-left"></i> Back To All Game </a></h5>

					<div class="spacer-20"></div>

					@include('layouts.messages')

					<div class="row">
						<div class="col-12 text-right">
							<button type="button" id="add_tutorial" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Add More Items</button>
						</div>
					</div>

					<form action="{{url('/tuto_update')}}" class="needs-validation" id="tuto_form" method="post" enctype="multipart/form-data" novalidate>
						{{ csrf_field() }}
						<input type="hidden" name="tuto_count" id="tuto_count" value="{{count($tutorial)}}">
						<input type="hidden" name="game_id" id="game_id" value="{{$game_data->id}}">
						@php $count = 1; @endphp
						@foreach($tutorial as $tuto)
							<div class="form-group" id="tutorial_{{$count}}">
								<label style="font-weight: bold;">{{$count}}.</label>
								<select class="form-control {{ $errors->has('tuto_$count_type') ? ' is-invalid' : '' }}" name="tuto_{{$count}}_type" id="tuto_{{$count}}_type" onchange="getType({{$count}});" required>
									<option value="">Select Type</option>
									<option value="1" {{ $tuto->tuto_type == '1' ? 'selected':'' }}>Text</option>
									<option value="2" {{ $tuto->tuto_type == '2' ? 'selected':'' }}>Image</option>
									<option value="3" {{ $tuto->tuto_type == '3' ? 'selected':'' }}>Video</option>
								</select>
								<div id="tuto_{{$count}}">
									<div class="spacer-20"></div>
									@if($tuto->tuto_type == 1)
										<textarea name="tuto_{{$count}}_file" class="form-control {{ $errors->has('tuto_$count_file') ? ' is-invalid' : '' }}" rows="5" style="resize:none;" required>{{$tuto->tuto_value}}</textarea>
									@elseif($tuto->tuto_type == 2)
										@foreach($tuto_photo as $t_photo)
											@if($t_photo->tuto_id == $tuto->id)
												<img src="{{url('/storage/'.$t_photo->photo)}}">
											@endif
										@endforeach
										<br>
										<label>Image for English</label>
										<input type="file" name="tuto_{{$count}}_file[]" class="form-control {{ $errors->has('tuto_$count_file') ? ' is-invalid' : '' }}" multiple required>

										@foreach($chinese_tuto_photo as $t_photo)
											@if($t_photo->tuto_id == $tuto->id)
												<img src="{{url('/storage/'.$t_photo->photo)}}">
											@endif
										@endforeach
										<br>
										<label>Image for English</label>
										<input type="file" name="tuto_{{$count}}_file_chinese[]" class="form-control {{ $errors->has('tuto_$count_file_chinese') ? ' is-invalid' : '' }}" multiple required>
										<hr>
									@elseif($tuto->tuto_type == 3)
										<input type="file" name="tuto_{{$count}}_file" class="form-control {{ $errors->has('tuto_$count_file') ? " is-invalid" : "" }}" multiple required>
										<hr>
									@endif
									<hr class="tuto_{{$count}}_hr d-none">
								</div>
							</div>
							@php $count = $count+1; @endphp
						@endforeach
						

						<input type="text" class="d-none" name="publish_flg" id="publish_flg" value="">

						<button type="submit" id="btn_save" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Save</button>
						<button type="submit" class="d-none" id="btn_save_submit">submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function getType(tuto_id) {
			if ($('#tuto_'+tuto_id+'_type').val() == 1) {
				var text_html = '<div class="spacer-20"></div>'+
								'<textarea name="tuto_'+tuto_id+'_file" class="form-control {{ $errors->has("tuto_'+tuto_id+'_file") ? " is-invalid" : "" }}" rows="5" style="resize:none;" required></textarea>'+
								'<hr>';
				$("#tuto_"+tuto_id).html(text_html);
			} else if($('#tuto_'+tuto_id+'_type').val() == 2) {
				$('#tuto_'+tuto_id+'_type').after('');
				var text_html = '';
				text_html = '<div class="spacer-20"></div>'+
								'<label>Image for English</label>'+
								'<input type="file" name="tuto_'+tuto_id+'_file[]" class="form-control {{ $errors->has("tuto_'+tuto_id+'_file") ? " is-invalid" : "" }}" multiple required>'+
								'<br>'+
								'<label>Image for Chinese</label>'+
								'<input type="file" name="tuto_'+tuto_id+'_file_chinese[]" class="form-control {{ $errors->has("tuto_'+tuto_id+'_file_chinese") ? " is-invalid" : "" }}" multiple required>'+
								'<hr>';
				$("#tuto_"+tuto_id).html(text_html);
			} else if($('#tuto_'+tuto_id+'_type').val() == 3) {
				$('#tuto_'+tuto_id+'_type').after('');
				var text_html = '';
				text_html = '<div class="spacer-20"></div>'+
								'<label>Video for English</label>'+
								'<input type="file" name="tuto_'+tuto_id+'_file" class="form-control {{ $errors->has("tuto_'+tuto_id+'_file") ? " is-invalid" : "" }}" multiple required>'+
								'<br>'+
								'<label>Video for Chinese</label>'+
								'<input type="file" name="tuto_'+tuto_id+'_file_chinese" class="form-control {{ $errors->has("tuto_'+tuto_id+'_file_chinese") ? " is-invalid" : "" }}" multiple required>'+
								'<hr>';
				$("#tuto_"+tuto_id).html(text_html);
			} else {
				$("#tuto_"+tuto_id).html('');
			}
		}

		$("#add_tutorial").click(function() {
			var tuto_count = parseInt($("#tuto_count").val());
			var tuto_count_new = tuto_count + 1;

			var tuto_html = '<div class="form-group" id="tutorial_'+tuto_count_new+'">'+
								'<label style="font-weight: bold;">'+tuto_count_new+'.</label>'+
								'<select class="form-control {{ $errors->has("tuto_'+tuto_count_new+'_type") ? " is-invalid" : "" }}" name="tuto_'+tuto_count_new+'_type" id="tuto_'+tuto_count_new+'_type" onchange="getType('+tuto_count_new+');" required>'+
									'<option value="">Select Type</option>'+
									'<option value="1">Text</option>'+
									'<option value="2">Image</option>'+
									'<option value="3">Video</option>'+
								'</select>'+
								'<div id="tuto_'+tuto_count_new+'"></div>'+
							'</div>';

			$("#tutorial_"+tuto_count).after(tuto_html);
			$("#tuto_count").val(tuto_count_new);
		});

		$("#btn_save").click(function() {
			var r = window.confirm("This tutorial will be save as draft & u can edit or published.");
			if (r == true) {
				$("#publish_flg").val('1');
				$("#btn_save_submit").click();
			}
		});

		$("#btn_publish").click(function() {
			if ($("#tuto_count").val() == 3) {
				var r = window.confirm("This tutorial will be save as draft & u can edit or published.");
				if (r == true) {
					$("#publish_flg").val('0');
				$("#btn_save_submit").click();
				}
			} else {
				alert('You have to add 3 tutorial item.');
			}
		});

		(function() {
			'use strict';
			window.addEventListener('load', function() {
				var forms = document.getElementsByClassName('needs-validation');
				var validation = Array.prototype.filter.call(forms, function(form) {
					form.addEventListener('submit', function(event) {
						if (form.checkValidity() === false) {
							event.preventDefault();
							event.stopPropagation();
						}
						form.classList.add('was-validated');
					}, false);
				});
			}, false);
		})();
	</script>
@endsection
