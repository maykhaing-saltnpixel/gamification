@extends('layouts.app')

@section('title', 'Game')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '2'])

			</div>

			<div class="main-right">
				<div class="container-fluid m-height">

					<h1 class="main-title">Game</h1>

					@include('layouts.messages')

					<a href="{{url('/create/game/title')}}">
						<button type="button" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Create</button>
					</a>

					<div class="spacer-20"></div>

					<div class="row">
						<div class="col-12">
							@if(count($game_list) > 0)
								<table class="table">
									<thead>
										<tr style="background-color: #0096D6;color: #fff;">
											<th scope="col" style="vertical-align: top;">#</th>
											<th scope="col" style="vertical-align: top;">Game Title</th>
											<th scope="col" style="vertical-align: top;">Start Date</th>
											<th scope="col" style="vertical-align: top;">End Date</th>
											<th scope="col" style="vertical-align: top;">Created date</th>
											<th scope="col" style="vertical-align: top;">Action</th>
										</tr>
									</thead>
									<tbody>
										@php $count = 1; @endphp
										@foreach($game_list as $g_list)
											<tr>
												<th scope="row">{{$count}}</th>
												<td>{{ $g_list->game_title }}</td>
												<td>
													{{date('Y/m/d H:i', strtotime($g_list->from_date))}}
												</td>
												<td>
													{{date('Y/m/d H:i', strtotime($g_list->to_date))}}
												</td>
												<td>
													{{date('Y/m/d H:i', strtotime($g_list->created_at))}}
													<!-- {{$g_list->tut_game_id}} -->
												</td>
												<td>
													@if($g_list->publish_flg == 0)
														<span style="font-weight: bold;color: #0096D6;">PUBLISHED</span>
														<a href="{{ url('/game/unplubish/'.$g_list->id) }}" class="btn btn-outline-danger">UNPLUBISH GAME</a>
													@else
														<a href="{{ url('/game/edit/'.$g_list->id) }}" class="btn btn-outline-primary">Edit Game</a>
														@if($g_list->tuto_flg != 0)
															<a href="{{ url('/game/tuto/edit/'.$g_list->id) }}" class="btn btn-outline-primary">Edit Tutorial</a>
														@else
															<a href="{{ url('/game/tuto/add/'.$g_list->id) }}" class="btn btn-outline-primary">Add Tutorial</a>
														@endif
														@if($g_list->question_flg != 0)
															<a href="{{ url('/game/quiz/edit/'.$g_list->id) }}" class="btn btn-outline-primary">Edit Quiz</a>
														@else
															<a href="{{ url('/game/quiz/add/'.$g_list->id) }}" class="btn btn-outline-primary">Add Quiz</a>
														@endif
													@endif	
												</td>
											</tr>
											@php $count = $count+1; @endphp
										@endforeach
									</tbody>
								</table>
							@else
								<div class="alert alert-info" role="alert">
									<p>There is no game created now.</p>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
