@extends('layouts.app')

@section('title', 'Login Attempt List')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '5'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Login Attempt List</h1>

					@include('layouts.messages')

					<div class="col-md-12">
						<form action="{{url('/admin/login_month/search')}}" method="post" id="search_form" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-3">
									<select class="form-control" name="login_month" id="login_month">
										<option value="" {{ $login_month_search_data == '' ? 'selected':'' }}>Select Month</option>
										<option value="January" {{ $login_month_search_data == 'January' ? 'selected':'' }}>January</option>
										<option value="February" {{ $login_month_search_data == 'February' ? 'selected':'' }}>February</option>
										<option value="March" {{ $login_month_search_data == 'March' ? 'selected':'' }}>March</option>
										<option value="April" {{ $login_month_search_data == 'April' ? 'selected':'' }}>April</option>
										<option value="May" {{ $login_month_search_data == 'May' ? 'selected':'' }}>May</option>
										<option value="June" {{ $login_month_search_data == 'June' ? 'selected':'' }}>June</option>
										<option value="July" {{ $login_month_search_data == 'July' ? 'selected':'' }}>July</option>
										<option value="August" {{ $login_month_search_data == 'August' ? 'selected':'' }}>August</option>
										<option value="September" {{ $login_month_search_data == 'September' ? 'selected':'' }}>September</option>
										<option value="October" {{ $login_month_search_data == 'October' ? 'selected':'' }}>October</option>
										<option value="November" {{ $login_month_search_data == 'November' ? 'selected':'' }}>November</option>
										<option value="December" {{ $login_month_search_data == 'December' ? 'selected':'' }}>December</option>
									</select>
								</div>
								<div class="col-1">
									<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Search</button>
								</div>
								<div class="col-1">
									<button type="button" class="btn btn-default" id="clear_btn">Clear</button>
								</div>
							</div>
						</form>		
					</div>

					<div class="spacer-30"></div>

					@if(count($login_attempt_list) > 0)
						<table class="table">
							<thead>
								<tr style="background-color: #0096D6;color: #fff;">
									<th scope="col" style="vertical-align: top;">No</th>
									<th scope="col" style="vertical-align: top;">Name</th>
									<th scope="col" style="vertical-align: top;">Email</th>
									<th scope="col" style="vertical-align: top;">Login Month</th>
									<th scope="col" style="vertical-align: top;">Login Count</th>
								</tr>
							</thead>
							<tbody style="background-color: #fff;">
								@php $count = 1; @endphp
								@foreach($login_attempt_list as $list)
									@php
										$user_data = App\Models\User::find($list->user_id);
									@endphp
									<tr>
										<th scope="row">{{$count}}</th>
										<td>{{$user_data->first_name}} {{$user_data->last_name}}</td>
										<td>{{$user_data->email}}</td>
										<td>{{$list->login_month}}</td>
										<td>{{$list->total}}</td>
									</tr>
									@php $count = $count+1; @endphp
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info" role="alert">
							<p>There is no registrant right now.</p>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#login_month").val('');
			$("#search_form").submit();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
@endsection
