@extends('layouts.app')

@section('title', 'Game Unfinished List')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">
				@include('admin.inc.left-menu', ['page' => '6'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Game Unfinished List</h1>

					@include('layouts.messages')

					<div class="col-md-12">
						<form action="{{url('/admin/unfinish/search')}}" method="post" id="search_form" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-3">
									<input type="text" class="form-control" name="partner_id" id="partner_id" value="{{$partner_id}}" placeholder="partner ID">
								</div>
								<div class="col-3">
									<select class="form-control" name="game_id" id="game_id">
										<option value="" {{ $game_id == '' ? 'selected':'' }}>Select Game</option>
										@foreach($game_list as $g_list)
											<option value="{{$g_list->id}}" {{ $game_id == $g_list->id ? 'selected':'' }}>{{$g_list->game_title}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-3">
									<input type="text" class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{$date}}" onkeypress="false" placeholder="Date" autocomplete="off">
								</div>
								<div class="col-1">
									<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Search</button>
								</div>
								<div class="col-1">
									<button type="button" class="btn btn-default" id="clear_btn">Clear</button>
								</div>
							</div>
						</form>		
					</div>

					<div class="spacer-30"></div>

					@if(count($unfinish_list) > 0)
						<table class="table">
							<thead>
								<tr style="background-color: #0096D6;color: #fff;">
									<th scope="col" style="vertical-align: top;">No</th>
									<th scope="col" style="vertical-align: top;">PartnerID</th>
									<th scope="col" style="vertical-align: top;">Name</th>
									<th scope="col" style="vertical-align: top;">Game Title</th>
									<th scope="col" style="vertical-align: top;">Step Reached</th>
									<th scope="col" style="vertical-align: top;">Date</th>
								</tr>
							</thead>
							<tbody style="background-color: #fff;">
								@php $count = 1; @endphp
								@foreach($unfinish_list as $list)
									@php
										$game_data = App\Models\Game::find($list->game_id);
									@endphp
									@foreach($user_list as $u_list)
										@if($list->user_id == $u_list->id)
											<tr>
												<th scope="row">{{$count}}</th>
												<td>{{$u_list->partner_id}}</td>
												<td>{{$u_list->first_name}} {{$u_list->last_name}}</td>
												<td>{{$game_data->game_title}}</td>
												<td>
													@if($list->tutorial_flg == 1)
														Tutorial
													@elseif($list->tutorial_flg == 0 && $list->question_id == 0)
														Complete
													@else
														@php $question_data = App\Models\Question::find($list->question_id); @endphp
														@if(#question_data == '')
														@else
															<strong>Q : </strong>{{$question_data->question}}
														@endif
													@endif
												</td>
												<td>{{$list->start_try_date}} {{$list->start_try_time}}</td>
											</tr>
										@endif
									@endforeach
									
									@php $count = $count+1; @endphp
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info" role="alert">
							<p>There is no registrant right now.</p>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#game_id").val('');
			$("#partner_id").val('');
			$("#date").val('');
			$("#search_form").submit();
		});

		$(function () {
			jQuery(function(){
				jQuery('#date').datetimepicker({
					format: 'Y-m-d',
					timepicker: false
				});
			});
		});

		$('#from_date').keypress(function(e) {
		    e.preventDefault();
		});
	</script>
@endsection
