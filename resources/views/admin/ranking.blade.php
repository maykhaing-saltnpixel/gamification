@extends('layouts.app')

@section('title', 'Ranking List')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '3'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Ranking List</h1>

					@include('layouts.messages')

					@if(count($ranking_list) > 0)
						<table class="table">
							<thead>
								<tr style="background-color: #0096D6;color: #fff;">
									<th scope="col" style="vertical-align: top;">#</th>
									<th scope="col" style="vertical-align: top;">First name</th>
									<th scope="col" style="vertical-align: top;">Last name</th>
									<th scope="col" style="vertical-align: top;">Email</th>
									<th scope="col" style="vertical-align: top;">Partner ID</th>
									<th scope="col" style="vertical-align: top;">Country</th>
									<th scope="col" style="vertical-align: top;">Company name</th>
									<th scope="col" style="vertical-align: top;">Designation</th>
									<th scope="col" style="vertical-align: top;">Points</th>
									<th scope="col" style="vertical-align: top;">Created date</th>
								</tr>
							</thead>
							<tbody style="background-color: #fff;">
								@php $count = 1; @endphp
								@foreach($ranking_list as $list)
									<tr>
										<th scope="row">{{$count}}</th>
										<td>
											<a href="{{url('/user_detail/'.$list->id)}}" style="color: #000;text-decoration: underline;">
												{{$list->first_name}}
											</a>
										</td>
										<td>{{$list->last_name}}</td>
										<td>{{$list->email}}</td>
										<td>{{$list->partner_id}}</td>
										<td>{{$list->country}}</td>
										<td>{{$list->company_name}}</td>
										<td>{{$list->designation}}</td>
										<td>
											@if($list->points == '')
												-<br>
												<i class="fa fa-question-circle" data-toggle="tooltip" title="Haven't confirm email yet!" data-placement="bottom"></i>
											@else
												{{$list->points}}
											@endif
										</td>
										<td>
											{{date('Y/m/d H:i', strtotime($list->created_at))}}
										</td>
									</tr>
									@php $count = $count+1; @endphp
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info" role="alert">
							<p>There is no registrant right now.</p>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

@endsection