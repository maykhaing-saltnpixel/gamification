@extends('layouts.app')

@section('title', 'Prize List')

@section('content')

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '5'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Prize List</h1>

					@include('layouts.messages')

					<div class="col-md-12">
						<form action="{{url('/admin/prize/create')}}" method="get" id="download_form" enctype="multipart\form-data">
							<div class="row">
								<div class="col-12 text-right">
									<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Create</button>
								</div>
							</div>
						</form>	
					</div>
	
					<div class="spacer-30"></div>

					@if(isset($prizes) && count($prizes) > 0)
						<table class="table">
							<thead>
								<tr style="background-color: #0096D6;color: #fff;">
									<th scope="col" style="vertical-align: top;">#</th>
									<th scope="col" style="vertical-align: top;">Location</th>
									<th scope="col" style="vertical-align: top;">Image</th>
								</tr>
							</thead>
							<tbody style="background-color: #fff;">
								@php $count = 1; @endphp
								@foreach($prizes as $prize)
									<tr>
										<th scope="row">{{$count}}</th>
										<td>{{$prize->location}}</td>
										<td>
											@if(Storage::exists($prize->image))
												<img class="imageThumb" src="{{url('/storage/' . $prize->image)}}" style="width:100px;height: 80px;"/>
											@else
												<img class="imageThumb" src="{{url('/img/no_product.png')}}" style="width:100px;height: 80px;"/>
											@endif
										</td>
									</tr>
									@php $count = $count+1; @endphp
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info" role="alert">
							<p>There is no prize right now.</p>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection
