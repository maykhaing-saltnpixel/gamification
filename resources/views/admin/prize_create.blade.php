@extends('layouts.app')

@section('title', 'Registrant List')

@section('content')

<!-- <style>
	h4{
    width:30px;
    margin-top:-10px;
    margin-left:5px;
}
</style> -->

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '5'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Create New Prize</h1>

					@include('layouts.messages')

					<!-- <div class="col-md-12" style="border: 1px solid #666;padding:20px;margin-left: 1.2%;border-radius: 10px;"> -->
					<div class="col-md-12">
						<form action="{{url('/admin/prize/store')}}" method="post" id="download_form" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="row">
								<div class="col-3">
									<select name="location" style="padding: 10px;background-color: #c9dae2;color: #495057;border: none;font-family: 'Gotham Light';">
										<option value="">Select location</option>
										<option value="SPAC (South Pacific)">SPAC (South Pacific)</option>
										<option value="SEAK (S.E.A & Korea)">SEAK (S.E.A & Korea)</option>
										<option value="China">China</option>
										<option value="India">India</option>
									</select>
									@if ($errors->has('location'))
										<span class="alert alert-danger">
											<strong>{{ $errors->first('location') }}</strong>
										</span>
									@endif
								</div>
								<div class="col-3">
									<input type="file" class="form-control" id="image" name="image">
									@if ($errors->has('image'))
										<span class="alert alert-danger">
											<strong>{{ $errors->first('image') }}</strong>
										</span>
									@endif
								</div>
								<div class="col-3">
									<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Create</button>
								</div>
							</div>					
						</form>	
					</div>
	
					<div class="spacer-30"></div>

				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#country").val('');
			$("#partner_id").val('');
			$("#search_form").submit();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
@endsection
