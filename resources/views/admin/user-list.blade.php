@extends('layouts.app')

@section('title', 'Registrant List')

@section('content')

<!-- <style>
	h4{
    width:30px;
    margin-top:-10px;
    margin-left:5px;
}
</style> -->

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '1'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Registrant List</h1>

					@include('layouts.messages')

					<!-- <div class="col-md-12" style="border: 1px solid #666;padding:20px;margin-left: 1.2%;border-radius: 10px;"> -->
					<div class="col-md-12">
							<!-- <h4 style="color: #666">Search</h4> -->

							<form action="{{url('/admin/user/search')}}" method="post" id="search_form" enctype="multipart/form-data">
							{{ csrf_field() }}
								<!-- <fieldset>
									<legend>Search</legend> -->
									<div class="row">
										<div class="col-3">
											<input type="text" class="form-control" name="partner_id" id="partner_id" value="{{$partner_id}}" placeholder="partner ID">
										</div>
										<div class="col-3">
											<select class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" id="country">
												<option value="" {{ $country == '' ? 'selected':'' }}>Select country</option>
												<option value="China (Greater China)" {{ $country == 'China (Greater China)' ? 'selected':'' }}>China (Greater China)</option>
												<option value="Hong Kong (Greater China)" {{ $country == 'Hong Kong (Greater China)' ? 'selected':'' }}>Hong Kong (Greater China)</option>
												<option value="Taiwan (Greater China)" {{ $country == 'Taiwan (Greater China)' ? 'selected':'' }}>Taiwan (Greater China)</option>
												<option value="India" {{ $country == 'India' ? 'selected':'' }}>India</option> 
												<option value="Indonesia (SEA-K)" {{ $country == 'Indonesia (SEA-K)' ? 'selected':'' }}>Indonesia (SEA-K)</option> 
												<option value="Malaysia (SEA-K)" {{ $country == 'Malaysia (SEA-K)' ? 'selected':'' }}>Malaysia (SEA-K)</option> 
												<option value="Philippines (SEA-K)" {{ $country == 'Philippines (SEA-K)' ? 'selected':'' }}>Philippines (SEA-K)</option> 
												<option value="Singapore (SEA-K)" {{ $country == 'Singapore (SEA-K)' ? 'selected':'' }}>Singapore (SEA-K)</option> 
												<option value="Thailand (SEA-K)" {{ $country == 'Thailand (SEA-K)' ? 'selected':'' }}>Thailand (SEA-K)</option> 
												<option value="Vietnam (SEA-K)" {{ $country == 'Vietnam (SEA-K)' ? 'selected':'' }}>Vietnam (SEA-K)</option>
												<option value="Korea (SEA-K)" {{ $country == 'Korea (SEA-K)' ? 'selected':'' }}>Korea (SEA-K)</option> 
												<option value="Australia (SPAC)" {{ $country == 'Australia (SPAC)' ? 'selected':'' }}>Australia (SPAC)</option> 
												<option value="New Zealand (SPAC)" {{ $country == 'New Zealand (SPAC)' ? 'selected':'' }}>New Zealand (SPAC)</option>
											</select>
										</div>
										<div class="col-1">
											<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Search</button>
										</div>
										<div class="col-1">
											<button type="button" class="btn btn-default" id="clear_btn">Clear</button>
										</div>
									</div>
											
								<!-- </fieldset> -->
							</form>		
					</div>
	
					<div class="spacer-30"></div>

					@if(count($registrant_list) > 0)
						<table class="table">
							<thead>
								<tr style="background-color: #0096D6;color: #fff;">
									<th scope="col" style="vertical-align: top;">#</th>
									<th scope="col" style="vertical-align: top;">First name</th>
									<th scope="col" style="vertical-align: top;">Last name</th>
									<th scope="col" style="vertical-align: top;">Email</th>
									<th scope="col" style="vertical-align: top;">Partner ID</th>
									<th scope="col" style="vertical-align: top;">Country</th>
									<th scope="col" style="vertical-align: top;">Company name</th>
									<th scope="col" style="vertical-align: top;">Designation</th>
									<th scope="col" style="vertical-align: top;">Points</th>
									<th scope="col" style="vertical-align: top;">Created date</th>
								</tr>
							</thead>
							<tbody style="background-color: #fff;">
								@php $count = 1; @endphp
								@foreach($registrant_list as $list)
									<tr>
										<th scope="row">{{$count}}</th>
										<td>
											<a href="{{url('/user_detail/'.$list->id)}}" style="color: #000;text-decoration: underline;">
												{{$list->first_name}}
											</a>
										</td>
										<td>{{$list->last_name}}</td>
										<td>{{$list->email}}</td>
										<td>{{$list->partner_id}}</td>
										<td>{{$list->country}}</td>
										<td>{{$list->company_name}}</td>
										<td>{{$list->designation}}</td>
										<td>
											@if($list->points == '')
												-<br>
												<i class="fa fa-question-circle" data-toggle="tooltip" title="Haven't confirm email yet!" data-placement="bottom"></i>
											@else
												{{$list->points}}
											@endif
										</td>
										<td>
											{{date('Y/m/d H:i', strtotime($list->created_at))}}
										</td>
									</tr>
									@php $count = $count+1; @endphp
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info" role="alert">
							<p>There is no registrant right now.</p>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#country").val('');
			$("#partner_id").val('');
			$("#search_form").submit();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
@endsection
