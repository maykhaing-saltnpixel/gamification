<div class="left-menu">
	<nav id="sidebar">
		<div class="sidebar-header text-center">
			<a href="#">
				<img src="{{url('/img/hp.png')}}" class="img-responsive" style="width: 40%;height: auto;">
			</a>
		</div>

		<div class="custom-menu-wrapper">
			<ul class="list-unstyled components">
				<li class="{{ ($page and $page == '1') ? 'li_active' : '' }}">
					<a href="{{url('/register/user_list')}}">
						Registrant List
					</a>
				</li>
				<li class="{{ ($page and $page == '2') ? 'li_active' : '' }}">
					<a href="{{url('/game')}}">
						Game
					</a>
				</li>
				<li class="{{ ($page and $page == '3') ? 'li_active' : '' }}">
					<a href="{{url('/ranking')}}">
						Ranking
					</a>
				</li>
				<li class="{{ ($page and $page == '4') ? 'li_active' : '' }}">
					<a href="{{url('/admin/report')}}">
						Report
					</a>
				</li>
				<li class="{{ ($page and $page == '5') ? 'li_active' : '' }}">
					<a href="{{url('/admin/login_attempt')}}">
						Login Attempt List
					</a>
				</li>
				<li class="{{ ($page and $page == '6') ? 'li_active' : '' }}">
					<a href="{{url('/admin/game_unfinished')}}">
						Game Unfinished List
					</a>
				</li>

				<li>
				    <a href="{{url('/admin/logout')}}" >
				        Logout
				    </a>
				</li>
			</ul>
		</div>
	</nav>
</div>
