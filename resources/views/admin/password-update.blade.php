@extends('layouts.app')

@section('title', 'Reset Your Password')

@section('content')

<div  class="container">
    <h3>Reset Your Password</h3>
    <hr>
    @include('layouts.messages')
    <form action="{{url('/admin/reset/password')}}" class="needs-validation" method="post" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}

        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @else
                <div class="invalid-feedback">
                  Please enter a password.
                </div>
            @endif
        </div>

        <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" id="confirm_password" required>
            @if ($errors->has('confirm_password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('confirm_password') }}</strong>
                </span>
            @else
                <div class="invalid-feedback">
                    Please enter a password that match with password field.
                </div>
            @endif
        </div>

        <input type="hidden" name="sec_code" value="{{$sec_code}}">

        <div class="spacer-20"></div>

        <button type="submit" id="submit_btn" class="btn btn-primary">Submit</button>
    </form>
</div>

<script type="text/javascript">
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection