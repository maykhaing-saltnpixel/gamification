@extends('layouts.app')

@section('title', 'Registrant List')

@section('content')

<style>
	h4{
    width:30px;
    margin-top:-10px;
    margin-left:5px;
}
</style>

	<div class="page-new">
		<div class="main-row">
			<div class="main-left">

				@include('admin.inc.left-menu', ['page' => '4'])
			</div>

			<div class="main-right">
				<div class="container-fluid">

					<h1 class="main-title">Registrant Report</h1>

					@include('layouts.messages')

					<div class="spacer-20"></div>
					<!-- <div class="col-md-12" style="border: 1px solid #666;padding:20px;margin-left: 1.2%;border-radius: 10px;"> -->
					<div class="col-md-12">
						<form action="{{url('/admin/user/download')}}" method="post" id="download_form" enctype="multipart\form-data">
							{{ csrf_field() }}
							<!-- <fieldset>
								<legend>Download</legend> -->
								<div class="row">
									<div class="col-3">
										<select class="form-control {{ $errors->has('game') ? ' is-invalid' : '' }}" name="game" id="game">
											<option value="">Select game</option>
											@foreach($games as $game)
												<option value="{{$game->id}}">{{$game->game_title}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-3">
										<select class="form-control {{ $errors->has('download_country') ? ' is-invalid' : '' }}" name="download_country" id="download_country">
											<option value="" {{ $download_country == '' ? 'selected':'' }}>Select country</option>
											<option value="All" {{ $download_country == 'All' ? 'selected':'' }}>All</option>
											<option value="China (Greater China)" {{ $download_country == 'China (Greater China)' ? 'selected':'' }}>China (Greater China)</option>
											<option value="Hong Kong (Greater China)" {{ $download_country == 'Hong Kong (Greater China)' ? 'selected':'' }}>Hong Kong (Greater China)</option>
											<option value="Taiwan (Greater China)" {{ $download_country == 'Taiwan (Greater China)' ? 'selected':'' }}>Taiwan (Greater China)</option>
											<option value="India" {{ $download_country == 'India' ? 'selected':'' }}>India</option> 
											<option value="Indonesia (SEA-K)" {{ $download_country == 'Indonesia (SEA-K)' ? 'selected':'' }}>Indonesia (SEA-K)</option> 
											<option value="Malaysia (SEA-K)" {{ $download_country == 'Malaysia (SEA-K)' ? 'selected':'' }}>Malaysia (SEA-K)</option> 
											<option value="Philippines (SEA-K)" {{ $download_country == 'Philippines (SEA-K)' ? 'selected':'' }}>Philippines (SEA-K)</option> 
											<option value="Singapore (SEA-K)" {{ $download_country == 'Singapore (SEA-K)' ? 'selected':'' }}>Singapore (SEA-K)</option> 
											<option value="Thailand (SEA-K)" {{ $download_country == 'Thailand (SEA-K)' ? 'selected':'' }}>Thailand (SEA-K)</option> 
											<option value="Vietnam (SEA-K)" {{ $download_country == 'Vietnam (SEA-K)' ? 'selected':'' }}>Vietnam (SEA-K)</option>
											<option value="Korea (SEA-K)" {{ $download_country == 'Korea (SEA-K)' ? 'selected':'' }}>Korea (SEA-K)</option> 
											<option value="Australia (SPAC)" {{ $download_country == 'Australia (SPAC)' ? 'selected':'' }}>Australia (SPAC)</option> 
											<option value="New Zealand (SPAC)" {{ $download_country == 'New Zealand (SPAC)' ? 'selected':'' }}>New Zealand (SPAC)</option>
										</select>
									</div>
									<span class="error">{{Session::get('data')}}</span>
									<div class="col-1">
										<button type="submit" class="btn btn-default" style="background-color: #0096D6;color: #fff;">Download</button>
									</div>
									<div class="col-1">
										<button type="button" class="btn btn-default" id="clear_btn">Clear</button>
									</div>
								</div>
							<!-- </fieldset> -->
						</form>
							
					</div>		

	
					<div class="spacer-30"></div>

				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$("#clear_btn").click(function() {
			$("#country").val('');
			$("#game").val('');
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
@endsection
