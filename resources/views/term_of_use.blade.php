@extends('layouts.app')

@section('title', 'Terms of Use')

@section('content')

@include('layouts.header')

<style type="text/css">
	ol.c {list-style-type: number;}
</style>

<div class="spacer-30"></div>

<div class="row" style="margin-right: 0px;margin-left: 0px;">
	<div class="col-md-2 col-sm-2"></div>
	<div class="col-md-8 col-sm-8 d-none d-sm-block">
		<img src="{{url('/img/bg.png')}}" style="width: 100%;height: 42px;">
		<div class="bottom-left">
			<a href="{{url('/')}}" style="text-decoration: none;">
				<i class="fa fa-home" style="color: #fff;vertical-align: -webkit-baseline-middle;"></i>
			</a>
		</div>
		<div class="bottom-left2">
			<a href="{{url('/term_of_use')}}" style="text-decoration: none;">
				<span class="bread_tu_qui_txt">Terms & Conditions</span>
			</a>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 d-block d-sm-none">
		<a href="{{url('/')}}" style="text-decoration: none;">
			<i class="fa fa-home" style="color: #1ab6f1;"></i>
		</a>
		>
		<span><a href="{{url('/login')}}">{{__('messages.login')}}</a> / <a href="{{url('/register')}}">{{ __('messages.register') }}</a></span>
	</div>
	<div class="col-md-2 col-sm-2"></div>
</div>
<div class="spacer-20"></div>

<div class="row" style="width: 100%;margin-left: 0px;">
	<div class="col-md-3" style="padding: 0px;"></div>
	<div class="col-md-6" style="padding: 0px;">
		<h3 style="text-transform: uppercase;font-family: 'Gotham Medium';font-size: 30px;">Terms & Conditions</h3>
		@include('layouts.messages')
		<div class="spacer-5"></div>
		<div class="row" style="background-color:#e9f2f7;padding: 10px;width: 100%;margin-left: 0px;">
			<div class="col-12" style="border: 1px solid #fff">
				<div style="padding: 35px 15px;">
					<h5 style="font-family: 'Gotham Medium';font-size: 16px;color: #333;">HP Vibrant Planet Rally Terms and Conditions</h5>
					<div class="row" style="font-family: 'Gotham Light';font-size: 14px;color: #666">
						<ol class="c">
							<li>Participation in the <b>HP Vibrant Planet Rally</b> ("contest") indicates that the participant has read, understood and agreed to comply with the Terms and Conditions of this contest.</li>
							<li>The contest is only open to participants in APJ. Prizes will only be delivered to addresses within APJ.</li>
							<li>Only HP Channel Partners are eligible for this contest.</li>
							<li>There may be multiple contest periods as provided by HP APJ (“Organizers”). Actual number of contests is the sole discretion of the Organizers.</li>
							<li>All entries must comply with all directions and regulations of the Organizers in order to be considered for the contest, including but not limited to the following:</li>
							<li>The winners of the contest may be awarded a prize from HP as stated on the website, or, if unavailable, a prize of equivalent value (the "prize"), subject to the discretion of the Organizers.</li>
							<li>Organizers reserve the right to select winners based on additional, undisclosed and/or discretional criteria, including the limitation of the number of winners from the same company. The Organizers expressly reserve the right to exclude entries deemed inappropriate for the purpose of the competition. The Organiser's decision shall be final and is not subject to appeal.</li>
							<li>Participants may not register/enter the contest with multiple email addresses and/or accounts nor may entrants use any other device or artifice to register/enter under multiple identities. Any participant who attempts to enter with multiple email addresses under multiple identities will be disqualified and forfeits any prize won, at the Organizer's discretion. Only official company email address are allowed for the registration to be valid.</li>
							<li>All winners will be notified by email, within 14 working days of the end the <b>HP Vibrant Planet Rally</b> Game.</li>
							<li>The end-user will assume all liability in case of any injury, damage or claim resulting from use of the free gift/prizes.</li>
							<li>For the avoidance of doubt, in no event shall the Organizers, its directors and employees, or any third party service providers engaged by the Organizers for purposes of the redemption or contest, be liable for any loss or damage (including without limitation loss of income, profits or goodwill, indirect, incidental, consequential, exemplary, punitive or special damages of any party including third parties) howsoever arising whether in contract, tort, negligence or otherwise, in connection with the program and free gift/prizes.</li>
							<li>The Organizers reserve the right to extend the "<b>HP Vibrant Planet Rally</b>" contest(s) at any time throughout the planned duration without prior notice.</li>
							<li>The Organizers shall not be liable for any loss or damage should the Organizers extend or end the "<b>HP Vibrant Planet Rally</b>" contest(s).</li>
							<li>By participating in the "<b>HP Vibrant Planet Rally</b>" contest, participants fully and unconditionally agree to and accept these terms and conditions.</li>
							<li>Information collected from this campaign will be kept confidential and will be used for the purposes of this program or audit.</li>
							<li>By agreeing to these terms and conditions, each participant consents to and authorizes the Organizers to contact them with regards to the contest.</li>
							<li>The winners may also be required to attend prize presentations and/or participate in publicity programmes if deemed necessary. Personal expenses and/or costs incurred for such attendance will be borne by the winners.</li>
							<li>The Organizers are not responsible or liable for any problem, delay, loss, or damage as a result of any postal delay or other unforeseen circumstances and events not within control of the Organizers.</li>
							<li>The terms and conditions stated herein shall prevail over any provisions, representations or statements found in any advertising materials in relation to this programme.</li>
							<li>The terms and conditions stated herein shall prevail over any provisions, representations or statements found in any advertising materials in relation to this programme.</li>
							<li>Any term or condition, or the application of any term or condition, which is or found to be void, illegal or unenforceable shall not impair or affect the validity, legality or enforceability of the other remaining terms and conditions.</li>
							<li>The Organizers’ decision will be deemed as final. No appeals or correspondences will be entertained.</li>
							<li>All gifts/prizes are non-transferable, non-refundable and non-exchangeable for cash or for any other gifts/prizes.</li>
							<li>The Organizers are not responsible for any defects or other issues relating to the gifts/prizes. All issues will be considered by the Organizers on a case to case basis.</li>
							<li>The Organizers reserve the right to change the above Term and Conditions, should it be deemed necessary, without prior notice.</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="row hpcopyright">
			<div class="col-md-12">
				<div class="row d-none d-sm-block">
					<div class="col-12 " style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
						<div class="row">
							<div class="col-sm-8 col-md-8 footer_div">
								<span>{{__('messages.copy_right')}}</span>
							</div>
							<div class="col-sm-4 col-md-4 footer_div text-right">
								<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-sm-none" style="height: 35px;background-image: linear-gradient(to bottom, #e9f2f7, #f0f5fa, #f6f8fc, #fbfbfd, #ffffff);padding: 30px;">
					<div class="col-sm-8 col-md-8 footer_div">
						<span>{{__('messages.copy_right')}}</span>
					</div>
					<div class="col-sm-4 col-md-4 footer_div">
						<a href="{{url('/term_of_use')}}" style="text-decoration: underline;color: #000;">{{__('messages.term_condi')}}</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3" style="padding: 0px;"></div>
</div>

<div class="spacer-100"></div>

@endsection