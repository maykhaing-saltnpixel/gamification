@extends('layouts.app')

@section('title', 'Participants_info')

@section('content')

@include('layouts.header')

<div class="participant_info_china">
	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<div class="">
				<img src="{{url('/img/bg.png')}}" class="w-100" style="position: relative;right: 4%;">
				<span><i class="fas fa-home" style="position: absolute;left: -2%;
				top: 27%;"></i></span>
				<span style="position: absolute;left: 5%;top: 16%;font-size: 14px;">
					<a href="{{url('/term_of_use')}}" style="text-decoration: none;color: #000;">Participant info.</a></span>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
	<div class="spacer-50"></div>

	<div class="participantinfo_section d-none d-sm-block">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<h3>Participant Info</h3>
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-3 text-center" style="padding: 0px;background: #1ab4f0;">
						<img src="{{url('/img/participant-04.png')}}" class="participant_img1 mx-auto d-block">
						<span class="participant_name">Xin Ni</span>
						<span class="participant_nation">CHINA</span>
						<span class="participant_partnet">PARTNET ID - C5818P</span>

						<div class="text-center">
							<button class="btn participant_btn1">EDIT PROFILE</button>
						</div>	
						
						<div class="">
							<img class="points_nation" src="{{url('/img/participant_separate-line.png')}}">
							<span class="participant_point1">1875</span>
							<span class="participant_point2">POINTS</span>
						</div>

						<div class="">
							<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
							<span class="participant_rankingnation1">17</span>
							<span class="participant_rankingnation2">RANKING IN CHINA</span>
							<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
						</div>
					</div>
					<div class="col-sm-9" style="padding: 0px;">
						<div class="participant_section1">
							<img src="{{url('/img/participant.jpg')}}" class="participant_img4 w-100">
						</div>	
						<div class="participant_section2">
							<img src="{{url('/img/achivement_bg.png')}}" align="right">
							<span class="participant_achivement">Achivement</span>
						</div>
							
						<div class="achivement col-sm-12">
							
							<div class="row">
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-01.png')}}" class="mx-auto d-block">
									<span>First 500 Registant</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-02.png')}}" class="mx-auto d-block">
									<span>Completed week 1</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 2</span>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4"></div>
								<div class="col-sm-4 text-center">
									
									<button type="button" class="btn" data-toggle="collapse" data-target="#demo">VIEW MORE</button>
									
								</div>
								<div class="col-sm-4"></div>
							</div>
								
							<div id="demo" class="row collapse">
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 3</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 4</span>
								</div>
							</div>
							<div class="spacer-20"></div>
						</div>

						<div class="prizes_for_nation">
							<div class="row" style="width: 100%;margin-left: 0px;">
								<div class="col-sm-8" style="padding: 0px;">
									<div class="">
										<img class="participant_img2" src="{{url('/img/prize_bg.png')}}" align="left">
										<span class="prizes_china">Prizes For China</span>
									</div>
									<div class="spacer-10"></div>
									<div class="text-center">
										<img class="participant_img3" src="{{url('/img/participant-06.png')}}" class="mx-auto d-block">
									</div>
									<div class="winnernation">
										<div class="row" style="width: 100%;margin-left: 0px;">
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/1st.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">1400</span>
														<span class="winner2">1WINNER</span>
													</div>
												</div>
											</div>
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/2nd.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">700</span>
														<span class="winner2">3WINNERS</span>
													</div>
												</div>
											</div>
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/3rd.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">350</span>
														<span class="winner2">10WINNERS</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4" style="padding: 0px;">
									<img class="participant_img4" src="{{url('/img/participant-05.jpg')}}">
								</div>
							</div>
						</div>
					</div>				
				</div>			
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
	</div>

	<div class="participantinfo_mobilesection  d-block d-sm-none">
		<div class="row" style="width: 100%;margin-left: 0px;">
			<div class="col-md-3" style="padding: 0px;"></div>
			<div class="col-md-6" style="padding: 0px;">
				<h3>Participant Info</h3>
				<div class="row" style="width: 100%;margin-left: 0px;">
					<div class="col-sm-3 text-center" style="padding: 0px;background: #1ab4f0;">
						<img src="{{url('/img/participant-04.png')}}" class="participant_img1 mx-auto d-block">
						<span class="participant_name">Xin Ni</span>
						<span class="participant_nation">CHINA</span>
						<span class="participant_partnet">PARTNET ID - C5818P</span>

						<div class="text-center">
							<button class="btn participant_btn1">EDIT PROFILE</button>
						</div>	
						
						<div class="">
							<img class="points_nation" src="{{url('/img/participant_separate-line.png')}}">
							<span class="participant_point1">1875</span>
							<span class="participant_point2">POINTS</span>
						</div>

						<div class="">
							<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
							<span class="participant_rankingnation1">17</span>
							<span class="participant_rankingnation2">RANKING IN CHINA</span>
							<img class="rankings_nation" src="{{url('/img/participant_separate-line.png')}}">
						</div>
					</div>
					<div class="col-sm-9" style="padding: 0px;">
						<div class="participant_section1">
							<img src="{{url('/img/participant.jpg')}}" class="participant_img4 w-100">
						</div>	
						<div class="participant_section2">
							<img src="{{url('/img/achivement_bg.png')}}" align="right">
							<span class="participant_achivement">Achivement</span>
						</div>
							
						<div class="achivement col-sm-12">
							
							<div class="row">
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-01.png')}}" class="mx-auto d-block">
									<span>First 500 Registant</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-02.png')}}" class="mx-auto d-block">
									<span>Completed week 1</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 2</span>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4"></div>
								<div class="col-sm-4 text-center">
									
									<button type="button" class="btn" data-toggle="collapse" data-target="#demo">VIEW MORE</button>
									
								</div>
								<div class="col-sm-4"></div>
							</div>
								
							<div id="demo" class="row collapse">
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 3</span>
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{url('/img/participant-03.png')}}" class="mx-auto d-block">
									<span>Completed week 4</span>
								</div>
							</div>
							<div class="spacer-20"></div>
						</div>

						<div class="prizes_for_nation">
							<div class="row" style="width: 100%;margin-left: 0px;">
								<div class="col-xs-6" style="padding: 0px;">
									<div class="">
										<img class="participant_img2" src="{{url('/img/prize_bg.png')}}" align="left">
										<span class="prizes_china">Prizes For China</span>
									</div>
									<div class="spacer-10"></div>
									<div class="text-center">
										<img class="participant_img3" src="{{url('/img/participant-06.png')}}" class="mx-auto d-block">
									</div>
									<div class="winnernation">
										<div class="row" style="width: 100%;margin-left: 0px;">
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/1st.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">1400</span>
														<span class="winner2">1WINNER</span>
													</div>
												</div>
											</div>
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/2nd.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">700</span>
														<span class="winner2">3WINNERS</span>
													</div>
												</div>
											</div>
											<div class="col-sm-4" style="padding: 0px;">
												<div class="row" class="row" style="width: 100%;margin-left: 0px;">
													<div class="col-sm-6" style="padding: 0px;">
														<img src="{{url('/img/3rd.png')}}">
													</div>
													<div class="col-sm-6" style="padding: 0px;">
														<span class="winner1">350</span>
														<span class="winner2">10WINNERS</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-6" style="padding: 0px;">
									<img class="participant_img4" src="{{url('/img/participant-05.jpg')}}">
								</div>
							</div>
						</div>
					</div>				
				</div>			
			</div>
			<div class="col-md-3" style="padding: 0px;"></div>
		</div>
	</div>
	

	<div class="row" style="width: 100%;margin-left: 0px;">
		<div class="col-md-3" style="padding: 0px;"></div>
		<div class="col-md-6" style="padding: 0px;">
			<div class="row hpcopyright_text" style="width: 100%;margin-left: 0px;">
				<div class="col-sm-6" style="padding: 0px;">
					<span>{{ __('messages.copy_right') }}</span>
				</div>
				<div class="col-sm-6 text-right" style="padding: 0px;">
					<a href="{{url('/term_of_use')}}">
						<span>{{ __('messages.term_condi') }}</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-3" style="padding: 0px;"></div>
	</div>
	<div class="spacer-100"></div>
</div>

@endsection