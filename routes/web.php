<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/',
	'HomeController@home'
);

// Register
Route::get('/register',
	'LoginController@register'
);
Route::post('/register',
	'LoginController@store'
);
Route::get('/user/confirm-email/{sec_code}',
	'LoginController@confirm_email'
);

// user login
Route::get('/login',
	'LoginController@login'
);
Route::post('/login_check',
	'LoginController@login_check'
);

//user forget password
Route::get('/forget_password',
	'LoginController@forget_password'
);
Route::get('/term_of_use',
	'LoginController@term_of_use'
);

//participants info 
Route::get('/participants_info',
	'AccountController@participants_info'
);

// ADMIN
Route::get('/admin/login',
	'Admin\LoginController@login'
);
Route::post('/admin/login',
	'Admin\LoginController@login_check'
);
Route::get('/admin/logout',
	'Admin\AccountController@logout'
);
Route::get('/admin/forget',
	'Admin\LoginController@forget_password'
);
Route::post('/admin/forget',
	'Admin\LoginController@password_reset_link'
);
Route::get('/admin/password/reset/{sec_code}',
	'Admin\LoginController@password_reset'
);
Route::post('/admin/reset/password',
	'Admin\LoginController@password_update'
);

// admin registrant list
Route::get('/register/user_list',
	'Admin\RegistrantController@list'
);
Route::any('/admin/user/search',
	'Admin\RegistrantController@search_registrant'
);
Route::get('/user_detail/{user_id}',
	'Admin\RegistrantController@user_detail'
);

// admin game
Route::get('/game',
	'Admin\GameController@index'
);
Route::get('/create/game/title',
	'Admin\GameController@create_title'
);
Route::post('/create/game/title',
	'Admin\GameController@store_game_title'
);
Route::get('/game/edit/{game_id}',
	'Admin\GameController@edit_game'
);
Route::post('/update/game/title',
	'Admin\GameController@update_game'
);
Route::get('/game/tuto/add/{game_id}',
	'Admin\GameController@add_game_qa'
);
Route::get('/game/tuto/edit/{game_id}',
	'Admin\GameController@edit_game_tuto'
);
Route::post('/tuto_update',
	'Admin\GameController@updateTuto'
);
Route::post('/store',
	'Admin\GameController@storeTuto'
);
Route::get('/game/quiz/add/{game_id}',
	'Admin\GameController@add_quiz'
);
Route::get('/game/quiz/cn_add/{game_id}',
	'Admin\GameController@add_cn_quiz'
);
Route::post('/game/quiz/store',
	'Admin\GameController@store_quiz'
);
Route::post('/game/cn_quiz/store',
	'Admin\GameController@store_cn_quiz'
);
Route::get('/game/quiz/edit/{game_id}',
	'Admin\GameController@edit_quiz'
);
Route::get('/game/quiz/cn_edit/{game_id}',
	'Admin\GameController@edit_cn_quiz'
);
Route::post('/game/quiz/update',
	'Admin\GameController@update_quiz'
);
Route::get('/game/unplubish/{game_id}',
	'Admin\GameController@unplubish_game'	
);

// admin ranking
Route::get('/ranking',
	'Admin\RankController@ranking_list'
);

Route::get('/home',
	'HomeController@home'
);
Route::get('/user/home',
	'HomeController@user_home'
);
Route::get('/tutorial_quiz',
	'TutorialQuizzController@game_schedule'
);
Route::get('/game/start/{game_id}',
	'GameStartController@game_start'
);
Route::get('/start_quiz/{game_id}',
	'GameStartController@quiz_start'
);
Route::get('/start_cn_quiz/{game_id}',
	'GameStartController@quiz_cn_start'
);
Route::post('/quiz_start/next',
	'GameStartController@next_quiz'
);
Route::get('/start_quiz/{game_id}/next_q/{question_id}',
	'GameStartController@next_question'
);
Route::post('/start_quiz/wrong/next',
	'GameStartController@wrong_question'
);
Route::post('/start_quiz/save',
	'GameStartController@ans_checkbox_save'
);
Route::get('/start_quiz/{game_id}/next_que/{question_id}',
	'GameStartController@next_que'
);
Route::get('/result/{game_id}',
	'GameStartController@result'
);
Route::get('/logout',
	'AccountController@logout'
);
Route::get('/knowledge_meter',
	'GameStartController@knowledge_meter'
);

Route::post('/admin/user/download',
	'Admin\UserController@download'
);
Route::post('/rank_search',
	'GameStartController@rank_search'
);
Route::get('/profile_edit',
	'AccountController@profile_edit'
);
Route::post('/profile_update',
	'AccountController@profile_update'
);
Route::post('/profile_remove',
 	'AccountController@image_remove'
);

Route::get('/admin/report',
	'Admin\UserController@report'
);

Route::get('/prize',
	'Admin\UserController@prize'
);

Route::get('/admin/prize/create',
	'Admin\UserController@create'
);

Route::post('/admin/prize/store',
	'Admin\UserController@store'
);

Route::post('/picture_update',
	'AccountController@picture_update'
);
Route::post('/password_update',
	'AccountController@password_update'
);
Route::post('/forget_password',
	'LoginController@forget_email_send'
);
Route::get('/user/forget_password/{sec_code}',
	'LoginController@forget_password_view'
);
Route::post('/update_password',
	'LoginController@update_password'
);

// Admin Login Attempt List
Route::get('/admin/login_attempt',
	'Admin\TrackingController@login_attempt'
);
Route::post('/admin/login_month/search',
	'Admin\TrackingController@login_attempt_search'
);
Route::get('/admin/game_unfinished',
	'Admin\TrackingController@game_unfinished'
);
Route::post('/admin/unfinish/search',
	'Admin\TrackingController@unfinish_search'
);

Route::get('/admin/eDMSend',
	'Admin\AccountController@eDMSend'
);

Route::get('/portfolio',
	'HomeController@portfolio'
);