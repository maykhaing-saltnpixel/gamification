<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Game;
use App;

class HomeController extends Controller
{
    public function __construct()
    {
        $locale = substr(url()->current(), 7, 2);

        if ($locale == 'cn') {
            App::setLocale('cn');
            $this->locale = 'cn';
        } else {
            App::setLocale('en');
            $this->locale = 'en';
            // App::setLocale('cn');
            // $this->locale = 'cn';
        }
    }
    public function home(Request $request)
    {
        $date = date('Y/m/d');

		$game_list = Game::where('delete_flg',0)
                            ->where('from_date','LIKE','%'.$date.'%')
                            ->where('to_date','LIKE','%'.$date.'%')
                            ->orderBy('id','asc')
                            ->first();

    	return view('home')->with('game_list',$game_list);
    }

    public function user_home($value='')
    {
    	echo "coming soon :)";
    }

    public function portfolio()
    {
        return view('portfolio');
    }
}
