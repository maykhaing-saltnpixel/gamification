<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserAnswer;
use App\Models\Game;
use DB;
use App;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.user');

        $locale = substr(url()->current(), 7, 2);

        if ($locale == 'cn') {
            App::setLocale('cn');
            $this->locale = 'cn';
        } else {
            App::setLocale('en');
            $this->locale = 'en';
            // App::setLocale('cn');
            // $this->locale = 'cn';
        }
    }

    public function logout()
	{
        Auth::logout();
		return redirect('/');
	}

	public function profile_edit()
	{
		$user_data = User::find(Auth::id());
		$ranking_list = User::where('delete_flg',0)->where('user_type',1)->orderBy('points','desc')->paginate(5);
    	$current_user_rank = User::where('delete_flg',0)
    								->where('user_type',1)
    								->where('id',auth()->user()->id)
    								->orderBy('points','desc')
    								->first();
		if($user_data){
			return view('profile_edit',compact('user_data','ranking_list','current_user_rank'));	
		}else{
			abort(404);
		}
		
	}

	public function profile_update(Request $request)
	{
		$profile=$request->file('profile');
		$user = User::find(Auth::id());
		if($user!=null){
			$old_image = $user->profile;
			$email = $user->email;
			$gender = $user->gender;
			$company_name = $user->company_name;
			$designation = $user->designation;
			$first_name = $user->first_name;
			$last_name = $user->last_name;
			$country = $user->country;
		}

	 	if($profile != null){
	 		$path = $request->file('profile')->store('profile');
	 	}

		$user->first_name = $request->first_name ?? $first_name;
		$user->last_name = $request->last_name ?? $last_name;
		$user->country = $request->country ?? $country;
		$user->email = $request->email ?? $email;
		$user->company_name = $request->company_name ?? $company_name;
		if ($request->input('designation') == 'Others') {
            $user->designation = $request->other_designation ?? $designation;
        } else {
            $user->designation = $request->designation ?? $designation;
        }
		$user->gender = $request->gender ?? $gender;
     	$user->profile = $path ?? $old_image;
     	$user->save();
		return redirect()->back()->with('success','Your profile has been updated successfully.');

	}

	public function participants_info()
    {
    	$user_participant = UserAnswer::select('game_id','user_id')
    									->where('user_id',auth()->user()->id)
    									->distinct()
    									->get();

    	// $game_list = Game::select('id')->where('delete_flg',0)->orderBy('id','asc')->get();
        // $game_list = Game::select('games.id','user_answers.user_id','user_answers.game_id')
        //                     ->leftJoin('user_answers','user_answers.game_id','=','games.id')
        //                     ->where('games.delete_flg',0)
        //                     // ->orderBy('games.id','asc')
        //                     ->where('user_answers.user_id',auth()->user()->id)
        //                     ->distinct()
        //                     ->get();

        $current_answer_list = UserAnswer::select('game_id')->where('user_id',auth()->user()->id)->distinct()->orderBy('game_id')->get();

        $cur_ans = array();
        foreach ($current_answer_list as $current) {
            array_push($cur_ans, $current->game_id);
        }

        // dd($cur_ans);

        $game_list = Game::select('games.*')
                                ->where('games.delete_flg',0)
                                // ->orderBy('games.id')
                                ->get();
        // dd($game_list);

        $today = Date('Y/m/d');
        $end_games = Game::where('to_date','<',$today)->get();
        $id = array();
        foreach($game_list as $game){
            
        }
        // dd($id);
    	if (auth()->user()->country == 'Australia (SPAC)' || auth()->user()->country == 'New Zealand (SPAC)') {
            $ranking_list = User::where('delete_flg',0)
                                ->where('user_type',1)
                                ->where('country','Australia (SPAC)')
                                ->orWhere('country','New Zealand (SPAC)')
                                ->orderBy('points','desc')
                                ->get();
        }elseif (auth()->user()->country == 'Indonesia (SEA-K)' || auth()->user()->country == 'Malaysia (SEA-K)' || auth()->user()->country == 'Thailand (SEA-K)') {
            $ranking_list = User::where('delete_flg',0)
                        ->where('user_type',1)
                        ->where('country','Indonesia (SEA-K)')
                        ->orWhere('country','Malaysia (SEA-K)')
                        ->orWhere('country','Philippines (SEA-K)')
                        ->orWhere('country','Singapore (SEA-K)')
                        ->orWhere('country','Thailand (SEA-K)')
                        ->orWhere('country','Vietnam (SEA-K)')
                        ->orWhere('country','Korea (SEA-K)')
                        ->orderBy('points','desc')
                        ->get();
        } elseif (auth()->user()->country == 'Philippines (SEA-K)' || auth()->user()->country == 'Singapore (SEA-K)' || auth()->user()->country == 'Vietnam (SEA-K)' || auth()->user()->country == 'Korea (SEA-K)') {
            $ranking_list = User::where('delete_flg',0)
                        ->where('user_type',1)
                        ->where('country','Indonesia (SEA-K)')
                        ->orWhere('country','Malaysia (SEA-K)')
                        ->orWhere('country','Philippines (SEA-K)')
                        ->orWhere('country','Singapore (SEA-K)')
                        ->orWhere('country','Thailand (SEA-K)')
                        ->orWhere('country','Vietnam (SEA-K)')
                        ->orWhere('country','Korea (SEA-K)')
                        ->orderBy('points','desc')
                        ->get();
        } elseif (auth()->user()->country == 'China (Greater China)' || auth()->user()->country == 'Hong Kong (Greater China)' || auth()->user()->country == 'Taiwan (Greater China)') {
            $ranking_list = User::where('delete_flg',0)
                        ->where('user_type',1)
                        ->where('country','China (Greater China)')
                        ->orWhere('country','Hong Kong (Greater China)')
                        ->orWhere('country','Taiwan (Greater China)')
                        ->orderBy('points','desc')
                        ->get();
        }elseif (auth()->user()->country == 'India') {
            $ranking_list = User::where('delete_flg',0)
                        ->where('user_type',1)
                        ->where('country','India')
                        ->orderBy('points','desc')
                        ->get();
        }
    	$current_user_rank = User::where('delete_flg',0)
    								->where('user_type',1)
    								->where('id',auth()->user()->id)
    								->orderBy('points','desc')
    								->first();

        return view('participants_info')->with('user_participant',$user_participant)
        								->with('game_list',$game_list)
        								->with('ranking_list',$ranking_list)
        								->with('current_user_rank',$current_user_rank)
                                        ->with('game_list',$game_list)
                                        ->with('cur_ans',$cur_ans)
                                        ->with('end_games',$end_games);
    }

    public function picture_update(Request $request)
    {
    	$user = User::find(Auth::id());
		$profile=$request->file('profile');

		if($user!=null){
			$old_image = $user->profile;
		}
		
	 	if($profile != null){
	 		$path = $request->file('profile')->store('profile');
	 	}
		 	
     	$user->profile = $path ?? $old_image;
     	$user->save();
		return redirect()->back()->with('success','Your profile picture has been updated successfully.');
    }

    public function password_update(Request $request)
    {
    	$this->validate($request, [
            'password' => 'min:4',
            'confirm_password' => 'same:password',
        ]);
    	$user = User::find(Auth::id());

		if($user!=null){
			$old_password = $user->password;
		}
		 	
     	$user->password = Hash::make($request->password) ?? $old_password;
     	$user->save();
		return redirect()->back()->with('success','Your password has been updated successfully.');
    }
}
