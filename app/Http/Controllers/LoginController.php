<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Partner;
use App\Models\LoginAttempt;
use App\Mail\EmailConfirm;
use App\Mail\ChineseEmailConfirm;
use App\Mail\ForgetPassword;
use App;

class LoginController extends Controller
{
    public function __construct($value='')
    {
        $locale = substr(url()->current(), 7, 2);
        
        if ($locale == 'cn') {
            App::setLocale('cn');
            $this->locale = 'cn';
        } else {
            app()->setLocale($locale);
            // App::setLocale('cn');
            $this->locale = 'en';
            // $this->locale = 'cn';
        }
    }
    public function register()
    {
		$partner_list = Partner::select('partner_id')->get();
		return view('register')->with('partner_list',$partner_list);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'designation' => 'required',
            'partner_id' => 'required',
            'email' => 'email|unique:users|required',
            'gender' => 'required',
            'company_name' => 'required',
            'password' => 'required|min:4',
            'confirm_password' => 'same:password',
        ]);

        $partner_id = Partner::where('partner_id',$request->input('partner_id'))->get();
        if (count($partner_id) == 0) {
            return redirect()->back()
                ->with('error', "The partnerID wasn't valid.")
                ->with('message','register');
        } else {
            $user_name = $request->input('first_name') . ' ' . $request->input('last_name');

            $sec_code = str_random(40);

            if ($this->locale == 'cn') {
                Mail::to($request->input('email'))->send(new ChineseEmailConfirm($sec_code,$user_name));
            } else {
                Mail::to($request->input('email'))->send(new EmailConfirm($sec_code,$user_name));
            }

            $user_position = User::select('registrant_position')->where('user_type',1)->orderBy('id','desc')->first();

            if ($user_position == '') {
                $position = 1;
            } else {
                $position = (int)$user_position->registrant_position + 1;
            }

            if ($position <= 500) {
                $first_fivehundred = 0;
            } else {
                $first_fivehundred = 1;
            }

            $user = new User;
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->gender = $request->input('gender');
            $user->email = $request->input('email');
            $user->partner_id = $request->input('partner_id');
            $user->country = $request->input('country');
            $user->company_name = $request->input('company_name');
            if ($request->input('designation') == 'Others') {
                $user->designation = $request->input('other_designation');
            } else {
                $user->designation = $request->input('designation');
            }
            $user->character = $request->input('character') ?? '';
            $user->password = Hash::make($request->input('password'));
            $user->first_registrant_flg = $first_fivehundred;
            $user->registrant_position = $position;
            $user->user_type = 1;
            $user->sec_code = $sec_code;
            $user->delete_flg = 0;
            $user->save();

            if ($this->locale == 'cn') {
                $message = '谢谢！我们已收到您的注册信息。请点击电子邮件中的确认链接来确认注册。';
            } else {
                $message = 'Thank you. We have received your registration. Please check your email to confirm your registration  by clicking on the confirmation link.';
            }
            
            return redirect('/register')
                    ->with('info', $message)
                    ->with('message','register');
        }
    }

    public function confirm_email($sec_code)
    {
        $user = User::where([
                    ['sec_code', $sec_code],
                ])->first();

        if ($user) {
            $user->sec_code = '';
            $user->points = '20';
            $user->delete_flg = 0;
            $user->save();
            if ($this->locale == 'cn') {
                return redirect('/login')
                    ->with('success', '您的注册信息已确认。请在登录后进行操作。');
            } else {
                return redirect('/login')
                    ->with('success', 'Your registration is confirmed. Login to continue.');
            }
        } else {
            abort(404);
        }
    }

    public function login($value='')
    {
    	return view('login');
    }

    public function login_check(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $login_date = date('Y-m-d');
        $login_time = date('H:i:s');
        $login_month = date('F');
        $login_year = date('Y');

        $user_id = User::where('email',$request->input('email'))
                                ->where('user_type',1)
                                ->first();

        if ($request->input('remember') !== null) {
        	$remember = true;
        } else {
        	$remember = false;
        }

        if (Auth::attempt([
        	'email' => $request->input('email'),
        	'password' => $request->input('password'),
            'user_type' => 1,
            'delete_flg' => 0,
            'sec_code' => '',
        ], $remember)) {
            // return redirect('/user/home');

            $login = new LoginAttempt;
            $login->user_id = $user_id->id;
            $login->login_date = $login_date;
            $login->login_time = $login_time;
            $login->login_month = $login_month;
            $login->login_year = $login_year;
            $login->save();

            return redirect('/tutorial_quiz');
        } else {
            return back()->withInput()->with('error', 'Login failed! Try again.');
        }
    }

     public function forget_password()
    {
        return view('forget_password');
    }
     public function term_of_use()
    {
        return view('term_of_use');
    }

    public function forget_email_send(Request $request)
    {
        $sec_code = str_random(40);
        $user = User::where('email',$request->email)->first();

        if($user != null){
            $user = new User;
            $user->where('email',$request->email)
                    ->update([
                        'sec_code' => $sec_code
                    ]);

            Mail::to($request->email)->send(new ForgetPassword($sec_code));

            return redirect('/login')
                ->with('info', 'We already sent you password rest link.');
        } else {
            abort(404);
        }
    }

    public function forget_password_view($sec_code)
    {
        $user = User::where([
                    ['sec_code', $sec_code],
                    ['user_type', 1],
                ])->first();


        if ($user) {
            return view('password-update', [
                'info' => 'Set your new password.',
                'sec_code' => $user->sec_code
            ]);
        } else {
            abort(404);
        }
    }

    public function update_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:4',
            'confirm_password' => 'same:password',
        ]);

        $user = User::where([
                    ['sec_code', $request->sec_code],
                    ['user_type', 1],
                ])->first();

        if ($user) {
            $user->password = Hash::make($request->input('password'));
            $user->sec_code = '';
            $user->save();

            return redirect('/login')
                    ->with('success', 'Your password has been changed. Login to continue.');
        } else {
            abort(404);
        }
    }
}
