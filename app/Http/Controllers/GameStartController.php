<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Game;
use App\Models\Tutorial;
use App\Models\Question;
use App\Models\Answer;
use App\Models\ChineseQuestion;
use App\Models\ChineseAnswer;
use App\Models\TutoImage;
use App\Models\UserAnswer;
use App\Models\ChineseUserAnswer;
use App\Models\User;
use App\Models\ChineseTutoImages;
use App\Models\ChineseTutorial;
use App\Models\UnfinishGames;
use App;

class GameStartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.user');
        $locale = substr(url()->current(), 7, 2);

        if ($locale == 'cn') {
            App::setLocale('cn');
            $this->locale = 'cn';
        } else {
            App::setLocale('en');
            $this->locale = 'en';
            // App::setLocale('cn');
            // $this->locale = 'cn';
        }
    }

    public function game_start($game_id)
    {
    	$game = Game::where('id',$game_id)->where('delete_flg',0)->first();

        $tutorial = Tutorial::where('game_id',$game_id)->get();

        $chinese_tutorial = ChineseTutorial::where('game_id',$game_id)->get();

        $tuto_image = TutoImage::where('game_id',$game_id)->get();
        $chinese_tuto_image = ChineseTutoImages::where('game_id',$game_id)->get();

        $question = Question::where('game_id',$game_id)->where('delete_flg',0)->get();

        $answer = Answer::where('game_id',$game_id)->get();
        $progress = 0;

        $already_answered = UserAnswer::where('user_id',auth()->user()->id)
                                        ->where('game_id',$game_id)
                                        ->get();

        $unfinish_user = new UnfinishGames;
        $unfinish_user->user_id = auth()->user()->id;
        $unfinish_user->game_id = $game_id;
        $unfinish_user->tutorial_flg = 1;
        $unfinish_user->start_try_date = date('Y-m-d');
        $unfinish_user->start_try_time = date('H:i');
        $unfinish_user->save(); 

        return view('tuto')->with('tutorial',$tutorial)
                            ->with('question',$question)
                            ->with('answer',$answer)
                            ->with('game',$game)
                            ->with('tuto_image',$tuto_image)
                            ->with('chinese_tuto_image',$chinese_tuto_image)
                            ->with('chinese_tutorial',$chinese_tutorial)
                            ->with('progress',$progress)
                            ->with('already_answered',$already_answered);
    }

    public function quiz_start($game_id)
    {
    	$game = Game::where('id',$game_id)->where('delete_flg',0)->first();

		$tutorial = Tutorial::where('game_id',$game_id)->get();

		$tuto_image = TutoImage::where('game_id',$game_id)->get();

		$question = Question::where('game_id',$game_id)->where('delete_flg',0)->orderBy('id','asc')->first();

		$question_id = Question::where('game_id',$game_id)->where('delete_flg',0)->orderBy('id','asc')->first();

		$answer = Answer::where('game_id',$game_id)->get();

		$progress = 1;

        $unfinish_user_last_data = UnfinishGames::where('user_id',auth()->user()->id)
                                                ->where('game_id',$game_id)
                                                ->orderBy('id','desc')
                                                ->first();

        $unfinish_user = new UnfinishGames;
        $unfinish_user->where('id',$unfinish_user_last_data->id)
                        ->update([
                            'tutorial_flg' => 0,
                            'question_id' => $question->id,
                            'month' => date('F'),
                            'start_try_date' => date('Y-m-d'),
                            'start_try_time' => date('H:i')
                        ]);

		return view('quiz')->with('tutorial',$tutorial)
							->with('question',$question)
							->with('answer',$answer)
							->with('game',$game)
							->with('tuto_image',$tuto_image)
							->with('progress',$progress);
    }

    public function next_quiz(Request $request)
    {
		$question_id = $request['question_id'];
		$ans_id = $request['ans_id'];
		$game_id = $request['game_id'];

		$user_ans = new UserAnswer;
        $user_ans->user_id = auth()->user()->id;
        $user_ans->game_id = $game_id;
        $user_ans->question_id = $question_id;
        $user_ans->answer_id = $ans_id;
        $user_ans->correct_flg = 0;
        $user_ans->save();

        $question_id = Question::select('id')->where('game_id',$game_id)->get();
        foreach ($question_id as $value) {
    		$question_id_array[] = $value->id;
        }

        $user_answered = UserAnswer::select('question_id')->where('game_id',$game_id)
        													->where('user_id',auth()->user()->id)
        													->get();

        foreach ($user_answered as $value) {
    		$user_answered_array[] = $value->question_id;
        }

        $result = array_diff($question_id_array,$user_answered_array);
        $reindexed_array = array_values($result);

		$question = Question::where('game_id',$game_id)
							->where('delete_flg',0)
							->where('id','!=',$question_id)
							->orderBy('id','asc')
							->first();

        $url = config('app.url').'/start_quiz/'.$game_id.'/next_q/' . $question->id;

        $data = array(
        	'url' => $url,
        	'game_id' => $game_id,
        	'question_id' => $question->id
        );

        return $data;
    }

    public function next_question($game_id,$question_id)
    {
    	$game = Game::where('id',$game_id)->where('delete_flg',0)->first();

		$tutorial = Tutorial::where('game_id',$game_id)->get();

		$tuto_image = TutoImage::where('game_id',$game_id)->get();

		$question = Question::where('game_id',$game_id)->where('id',$question_id)->where('delete_flg',0)->orderBy('id','asc')->first();

		$answer = Answer::where('game_id',$game_id)->get();
		$progress = 2;

		return view('quiz')->with('tutorial',$tutorial)
							->with('question',$question)
							->with('answer',$answer)
							->with('game',$game)
							->with('tuto_image',$tuto_image)
							->with('progress',$progress);
    }

    public function wrong_question(Request $request)
    {
		$question_id = $request['question_id'];
		$ans_id = $request['ans_id'];
		$game_id = $request['game_id'];

		$user_ans = new UserAnswer;
        $user_ans->user_id = auth()->user()->id;
        $user_ans->game_id = $game_id;
        $user_ans->question_id = $question_id;
        $user_ans->answer_id = $ans_id;
        $user_ans->correct_flg = 1;
        $user_ans->save();

		$question_id = Question::select('id')->where('game_id',$game_id)->get();
        foreach ($question_id as $value) {
    		$question_id_array[] = $value->id;
        }

        $user_answered = UserAnswer::select('question_id')->where('game_id',$game_id)
        													->where('user_id',auth()->user()->id)
        													->get();

        foreach ($user_answered as $value) {
    		$user_answered_array[] = $value->question_id;
        }

        $result = array_diff($question_id_array,$user_answered_array);
        $reindexed_array = array_values($result);

        if (count($reindexed_array) == 1 && count($reindexed_array) != 0) {
			$question = Question::where('game_id',$game_id)
									->where('delete_flg',0)
									->where('id',$reindexed_array[0])
									->first();
		} else {
			$question = Question::where('game_id',$game_id)
							->where('delete_flg',0)
							->where('id','!=',$question_id)
							->orderBy('id','asc')
							->first();
		}

		if ($question == '') {
			$url = config('app.url').'/result/'.$game_id;
		} else {
			$url = config('app.url').'/start_quiz/'.$game_id.'/next_q/' . $question->id;
		}

        $data = array(
        	'url' => $url,
        	'game_id' => $game_id,
        	'question_id' => $question->id
        );

        return $data;
    }

    public function ans_checkbox_save(Request $request)
    {
    	$question_id = $request->question_id;
		$game_id = $request->game_id;
		$question_type = $request->question_type;

		$bonus = substr($request->input('bonus_time'), 2);

        if (is_array($request->input('user_ans'))) {
            foreach ($request->input('user_ans') as $value) {
                $user_ans = new UserAnswer;
                $user_ans->user_id = auth()->user()->id;
                $user_ans->game_id = $game_id;
                $user_ans->question_id = $question_id;
                $user_ans->answer_id = $value;
                $user_ans->correct_flg = 0;
                $user_ans->bonus_time = $bonus;
                $user_ans->save();
            }
        } else {
            $answer = $request->input('user_ans');
            $user_ans = new UserAnswer;
            $user_ans->user_id = auth()->user()->id;
            $user_ans->game_id = $game_id;
            $user_ans->question_id = $question_id;
            $user_ans->answer_id = $request->input('user_ans');
            $user_ans->correct_flg = $request->input('user_ans_correct_'.$answer);
            if ($request->input('user_ans_correct_'.$answer) == 0) {
                $user_ans->bonus_time = $bonus;
            } else {
                $user_ans->bonus_time = 0;
            }
            $user_ans->save();
        }

        $game = Game::where('id',$game_id)->where('delete_flg',0)->first();

        $tutorial = Tutorial::where('game_id',$game_id)->get();

        $tuto_image = TutoImage::where('game_id',$game_id)->get();

        $question_id = Question::select('id')->where('game_id',$game_id)->get();
        foreach ($question_id as $value) {
            $question_id_array[] = $value->id;
        }

        $user_answered = UserAnswer::select('question_id')->where('game_id',$game_id)
                                                            ->where('user_id',auth()->user()->id)
                                                            ->distinct()
                                                            ->get();

        foreach ($user_answered as $value) {
            $user_answered_array[] = $value->question_id;
        }

        if (count($user_answered_array) == 0) {
            $progress = 1;
        } elseif (count($user_answered_array) == 1) {
            $progress = 2;
        } elseif (count($user_answered_array) == 2) {
            $progress = 3;
        }

        $result = array_diff($question_id_array,$user_answered_array);
        $reindexed_array = array_values($result);

        $question = Question::where('game_id',$game_id)->whereIn('id',$reindexed_array)->where('delete_flg',0)->orderBy('id','asc')->first();

        $answer = Answer::where('game_id',$game_id)->get();

        if ($question != '') {
            $unfinish_user_last_data = UnfinishGames::where('user_id',auth()->user()->id)
                                                    ->where('game_id',$game_id)
                                                    ->orderBy('id','desc')
                                                    ->first();

            $unfinish_user = new UnfinishGames;
            $unfinish_user->where('id',$unfinish_user_last_data->id)
                            ->update([
                                'tutorial_flg' => 0,
                                'question_id' => $question->id,
                                'month' => date('F'),
                                'start_try_date' => date('Y-m-d'),
                                'start_try_time' => date('H:i')
                            ]);
            return view('quiz')->with('tutorial',$tutorial)
                            ->with('question',$question)
                            ->with('answer',$answer)
                            ->with('game',$game)
                            ->with('tuto_image',$tuto_image)
                            ->with('progress',$progress);
        } else {
            // $url = config('app.url').'/result';
            $unfinish_user_last_data = UnfinishGames::where('user_id',auth()->user()->id)
                                                    ->where('game_id',$game_id)
                                                    ->orderBy('id','desc')
                                                    ->first();

            $unfinish_user = new UnfinishGames;
            $unfinish_user->where('id',$unfinish_user_last_data->id)
                            ->update([
                                'tutorial_flg' => 0,
                                'question_id' => 00,
                                'month' => date('F'),
                                'start_try_date' => date('Y-m-d'),
                                'start_try_time' => date('H:i')
                            ]);

            return redirect('/result/'.$game_id);
        }
    }

    public function next_que($game_id,$question_id)
    {
    	$game = Game::where('id',$game_id)->where('delete_flg',0)->first();

		$tutorial = Tutorial::where('game_id',$game_id)->get();

		$tuto_image = TutoImage::where('game_id',$game_id)->get();

		$question = Question::where('game_id',$game_id)->where('id',$question_id)->where('delete_flg',0)->orderBy('id','asc')->first();

		$answer = Answer::where('game_id',$game_id)->get();
		$progress = 3;

		return view('quiz')->with('tutorial',$tutorial)
							->with('question',$question)
							->with('answer',$answer)
							->with('game',$game)
							->with('tuto_image',$tuto_image)
							->with('progress',$progress);
    }

    public function result($game_id)
    {
        $game = Game::where('id',$game_id)->where('delete_flg',0)->first();
        $game_list = Game::where('delete_flg',0)->orderBy('id','asc')->get();

        $user_answered = UserAnswer::where('user_id',auth()->user()->id)
                                    ->where('game_id',$game_id)
                                    ->get();

        $user_answered_list = UserAnswer::select('game_id')
                                    ->where('user_id',auth()->user()->id)
                                    ->distinct()
                                    ->orderBy('game_id','asc')
                                    ->get();

        $correct_count = 0;
        $bonus_time = 0;
        $bonus = 0;

        if (count($user_answered) > 0) {
            foreach ($user_answered as $u_ans) {
                if ($u_ans->correct_flg == 0 || $u_ans->correct_flg == 9) {
                    $correct_count = $correct_count + 1;
                }
            }

            $total_point = 100 * $correct_count;

            foreach ($user_answered as $u_ans) {
                $bonus += $u_ans->bonus_time;
                $bonus_time = $bonus_time + $u_ans->bonus_time;
            }

            $final_total_point = (int)auth()->user()->points + $total_point+$bonus;

            $user_point_update = new User;
            $user_point_update->where('id',auth()->user()->id)
                                ->update([
                                    'points' => $final_total_point
                                ]);
        }

        return view('result')->with('game',$game)
                                ->with('user_answered',$user_answered)
                                ->with('game_list',$game_list)
                                ->with('user_answered_list',$user_answered_list);
    }

    public function knowledge_meter()
    {
    	$ranking_list = User::where('delete_flg',0)->where('user_type',1)->orderBy('points','desc')->get();
    	$current_user_rank = User::where('delete_flg',0)
    								->where('user_type',1)
    								->where('id',auth()->user()->id)
    								->orderBy('points','desc')
    								->first();

    	$rank_key = '';

        return view('knowledge_meter')->with('ranking_list',$ranking_list)
                                        ->with('current_user_rank',$current_user_rank)
                                        ->with('rank_key',$rank_key);
    }

    public function rank_search(Request $request)
    {
    	if ($request->country != '' && $request->country != 'overall') {
            if ($request->country == 'SPAC (South Pacific)') {
                $ranking_list = User::where('delete_flg',0)
                            ->where('user_type',1)
                            ->where('country','Australia (SPAC)')
                            ->orWhere('country','New Zealand (SPAC)')
                            ->orderBy('points','desc')
                            ->get();
            }elseif ($request->country == 'SEAK (S.E.A & Korea)') {
                $ranking_list = User::where('delete_flg',0)
                            ->where('user_type',1)
                            ->where('country','Indonesia (SEA-K)')
                            ->orWhere('country','Malaysia (SEA-K)')
                            ->orWhere('country','Philippines (SEA-K)')
                            ->orWhere('country','Singapore (SEA-K)')
                            ->orWhere('country','Thailand (SEA-K)')
                            ->orWhere('country','Vietnam (SEA-K)')
                            ->orWhere('country','Korea (SEA-K)')
                            ->orderBy('points','desc')
                            ->get();
            }
            elseif ($request->country == 'China') {
                $ranking_list = User::where('delete_flg',0)
                            ->where('user_type',1)
                            ->where('country','China (Greater China)')
                            ->orWhere('country','Hong Kong (Greater China)')
                            ->orWhere('country','Taiwan (Greater China)')
                            ->orderBy('points','desc')
                            ->get();
            }elseif ($request->country == 'India') {
                $ranking_list = User::where('delete_flg',0)
                            ->where('user_type',1)
                            ->where('country','India')
                            ->orderBy('points','desc')
                            ->get();
            }
    	} else {
    		$ranking_list = User::where('delete_flg',0)
							->where('user_type',1)
							->orderBy('points','desc')
							->get();
    	}
		

    	$current_user_rank = User::where('delete_flg',0)
    								->where('user_type',1)
    								->where('id',auth()->user()->id)
    								->orderBy('points','desc')
    								->first();

    	$rank_key = $request->country;

    	return view('knowledge_meter', compact('ranking_list','current_user_rank','rank_key'));
    }
}
