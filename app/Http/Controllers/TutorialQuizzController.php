<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Game;
use App\Models\UserAnswer;
use App\Models\ChineseUserAnswer;
use Illuminate\Support\Facades\Auth;
use App;

class TutorialQuizzController extends Controller
{
    public function __construct($value='')
    {
        $locale = substr(url()->current(), 7, 2);

        if ($locale == 'cn') {
            App::setLocale('cn');
        } else {
            App::setLocale('en');
             // App::setLocale('cn');
        }
    }
    public function game_schedule()
    {
		$user_answer_list = Game::select('games.*')
    							->where('games.delete_flg',0)
                                ->orderBy('games.id')
    							->get();

		if (Auth::check()) {
			$current_answer_list = UserAnswer::select('game_id')->where('user_id',auth()->user()->id)->distinct()->orderBy('game_id')->get();
            $cur_ans = array();
            foreach ($current_answer_list as $current) {
                array_push($cur_ans, $current->game_id);
            }
		} else {
			$current_answer_list = array();
            $cur_ans = array();
		}

    	return view('game-schedule')->with('user_answer_list',$user_answer_list)
    								->with('current_answer_list',$current_answer_list)
                                    ->with('cur_ans',$cur_ans);
    }
}
