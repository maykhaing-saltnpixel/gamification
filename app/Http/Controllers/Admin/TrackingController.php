<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use DB;
use App\Models\User;
use App\Models\LoginAttempt;
use App\Models\UnfinishGames;
use App\Models\Game;

class TrackingController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth.admin');
    }

	public function login_attempt($value='')
	{
		$login_month = date('F');
		$login_attempt_list = LoginAttempt::select('*', DB::raw('count(*) as total'))->where('login_month',$login_month)->groupBy('user_id')->get();

		return view('admin.tracking.login-attempt-list')->with('login_attempt_list',$login_attempt_list)
														->with('login_month_search_data',$login_month );
	}

	public function login_attempt_search(Request $request)
	{
		if ($request->login_month != '') {
			$login_month = $request->login_month;
			$login_attempt_list = LoginAttempt::select('*', DB::raw('count(*) as total'))->where('login_month',$login_month)->groupBy('user_id')->get();
		} else {
			$login_month = date('F');
			$login_attempt_list = LoginAttempt::select('*', DB::raw('count(*) as total'))->where('login_month',$login_month)->groupBy('user_id')->get();
		}

		return view('admin.tracking.login-attempt-list')->with('login_attempt_list',$login_attempt_list)
														->with('login_month_search_data',$login_month);
	}

	public function game_unfinished($value='')
	{
		$unfinish_list = UnfinishGames::get();
		$game_list = Game::where('delete_flg',0)->get();
		$user_list = User::where('user_type',1)->where('delete_flg',0)->get();

		return view('admin.tracking.game-unfinish-list')->with('unfinish_list',$unfinish_list)
														->with('game_list',$game_list)
														->with('user_list',$user_list)
														->with('partner_id','')
														->with('game_id','')
														->with('date','');
	}

	public function unfinish_search(Request $request)
	{
		$partner_id = $request->partner_id;
		$game_id = $request->game_id;
		$date = $request->date;

		$game_list = Game::where('delete_flg',0)->get();

		if ($game_id != '' && $date != '') {
			$unfinish_list = UnfinishGames::where('game_id',$game_id)
											->where('start_try_date',$date)
											->get();
		}

		if ($game_id != '' && $date == '') {
			$unfinish_list = UnfinishGames::where('game_id',$game_id)->get();
		}

		if ($game_id == '' && $date != '') {
			$unfinish_list = UnfinishGames::where('start_try_date',$date)->get();
		}

		if ($game_id == '' && $date == '') {
			$unfinish_list = UnfinishGames::get();
		}

		if ($partner_id != '') {
			$user_list = User::where('user_type',1)->where('delete_flg',0)->where('partner_id',$partner_id)->get();
		} else {
			$user_list = User::where('user_type',1)->where('delete_flg',0)->get();
		}
		

		return view('admin.tracking.game-unfinish-list')->with('unfinish_list',$unfinish_list)
														->with('game_list',$game_list)
														->with('user_list',$user_list)
														->with('partner_id',$partner_id)
														->with('game_id',$game_id)
														->with('date',$date);
	}
}