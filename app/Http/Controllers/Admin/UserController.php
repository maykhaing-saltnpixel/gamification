<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Models\User;
use App\Models\Game;
use App\Models\UserAnswer;
use Redirect;
use App\Models\Prize;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{   

    public function report()
    {
        $games = Game::all();
        return view('admin.report',compact('games'))
                ->with('download_game','')
                ->with('download_country','');
    }

    public function prize()
    {
        $prizes = Prize::all();
        return view('admin.prize')->with('prizes',$prizes);
    }

    public function create()
    {
        return view('admin.prize_create');
    }

    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'location' => 'required',
            'image' => 'required|file|mimes:jpeg,jpg,png',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $prize = Prize::where('location',$request->input('location'))->first();
        if($prize){
            $prize->delete();
        }
        
        $prize = new Prize();
        $prize->location = $request->input('location');
        $image = $request->file('image');

        if($image){
            $path = $request->file('image')->store('prize');
        }
        $prize->image = $path ?? '';
        $prize->save();

        return redirect()->to('/prize')->with('success','Successfully created.');
    }

    public function download(Request $request)
    {
        if ($request->input('game') != '') {
            $users = User::select('users.*')
                            ->join('user_answers','user_answers.user_id','=','users.id')
                            ->where('user_answers.game_id',$request->input('game'))
                            ->where('users.delete_flg',0)
                            ->where('users.user_type',1)
                            ->distinct('user_answers.user_id');
            if($request->input('download_country') == 'All'){
                $users = $users;
            } elseif ($request->input('download_country') != '') {
                $users = $users->where('country',$request->input('download_country'));
            }
            $users = $users->orderBy('users.points','desc')->get();
        } else {
            $users = User::where('delete_flg',0)->where('user_type',1);
            if($request->input('download_country') == 'All'){
                $users = $users;
            } elseif ($request->input('download_country') != '') {
                $users = $users->where('country',$request->input('download_country'));
            }

            $users = $users->get();
        }

        $data = array(
            'users' => $users,
        );

        $pdf = PDF::loadView('admin.user_download', $data);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->download('user_list.pdf');
    }
}
