<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class RankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function ranking_list()
    {
    	$ranking_list = User::where('delete_flg',0)->where('user_type',1)->orderBy('points','desc')->get();

    	return view('admin.ranking', compact('ranking_list'));
    }
}
