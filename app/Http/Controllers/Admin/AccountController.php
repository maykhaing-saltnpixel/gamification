<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\eDMSend;

class AccountController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function eDMSend($value='')
    {
    	Mail::to('maykhaing@saltnpixel.com')->send(new eDMSend());
    }

    public function logout()
	{
        Auth::logout();
		return redirect('/admin/login');
	}
}
