<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Partner;
use App\Mail\EmailConfirm;
use App\Mail\PasswordReset;

class LoginController extends Controller
{
    public function login($value='')
    {
		return view('admin.login');
    }

    public function login_check(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($request->input('remember') !== null) {
        	$remember = true;
        } else {
        	$remember = false;
        }

        if (Auth::attempt([
        	'email' => $request->input('email'),
        	'password' => $request->input('password'),
            'user_type' => 0,
            'delete_flg' => 0,
        ], $remember)) {
            return redirect('/register/user_list');
        } else {
            return back()->withInput()->with('error', 'Login failed! Try again.');
        }
    }

    public function forget_password($value='')
    {
        return view('admin.password-forget');
    }

    public function password_reset_link(Request $request)
    {
        $email = $request->email;

        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $user = User::where([
                    ['email', $request->input('email')],
                    ['user_type', 0],
                ])->first();

        if ($user) {
            $sec_code = str_random(40);

            $user->sec_code = $sec_code;
            $user->save();

            Mail::to($user->email)->send(new PasswordReset($user->sec_code));

            return redirect('/admin/login')
                    ->with('info', 'Check your email for password reset link.');
        } else {
            return back()->withInput()
                ->with('error', 'The selected email is not in the system.');
        }
    }

    public function password_reset($sec_code)
    {
        $user = User::where([
                    ['sec_code', $sec_code],
                    ['user_type', 0],
                ])->first();

        if ($user) {
            return view('admin.password-update', [
                'info' => 'Set your new password.',
                'sec_code' => $user->sec_code,
            ]);
        } else {
            abort(404);
        }
    }

    public function password_update(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:4',
            'confirm_password' => 'same:password',
        ]);

        $user = User::where([
                    ['sec_code', $request->sec_code],
                    ['user_type', 0],
                ])->first();

        if ($user) {
            $user->password = Hash::make($request->input('password'));
            $user->sec_code = '';
            $user->save();

            return redirect('/admin/login')
                    ->with('success', 'Your password has been changed. Login to continue.');
        } else {
            abort(404);
        }
    }
}
