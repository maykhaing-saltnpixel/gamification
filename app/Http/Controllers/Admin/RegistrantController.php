<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class RegistrantController extends Controller
{
    public function list($value='')
    {
    	$registrant_list = User::where('user_type',1)->where('delete_flg',0)->get();
    	return view('admin.user-list')->with('registrant_list',$registrant_list)
    									->with('partner_id','')
    									->with('country','')
                                        ->with('download_country','');
    }

    public function search_registrant(Request $request)
    {
    	$registrant_list = User::where('delete_flg',0)->where('user_type',1);
    	if ($request->input('partner_id') != '') {
    		$registrant_list = $registrant_list->where('partner_id',$request->input('partner_id'));
    	}
    	if ($request->input('country') != '') {
    		$registrant_list = $registrant_list->where('country',$request->input('country'));
    	}
    	$registrant_list = $registrant_list->get();

    	return view('admin.user-list')->with('registrant_list',$registrant_list)
    									->with('partner_id',$request->input('partner_id'))
    									->with('country',$request->input('country'))
                                        ->with('download_country',$request->input('download_country'));
    }

    public function user_detail($user_id)
    {
    	$user_data = User::where('id',$user_id)->where('delete_flg',0)
    											->where('user_type',1)
    											->first();
    	// echo "<pre>";
    	// print_r($user_data);exit();
    	if ($user_data != '') {
    		return view('admin.user-detail')->with('user_data',$user_data)->with('error', '');
    	} else {
    		return view('admin.user-detail')->with('user_data','')->with('error', 'This user is currently unavailable');
    	}
    	// if (count) {
    	// 	# code...
    	// }

    	
    }
}
