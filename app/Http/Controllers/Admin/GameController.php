<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Game;
use App\Models\Summernote;
use App\Models\Tutorial;
use App\Models\Question;
use App\Models\Answer;
use App\Models\ChineseQuestion;
use App\Models\ChineseAnswer;
use App\Models\TutoImage;
use App\Models\ChineseTutoImages;
use App\Models\ChineseTutorial;
use App;

class GameController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth.admin');
        $locale = substr(url()->current(), 7, 2);

        if ($locale == 'cn') {
            App::setLocale('cn');
            $this->locale = 'cn';
        } else {
            // App::setLocale('en');
            // $this->locale = 'en';
            App::setLocale('cn');
            $this->locale = 'cn';
        }
    }

    public function index($value='')
    {
    	$game_list = Game::select('games.*')
                            ->where('games.delete_flg',0)
                            ->where('games.create_user_id',auth()->user()->id)
                            ->orderBy('games.id','desc')
                            ->get();

        $tutorial = Tutorial::select('game_id')
                                ->distinct()
                                ->get();

        $que_game = Question::select('game_id')
                                ->where('delete_flg',0)
                                ->distinct()
                                ->get();

    	return view('admin.game.index')->with('game_list',$game_list)
                                        ->with('que_game',$que_game)
                                        ->with('tutorial',$tutorial);
    }

    public function create_title($value='')
    {
    	return view('admin.game.create-game');
    }

    public function store_game_title(Request $request)
    {
    	$this->validate($request, [
    		'game_title' => 'required',
            'cn_game_title' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'game_image' => 'required',
            'inactive_game_image' => 'required',
        ]);

        $game_position = Game::select('game_position')->orderBy('id','desc')->first();
        if ($game_position == null) {
            $game_posit = 0;
        } else {
            $game_posit = $game_position->game_position;
        }

        $path = Storage::put('game', $request->file('game_image'));
        $path2 = Storage::put('inactive_game', $request->file('inactive_game_image'));
        $game = new Game;
        $game->game_title = $request->input('game_title');
        $game->cn_game_title = $request->input('cn_game_title');
        $game->from_date = $request->input('from_date');
        $game->to_date = $request->input('to_date');
        $game->game_image = $path ?? '';
        $game->inactive_game_image = $path2 ?? '';
        $game->create_user_id = auth()->user()->id;
        $game->publish_flg = $request->input('publish_flg');
        $game->game_position = (int)$game_posit + 1;
        $game->save();

        return redirect('/game')->with('success','Game create successful.');
    }

    public function add_game_qa($game_id)
    {
        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return view('admin.game.create-tuto')->with('game_data',$game_data);
    }

    public function storeTuto(Request $request)
    {
        $game_data = Game::where('id',$request->input('game_id'))
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        $tuto_count = $request->input('tuto_count');

        for ($i=1; $i <= $tuto_count ; $i++) { 
            $tutorial = new Tutorial;
            $tutorial->game_id = $request->input('game_id');
            $tutorial->tuto_type = $request->input('tuto_'.$i.'_type') ?? '';
            if ($request->input('tuto_'.$i.'_type') == 1) {
                $tutorial->tuto_value = $request->input('tuto_'.$i.'_file') ?? '';
            } elseif ($request->input('tuto_'.$i.'_type') == 2) {
                $tutorial->tuto_value = 'file';
            } elseif ($request->input('tuto_'.$i.'_type') == 3) {
                $path = $request->file('tuto_'.$i.'_file')->store('tutorial');
                $tutorial->tuto_value = $path ?? '';
            }
            $tutorial->save();

            $chinese_tutorial = new ChineseTutorial;
            $chinese_tutorial->game_id = $request->input('game_id');
            $chinese_tutorial->tuto_type = $request->input('tuto_'.$i.'_type') ?? '';
            if ($request->input('tuto_'.$i.'_type') == 1) {
                $chinese_tutorial->tuto_value = $request->input('tuto_'.$i.'_file_chinese') ?? '';
            } elseif ($request->input('tuto_'.$i.'_type') == 2) {
                $chinese_tutorial->tuto_value = 'file';
            } elseif ($request->input('tuto_'.$i.'_type') == 3) {
                $path = $request->file('tuto_'.$i.'_file_chinese')->store('tutorial');
                $chinese_tutorial->tuto_value = $path ?? '';
            }
            $chinese_tutorial->save();

            if ($request->input('tuto_'.$i.'_type') == 2) {
                foreach($request->file('tuto_'.$i.'_file') as $file)
                {
                    // $path = $file->store('tutorial');
                    $path = Storage::put('game', $file);

                    $tuto_photo = new TutoImage;
                    $tuto_photo->game_id = $request->input('game_id');
                    $tuto_photo->tuto_id = $tutorial->id;
                    $tuto_photo->photo = $path ?? '';
                    $tuto_photo->save();
                }

                foreach($request->file('tuto_'.$i.'_file_chinese') as $file)
                {
                    $chinese_path = Storage::put('chinese_game', $file);

                    $tuto_photo = new ChineseTutoImages;
                    $tuto_photo->game_id = $request->input('game_id');
                    $tuto_photo->tuto_id = $chinese_tutorial->id;
                    $tuto_photo->photo = $chinese_path ?? '';
                    $tuto_photo->save();
                }
            }
        }

        $game = new Game;
        $game->where('id',$request->input('game_id'))
                ->where('create_user_id',auth()->user()->id)
                ->update([
                    'tuto_flg' => 1
                ]);

        return redirect('/game')->with('success','Tutorial create successful for '.$game_data->game_title.'.');
    }

    public function add_quiz($game_id)
    {
        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return view('admin.game.create-quiz')->with('game_data',$game_data);
    }

    public function add_cn_quiz($game_id)
    {
        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return view('admin.game.cn-create-quiz')->with('game_data',$game_data);
    }

    public function store_quiz(Request $request)
    {
        $q_count = $request->input('quiz_count');

        for ($i=1; $i <= $q_count ; $i++) {
            $question = new Question;
            $question->game_id = $request->input('game_id');
            $question->question = htmlspecialchars($request->input('quiz_q_'.$i)) ?? '';
            $question->cn_question = htmlspecialchars($request->input('cn_quiz_q_'.$i)) ?? '';
            $question->question_type = $request->input('quiz_'.$i.'_type');
            $question->save();

            $ans_count = (int)$request->input('ans_count_'.$i);
            for ($j=1; $j <= $ans_count ; $j++) { 
                $correct_flg = $i.'_'.$j;
                if ($correct_flg == $request->input('correct_a_'.$i)) {
                    $answer = new Answer;
                    $answer->game_id = $request->input('game_id');
                    $answer->question_id = $question->id;
                    $answer->answer = $request->input('quiz_a_'.$i.'_'.$j);
                    $answer->cn_answer = $request->input('cn_quiz_a_'.$i.'_'.$j);
                    $answer->correct_flg = 0;
                    $answer->save();
                } else {
                    $answer = new Answer;
                    $answer->game_id = $request->input('game_id');
                    $answer->question_id = $question->id;
                    $answer->answer = $request->input('quiz_a_'.$i.'_'.$j);
                    $answer->cn_answer = $request->input('cn_quiz_a_'.$i.'_'.$j);
                    $answer->correct_flg = 1;
                    $answer->save();
                }
            }
        }

        $game = new Game;
        $game->where('id',$request->input('game_id'))
                ->where('create_user_id',auth()->user()->id)
                ->update([
                    'question_flg' => 1
                ]);

        $game_data = Game::where('id',$request->input('game_id'))
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return redirect('/game')->with('success','Question created successful for '.$game_data->game_title.'.');
    }

    public function edit_quiz($game_id)
    {
        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        $question_data = Question::where('game_id',$game_id)
                                    ->where('delete_flg',0)
                                    ->get();

        $answer_data = Answer::where('game_id',$game_id)
                                    ->get();

        return view('admin.game.edit-quiz')->with('question_data',$question_data)
                                            ->with('answer_data',$answer_data)
                                            ->with('game_data',$game_data);
    }

    public function update_quiz(Request $request)
    {
        $q_count = $request->input('quiz_count');

        $q_delete = Question::where('game_id',$request->input('game_id'))->delete();
        $ans_delete = Answer::where('game_id',$request->input('game_id'))->delete();

        for ($i=1; $i <= $q_count ; $i++) {
            $question = new Question;
            $question->game_id = $request->input('game_id');
            $question->question = htmlspecialchars($request->input('quiz_q_'.$i)) ?? '';
            $question->cn_question = htmlspecialchars($request->input('cn_quiz_q_'.$i)) ?? '';
            $question->question_type = $request->input('quiz_'.$i.'_type');
            $question->save();

            $ans_count = (int)$request->input('ans_count_'.$i);
            for ($j=1; $j <= $ans_count ; $j++) { 
                $correct_flg = $i.'_'.$j;
                if ($correct_flg == $request->input('correct_a_'.$i)) {
                    $answer = new Answer;
                    $answer->game_id = $request->input('game_id');
                    $answer->question_id = $question->id;
                    $answer->answer = $request->input('quiz_a_'.$i.'_'.$j);
                    $answer->cn_answer = $request->input('cn_quiz_a_'.$i.'_'.$j);
                    $answer->correct_flg = 0;
                    $answer->save();
                } else {
                    $answer = new Answer;
                    $answer->game_id = $request->input('game_id');
                    $answer->question_id = $question->id;
                    $answer->answer = $request->input('quiz_a_'.$i.'_'.$j);
                    $answer->cn_answer = $request->input('cn_quiz_a_'.$i.'_'.$j);
                    $answer->correct_flg = 1;
                    $answer->save();
                }
            }
        }

        $game_data = Game::where('id',$request->input('game_id'))
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return redirect('/game')->with('success','Question updated successful for '.$game_data->game_title.'.');
    }

    public function edit_game($game_id)
    {
        $game = Game::where('id',$game_id)
                        ->where('create_user_id',auth()->user()->id)
                        ->first();

        return view('admin.game.edit-game')->with('game_data',$game);
    }

    public function update_game(Request $request)
    {
        $game = new Game;
        if ($request->game_image_old == '' && $request->inactive_game_image_old == '') {
            $path = Storage::put('game', $request->file('game_image'));
            $path2 = Storage::put('inactive_game', $request->file('inactive_game_image'));
            $game->where('id',$request->game_id)
                    ->where('create_user_id',auth()->user()->id)
                    ->update([
                        'game_title' => $request->game_title,
                        'cn_game_title' => $request->cn_game_title,
                        'game_image' => $path ?? '',
                        'inactive_game_image' => $path2 ?? '',
                        'from_date' => $request->from_date,
                        'to_date' => $request->to_date,
                        'publish_flg' => $request->publish_flg ?? 1
                    ]);
        } elseif ($request->game_image_old != '' && $request->inactive_game_image_old == '') {
            $path2 = Storage::put('inactive_game', $request->file('inactive_game_image'));
            $game->where('id',$request->game_id)
                    ->where('create_user_id',auth()->user()->id)
                    ->update([
                        'game_title' => $request->game_title,
                        'cn_game_title' => $request->cn_game_title,
                        'inactive_game_image' => $path2 ?? '',
                        'from_date' => $request->from_date,
                        'to_date' => $request->to_date,
                        'publish_flg' => $request->publish_flg ?? 1
                    ]);   
        } elseif ($request->game_image_old == '' && $request->inactive_game_image_old != '') {
            $path = Storage::put('game', $request->file('game_image'));
            $game->where('id',$request->game_id)
                    ->where('create_user_id',auth()->user()->id)
                    ->update([
                        'game_title' => $request->game_title,
                        'cn_game_title' => $request->cn_game_title,
                        'game_image' => $path ?? '',
                        'from_date' => $request->from_date,
                        'to_date' => $request->to_date,
                        'publish_flg' => $request->publish_flg ?? 1
                    ]);   
        } else {
            $game->where('id',$request->game_id)
                    ->where('create_user_id',auth()->user()->id)
                    ->update([
                        'game_title' => $request->game_title,
                        'cn_game_title' => $request->cn_game_title,
                        'from_date' => $request->from_date,
                        'to_date' => $request->to_date,
                        'publish_flg' => $request->publish_flg ?? 1
                    ]);
        }

        $game_data = Game::where('id',$request->input('game_id'))
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return redirect('/game')->with('success','Game updated successful for '.$game_data->game_title.'.');
    }

    public function edit_game_tuto($game_id)
    {
        $tutorial = Tutorial::where('game_id',$game_id)->get();

        $tuto_photo = TutoImage::where('game_id',$game_id)->get();
        $chinese_tuto_photo = ChineseTutoImages::where('game_id',$game_id)->get();

        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return view('admin.game.edit-tuto')->with('tutorial',$tutorial)
                                            ->with('tuto_photo',$tuto_photo)
                                            ->with('game_data',$game_data)
                                            ->with('chinese_tuto_photo',$chinese_tuto_photo);
    }

    public function updateTuto(Request $request)
    {
        $q_delete = TutoImage::where('game_id',$request->input('game_id'))->delete();
        $chinese_img_delete = ChineseTutoImages::where('game_id',$request->input('game_id'))->delete();
        $ans_delete = Tutorial::where('game_id',$request->input('game_id'))->delete();

        $game_data = Game::where('id',$request->input('game_id'))
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        $tuto_count = $request->input('tuto_count');

        for ($i=1; $i <= $tuto_count ; $i++) { 
            $tutorial = new Tutorial;
            $tutorial->game_id = $request->input('game_id');
            $tutorial->tuto_type = $request->input('tuto_'.$i.'_type') ?? '';
            if ($request->input('tuto_'.$i.'_type') == 1) {
                $tutorial->tuto_value = $request->input('tuto_'.$i.'_file') ?? '';
            } elseif ($request->input('tuto_'.$i.'_type') == 2) {
                $tutorial->tuto_value = 'file';
            } elseif ($request->input('tuto_'.$i.'_type') == 3) {
                $path = $request->file('tuto_'.$i.'_file')->store('tutorial');
                $tutorial->tuto_value = $path ?? '';
            }
            $tutorial->save();

            if ($request->input('tuto_'.$i.'_type') == 2) {
                foreach($request->file('tuto_'.$i.'_file') as $file)
                {
                    // $path = $file->store('tutorial');
                    $path = Storage::put('game', $file);

                    $tuto_photo = new TutoImage;
                    $tuto_photo->game_id = $request->input('game_id');
                    $tuto_photo->tuto_id = $tutorial->id;
                    $tuto_photo->photo = $path ?? '';
                    $tuto_photo->save();
                }
                foreach($request->file('tuto_'.$i.'_file_chinese') as $file)
                {
                    // $path = $file->store('tutorial');
                    $path = Storage::put('chinese_game', $file);

                    $tuto_photo = new ChineseTutoImages;
                    $tuto_photo->game_id = $request->input('game_id');
                    $tuto_photo->tuto_id = $tutorial->id;
                    $tuto_photo->photo = $path ?? '';
                    $tuto_photo->save();
                }
            }
        }

        return redirect('/game')->with('success','Tutorial create successful for '.$game_data->game_title.'.');
    }

    public function unplubish_game($game_id)
    {
        $game = new Game;
        $game->where('id',$game_id)
                ->where('create_user_id',auth()->user()->id)
                ->update([
                    'publish_flg' => 1
                ]);
        $game_data = Game::where('id',$game_id)
                            ->where('delete_flg',0)
                            ->where('create_user_id',auth()->user()->id)
                            ->first();

        return redirect('/game')->with('success','Game updated successful for '.$game_data->game_title.'.');
    }
}
