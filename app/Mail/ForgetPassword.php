<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $sec_code;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sec_code)
    {
        $this->link = config('app.url') .'user/forget_password/' . $sec_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.user.forget-password');
    }
}
