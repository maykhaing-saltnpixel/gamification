<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class eDMSend extends Mailable
{
    use Queueable, SerializesModels;

    public $kv_link;
    public $space;
    public $section1_top;
    public $start_now_button;
    public $section1_bottom;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->kv_link = public_path() .'/img/week1_eDM/KV.jpg';
        $this->space = public_path().'/img/week1_eDM/spacer.gif';
        $this->section1_top = public_path().'/img/week1_eDM/section1_top.jpg';
        $this->start_now_button = public_path().'/img/week1_eDM/start-now_button.png';
        $this->section1_bottom = public_path().'/img/week1_eDM/section1_bottom.jpg';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.eDM.week1');
    }
}
