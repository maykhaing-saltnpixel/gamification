<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    public $sec_code;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sec_code)
    {
        $this->link = config('app.url') .'admin/password/reset/' . $sec_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.admin.password-reset');
    }
}
