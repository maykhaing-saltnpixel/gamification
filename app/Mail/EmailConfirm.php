<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailConfirm extends Mailable
{
    use Queueable, SerializesModels;

    public $sec_code;
    public $link;
    public $user_name;
    public $kv_link;
    public $line_break_link;
    public $space;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sec_code,$user_name)
    {
        $this->link = config('app.url') .'user/confirm-email/' . $sec_code;
        $this->kv_link = public_path() .'/img/confirm_KV.jpg';
        $this->line_break_link = public_path().'/img/confirm_line-break.png';
        $this->space = public_path().'/img/spacer.gif';
        $this->user_name = $user_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.user.confirm-email')->subject('HP Vibrant Planet Rally registration confirmation');
    }
}
